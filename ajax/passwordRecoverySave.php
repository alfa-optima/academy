<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){
    
    $arFields = [];
    parse_str($_POST["form_data"], $arFields);
    
    if( $USER->IsAuthorized() ){
        echo json_encode([
            "status" => "error",
            "text" => "Вы уже авторизованы на сайте"
        ]);
        return;
    }

    $recovery_code = trim(strip_tags($arFields['recovery_code']));
    $password = trim(strip_tags($arFields['password']));
    $password_confirm = trim(strip_tags($arFields['password_confirm']));

    if(
        !( strlen( $recovery_code ) > 0 )
        ||
        !( strlen( $password ) > 0 )
        ||
        !( strlen( $password_confirm ) > 0 )
    ){
        echo json_encode([
            "status" => "error",
            "text" => "Ошибка данных"
        ]);
        return;
    }

    // проверяем EMAIL на наличие
    $filter = [ "UF_PASSWORD_RECOVERY_CODE" => $recovery_code ];
    $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
    if( $user = $rsUsers->GetNext() ){

        $user_groups = \CUser::GetUserGroup($user['ID']);

        if( in_array( 1, $user_groups ) ){

            echo json_encode([
                "status" => "error",
                "text" => "Смена пароля по этому Email невозможна"
            ]);
            return;

        } else {

            $obUser = new CUser;
            $fields = [
                "UF_PASSWORD_RECOVERY_CODE" => null,
                "PASSWORD" => $password,
                "CONFIRM_PASSWORD" => $password_confirm,
            ];
            $result = $obUser->Update( $user['ID'], $fields );

            if( $result ){

                ob_start();
                    $APPLICATION->IncludeComponent("aoptima:password_recovery", "", [
                        'SUCCESS_SAVED' => 'Y'
                    ]);
                    $html = ob_get_contents();
                ob_end_clean();

                $server_name = tools\config::getValue('DOMAIN');

                // Отправка уведомления на почту
                $mail_title = 'Изменен пароль от личного кабинета - '.$server_name;
                $mail_html = '<p>Вы успешно сменили пароль на сайте '.$server_name.'</p>';
                project\email_queue::add(
                    $mail_title,
                    $mail_html,
                    $user['EMAIL']
                );

                echo json_encode([
                    "status" => "ok",
                    'html' => $html
                ]);
                return;

            } else {

                echo json_encode([
                    "status" => "error",
                    "text" => $obUser->LAST_ERROR
                ]);
                return;
            }
        }

    } else {

        echo json_encode([
            "status" => "error",
            "text" => "Ссылка ошибочна либо устарела"
        ]);
        return;
    }
}

// Ответ
echo json_encode([
    "status" => "error",
    "text" => "Ошибка запроса"
]);
return;
