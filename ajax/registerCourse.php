<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $logPrefix = 'Регистрация на курс - ';

    if( !$USER->IsAuthorized() ){
        // Ответ
        echo json_encode([ "status" => "error", "text" => "Ошибка авторизации" ]);
        return;
    }

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);
    foreach ( $arFields as $key => $value ){   $arFields[ $key ] = trim(strip_tags($value));   }

    $user = tools\user::info( $USER->GetID() );

    $errors = [];

    if ( strlen( $arFields['phone'] ) == 0 ){
        $errors[] = 'Введите, пожалуйста, Ваш телефон!';
    } else if (
        strlen( $arFields['phone'] ) > 0
        &&
        !preg_match("/^[0-9()+ -]{1,99}$/", $arFields['phone'], $matches, PREG_OFFSET_CAPTURE)
    ){
        $errors[] = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
    } else if ( strlen( $arFields['f'] ) == 0 ){
        $errors[] = 'Введите, пожалуйста, Вашу фамилию!';
    } else if ( strlen( $arFields['i'] ) == 0 ){
        $errors[] = 'Введите, пожалуйста, Ваше имя!';
    } else if ( strlen( $arFields['o'] ) == 0 ){
        $errors[] = 'Введите, пожалуйста, Ваше отчество!';
    } else if ( !isset( $arFields['gender'] ) ){
        $errors[] = 'Укажите, пожалуйста, ваш пол!';
    } else if ( strlen( $arFields['birthday'] ) == 0 ){
        $errors[] = 'Укажите, пожалуйста, дату Вашего рождения!';
    } else if ( strlen( $arFields['city'] ) == 0 ){
        $errors[] = 'Введите, пожалуйста, Ваш город!';
    } else if ( $arFields['sfera'] == 'empty' ){
        $errors[] = 'Укажите, пожалуйста, вашу сферу интересов';
    } else if ( !$arFields['agree_1'] ){
        $errors[] = 'Отметьте, пожалуйста, согласие с ПД';
    }

    if( count( $errors ) > 0 ){
        echo json_encode([
            'status' => 'error',
            'text' => $errors[0]
        ]);
        return;
    }

    if( intval($arFields['course_id'] ) > 0 ){

        $course = tools\el::info( intval($arFields['course_id'] ) );

        if( intval( $course['ID'] ) > 0 ){

            // Проверим доступна ли регистрация в данный момент
            $registerAvailable = project\learning_course::registerAvailable( $course['ID'] );

            if( $registerAvailable['result'] ){

                $PROPS = [
                    'COURSE' =>     intval($arFields['course_id'] ),
                    'USER' =>       $user['ID'],
                    'F' =>          $arFields['f'],
                    'I' =>          $arFields['i'],
                    'O' =>          $arFields['o'],
                    'GENDER' =>     $arFields['gender'],
                    'BIRTHDAY' =>   $arFields['birthday'],
                    'CITY' =>       $arFields['city'],
                    'PHONE' =>      $arFields['phone'],
                    'SFERA' =>      $arFields['sfera']
                ];

                $element = new \CIBlockElement;
                $fields = [
                    "IBLOCK_ID"				=> project\learning_course::REG_IBLOCK_ID,
                    "PROPERTY_VALUES"		=> $PROPS,
                    "NAME"					=> 'Участник',
                    "ACTIVE"				=> "Y"
                ];

                if( $reg_item_id = $element->Add($fields) ){

                    // Письму участнику
                    $client_email_to = $user['EMAIL'];
                    $mail_title = 'Успешная регистрация на курс в Академии Кнауф';
                    ob_start();
                        $APPLICATION->IncludeComponent(
                            "aoptima:mail_courseRegisterStatus", "client",
                            [
                                "course_id" => $course['ID'],
                                "reg_item_id" => $reg_item_id,
                            ]
                        );
                        $mail_html = ob_get_contents();
                    ob_end_clean();

                    project\email_queue::add( $mail_title, $mail_html, $client_email_to );
                    /// /// ///

                    // Письмо админу
                    $admin_email_to = 'alfa-optima@mail.ru, lobankovaalyona@gmail.com';
                    $mail_title = 'Новая регистрация на курс в Академии Кнауф';
                    ob_start();
                        $APPLICATION->IncludeComponent(
                            "aoptima:mail_courseRegisterStatus", "admin",
                            [
                                "course_id" => $course['ID'],
                                "reg_item_id" => $reg_item_id,
                            ]
                        );
                        $mail_html = ob_get_contents();
                    ob_end_clean();

                    project\email_queue::add( $mail_title, $mail_html, $admin_email_to );
                    /// /// ///

                    echo json_encode([
                        'status' => 'ok',
                        'course' => $course,
                    ]);
                    return;

                } else {

                    tools\logger::addError( $logPrefix.$element->LAST_ERROR );

                    echo json_encode([
                        'status' => 'error',
                        'text' => 'ошибка регистрации на курс',
                    ]);
                    return;
                }

            } else {
                echo json_encode([
                    'status' => 'error',
                    'text' => 'регистрация невозможна - '.$registerAvailable['description'],
                ]);
                return;
            }
        } else {
            echo json_encode([
                'status' => 'error',
                'text' => 'ошибка данных',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => 'error',
            'text' => 'ошибка данных',
        ]);
        return;
    }
}

// Ответ
echo json_encode(["status" => "error", "text" => "Ошибка запроса"]);
return;
