<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $logPrefix = 'ЛК - сохранения личных данных - ';

        $arFields = [];
        parse_str($_POST["form_data"], $arFields);
        foreach ( $arFields as $key => $value ){
            if( !is_array( $value ) ){   $arFields[$key] = trim(strip_tags($value));   }
        }

        $errors = [];
        
        if( strlen($arFields['LAST_NAME']) == 0 ){
            $errors[] = 'Введите, пожалуйста, вашу фамилию!';
        } else if( strlen($arFields['NAME']) == 0 ){
            $errors[] = 'Введите, пожалуйста, ваше имя!';
        } else if( strlen($arFields['PERSONAL_BIRTHDAY']) == 0 ){
            $errors[] = 'Укажите, пожалуйста, вашу дату рождения!';
        } else if( $arFields['PERSONAL_GENDER'] == 'empty'){
            $errors[] = 'Укажите, пожалуйста, ваш пол!';
        } else if( strlen($arFields['PERSONAL_PHONE']) == 0 ){
            $errors[] = 'Укажите, пожалуйста, ваш номер телефона!';
        } else if ( !preg_match("/^[0-9()+ -]{1,99}$/u", $arFields['PERSONAL_PHONE'], $matches, PREG_OFFSET_CAPTURE) ){
            $errors[] = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
        } else if( strlen($arFields['PERSONAL_CITY']) == 0 ){
            $errors[] = 'Укажите, пожалуйста, ваш город!';
        } else if( $arFields['UF_AREA_OF_ACTIVITY'] == 'empty'){
            $errors[] = 'Укажите, пожалуйста, вашу сферу деятельности!';
        } else if( strlen($arFields['WORK_COMPANY']) == 0 ){
            $errors[] = 'Укажите, пожалуйста, ваше место работы!';
        } else if( strlen($arFields['WORK_POSITION']) == 0 ){
            $errors[] = 'Укажите, пожалуйста, вашу должность!';
        } else if( $arFields['UF_HOW_DID_YOU_KNOW'] == 'empty'){
            $errors[] = 'Укажите, пожалуйста, откуда вы о нас узнали!';
        }

        if( count( $errors ) == 0 ){

            $arData = [
                'LAST_NAME' => $arFields['LAST_NAME'],
                'NAME' => $arFields['NAME'],
                'SECOND_NAME' => $arFields['SECOND_NAME'],
                'PERSONAL_BIRTHDAY' => $arFields['PERSONAL_BIRTHDAY'],
                'PERSONAL_GENDER' => $arFields['PERSONAL_GENDER'],
                'PERSONAL_PHONE' => $arFields['PERSONAL_PHONE'],
                'PERSONAL_CITY' => $arFields['PERSONAL_CITY'],
                'UF_AREA_OF_ACTIVITY' => $arFields['UF_AREA_OF_ACTIVITY'],
                'WORK_COMPANY' => $arFields['WORK_COMPANY'],
                'WORK_POSITION' => $arFields['WORK_POSITION'],
                'UF_HOW_DID_YOU_KNOW' => $arFields['UF_HOW_DID_YOU_KNOW'],
            ];

            $obUser = new \CUser;
            $result = $obUser->Update( $USER->GetID(), $arData );

            if( $result ){

                // Ответ
                echo json_encode([ "status" => "ok" ]);
                return;

            } else {

                tools\logger::addError( $logPrefix.$obUser->LAST_ERROR );

                // Ответ
                echo json_encode([ "status" => "error", "text" => "Ошибка сохранения"]);
                return;
            }

        } else {

            // Ответ
            echo json_encode([ "status" => "error", "text" => implode("<br>", $errors)]);
            return;
        }

    } else {
        // Ответ
        echo json_encode([ "status" => "error", "text" => "Ошибка авторизации" ]);
        return;
    }
}

// Ответ
echo json_encode([ "status" => "error", "text" => "Ошибка запроса" ]);
return;