<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        ob_start();
            $APPLICATION->IncludeComponent("aoptima:personalCompletedCourses", "");
            $html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode([ "status" => "ok", "html" => $html ]);
        return;

    } else {

        // Ответ
        echo json_encode(["status" => "error", "text" => "Ошибка авторизации"]);
        return;
    }
}

// Ответ
echo json_encode(["status" => "error", "text" => "Ошибка запроса"]);
return;
