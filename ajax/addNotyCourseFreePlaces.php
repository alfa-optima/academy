<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('highloadblock');

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);

    if( intval($arFields['course_id'] ) > 0 ){

        $course = tools\el::info( intval($arFields['course_id'] ) );

        if( intval( $course['ID'] ) > 0 ){

            // Проверим достпана ли регистрация в данный момент
            $freePlacesCnt = project\learning_course::freePlacesCnt( $course['ID'] );

            // Свободных мест всё ещё нет
            if( $freePlacesCnt == 0 ){

                $hasItemForCourse = project\free_places_noty::hasItemForCourse(
                    $course['ID'],
                    trim(strip_tags($arFields['email']))
                );

                if( !$hasItemForCourse ){

                    $res = project\free_places_noty::add(
                        $course['ID'],
                        trim(strip_tags($arFields['email']))
                    );

                    if( $res ){

                        echo json_encode([
                            'status' => 'ok',
                            'course' => $course,
                        ]);
                        return;

                    } else {
                        echo json_encode([
                            'status' => 'error',
                            'text' => 'Ошибка&nbsp;создания&nbsp;заявки на&nbsp;уведомление',
                        ]);
                        return;
                    }
                } else {
                    echo json_encode([
                        'status' => 'error',
                        'text' => 'Вы&nbsp;уже&nbsp;подписались&nbsp;ранее на&nbsp;уведомления по&nbsp;данному&nbsp;курсу',
                    ]);
                    return;
                }
            } else {
                echo json_encode([
                    'status' => 'error',
                    'text' => 'В&nbsp;данный&nbsp;момент уже&nbsp;есть&nbsp;свободные&nbsp;места',
                ]);
                return;
            }
        } else {
            echo json_encode([
                'status' => 'error',
                'text' => 'Ошибка данных',
            ]);
            return;
        }
    }
}

// Ответ
echo json_encode(["status" => "error", "text" => "Ошибка запроса"]);
return;
