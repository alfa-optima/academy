<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);

    if( $USER->IsAuthorized() ){
        echo json_encode([
            "status" => "error",
            "text" => "Вы уже авторизованы на сайте"
        ]);
        return;
    }

    $email = trim(strip_tags($arFields['email']));

    if( !( strlen($email) > 0 ) ){
        echo json_encode([
            "status" => "error",
            "text" => "Введите, пожалуйста, Ваш Email!"
        ]);
        return;
    }

    // проверяем EMAIL на наличие
    $filter = [ "EMAIL" => $email ];
    $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
    if( $user = $rsUsers->GetNext() ){

        $user_groups = \CUser::GetUserGroup($user['ID']);

        if( in_array( 1, $user_groups ) ){

            echo json_encode([
                "status" => "error",
                "text" => "Смена пароля по этому Email невозможна"
            ]);
            return;

        } else {

            $recovery_code = md5('gg43gwgseg32g'.time().randString(12, [ "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789" ]));

            $obUser = new CUser;
            $fields['UF_PASSWORD_RECOVERY_CODE'] = $recovery_code;
            $result = $obUser->Update( $user['ID'], $fields );

            if( $result ){

                $server_name = tools\config::getValue('DOMAIN');

                $mail_title = 'Ссылка для восстановления пароля - '.$server_name;

                $link = tools\config::getValue('SCHEME').'://'.$server_name.'/password_recovery/?code='.$recovery_code;

                $mail_html = '<p>Ссылка для восстановления пароля на сайте '.$server_name.':</p>';
                $mail_html .= '<p><a href="'.$link.'">'.$link.'</a></p>';

                // Отправка ссылки на почту
                \AOptima\Tools\feedback::sendMainEvent(
                    $mail_title,
                    $mail_html,
                    $user['EMAIL']
                );

                echo json_encode([
                    "status" => "ok",
                ]);
                return;

            } else {

                tools\logger::addError('Запрос ссылки для восстановления пароля - '.$result->LAST_ERROR);

                echo json_encode([
                    "status" => "error",
                    "text" => "Ошибка отправки ссылки для восстановления пароля"
                ]);
                return;
            }
        }

    } else {

        echo json_encode([
            "status" => "error",
            "text" => "Пользователь с таким Email не найден"
        ]);
        return;
    }
}

// Ответ
echo json_encode([
    "status" => "error",
    "text" => "Ошибка запроса"
]);
return;
