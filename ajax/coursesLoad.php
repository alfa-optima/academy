<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);

    $centers = project\center::all_items();
    $programs = project\program::all_items();

    // Фильтрация
    $pageFilter = project\learning_course::pageFilter( $arFields );
    if( isset( $pageFilter ) ){
        $GLOBALS['courses'] = $pageFilter;
    }

    if( is_array($_POST['stop_ids']) ){
        $GLOBALS['courses']['!ID'] = $_POST['stop_ids'];
    }

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:coursesFilterBlock", "", [
                'arFields' => $arFields
            ]
        );
    	$filter_html = ob_get_contents();
    ob_end_clean();
    
    // courses
    ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "courses",
            array(
                "IS_AJAX" => "Y",
                "centers" => $centers,
                "programs" => $programs,
                "IS_FILTER" => $_POST['is_filter']=='Y'?'Y':'N',
                "IS_LOAD" => $_POST['is_load']=='Y'?'Y':'N',
                "COMPONENT_TEMPLATE" => "courses",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => project\learning_course::IBLOCK_ID,
                "NEWS_COUNT" => project\learning_course::LIST_CNT+1,
                "SORT_BY1" => 'PROPERTY_DATE_FROM',
                "SORT_ORDER1" => 'DESC',
                "SORT_BY2" => 'ID',
                "SORT_ORDER2" => 'DESC',
                "FIELD_CODE" => project\learning_course::$list_fields,
                "PROPERTY_CODE" => project\learning_course::$list_props,
                "FILTER_NAME" => "courses",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => "undefined",
                "SET_LAST_MODIFIED" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ),
            false
        );
    	$html = ob_get_contents();
    ob_end_clean();


	// Ответ
	echo json_encode([
        "status" => "ok",
        "html" => $html,
        "filter_html" => $filter_html,
    ]);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
