<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $logPrefix = 'ЛК - сохранения пароля - ';

        $arFields = [];
        parse_str($_POST["form_data"], $arFields);
        foreach ( $arFields as $key => $value ){
            if( !is_array( $value ) ){   $arFields[$key] = trim(strip_tags($value));   }
        }

        if( !( strlen($arFields['OLD_PASSWORD']) > 0 ) ){
            echo json_encode([
                "status" => "error",
                "text" => "Введите, пожалуйста, действующий пароль!"
            ]);
            return;
        }
        if( !( strlen($arFields['PASSWORD']) > 0 ) ){
            echo json_encode([
                "status" => "error",
                "text" => "Введите, пожалуйста, новый пароль!"
            ]);
            return;
        }
        if( strlen($arFields['PASSWORD']) < project\user::PASSWORD_MIN_LENGTH ){
            echo json_encode([
                "status" => "error",
                "text" => 'Длина пароля - не менее '.project\user::PASSWORD_MIN_LENGTH
            ]);
            return;
        }
        if( !( strlen($arFields['CONFIRM_PASSWORD']) > 0 ) ){
            echo json_encode([
                "status" => "error",
                "text" => "Введите, пожалуйста, пароль повторно!"
            ]);
            return;
        }
        if( $arFields['PASSWORD'] != $arFields['CONFIRM_PASSWORD'] ){
            echo json_encode([
                "status" => "error",
                "text" => "Пароли не совпадают!"
            ]);
            return;
        }
        // Проверяем старый пароль
        if( !tools\user::comparePassword( $USER->GetID(), $arFields['OLD_PASSWORD'] ) ){
            echo json_encode([
                "status" => "error",
                "text" => "Введён не верный текущий пароль!"
            ]);
            return;
        }

        $arData = [
            'PASSWORD' => $arFields['PASSWORD'],
            'CONFIRM_PASSWORD' => $arFields['CONFIRM_PASSWORD'],
        ];

        $obUser = new \CUser;
        $result = $obUser->Update( $USER->GetID(), $arData );

        if( $result ){

            // Ответ
            echo json_encode([ "status" => "ok" ]);
            return;

        } else {

            tools\logger::addError( $logPrefix.$obUser->LAST_ERROR );

            // Ответ
            echo json_encode([ "status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }

    } else {
        // Ответ
        echo json_encode([ "status" => "error", "text" => "Ошибка авторизации" ]);
        return;
    }
}

// Ответ
echo json_encode([ "status" => "error", "text" => "Ошибка запроса" ]);
return;