<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    global $USER;

    if( $USER->IsAuthorized() ){

        // Ответ
        echo json_encode([
            "status" => "ok",
        ]);
        return;

    } else {

        $arFields = [];
        parse_str($_POST["form_data"], $arFields);

        $login = strip_tags(trim($arFields['login']));
        $password = strip_tags(trim($arFields['password']));

        if(
            strlen($login) > 0
            &&
            strlen($password) > 0
        ){

            $result = $USER->Login($login, $password, "Y");
            if ( !is_array( $result ) ){

                $USER->Authorize( $USER->GetID() );

                $user = tools\user::info( $USER->GetID() );

                if( $user['ACTIVE'] == 'Y' ){

                    // Ответ
                    echo json_encode([
                        "status" => "ok",
                    ]);
                    return;

                } else {

                    // Ответ
                    echo json_encode([
                        "status" => "error",
                        "text" => "Неверный логин или пароль"
                    ]); return;
                }

            } else {

                // Ответ
                echo json_encode([
                    "status" => "error",
                    "text" => "Неверный логин или пароль"
                ]); return;
            }

        } else {

            // Ответ
            echo json_encode([
                "status" => "error",
                "text" => "Ошибка данных!"
            ]); return;
        }
    }

}

// Ответ
echo json_encode(["status" => "error", "text" => "Ошибка запроса"]);
return;
