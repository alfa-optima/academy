<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:team", "", array('IS_AJAX' => 'Y')
        );
    	$html = ob_get_contents();
    ob_end_clean();

	// Ответ
	echo json_encode(Array("status" => "ok", 'html' => $html));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
