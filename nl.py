﻿import shutil, os

projectDir = os.getcwd()
templatesDir = projectDir + "\\local\\templates\\main\\components\\bitrix\\news.list\\"

templateName = input('Template name: ')

templateDir = templatesDir + templateName + "\\"

if not os.path.exists( templateDir ):
    os.makedirs( templateDir )
	
f1 = open(templateDir+'template.php', 'tw', encoding='utf-8')
f1.write("<? if(!defined(\"B_PROLOG_INCLUDED\") || B_PROLOG_INCLUDED!==true)die();\n/** @var array $arParams\n@var array $arResult */\n$this->setFrameMode(true);\n\nif (count($arResult[\"ITEMS\"]) > 0){ ?>\n\n\n\n    <? foreach($arResult[\"ITEMS\"] as $arItem){\n        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], \CIBlock::GetArrayByID($arItem[\"IBLOCK_ID\"], \"ELEMENT_EDIT\"));\n        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], \CIBlock::GetArrayByID($arItem[\"IBLOCK_ID\"], \"ELEMENT_DELETE\"), array(\"CONFIRM\" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>\n\n        <div id='<?=$this->GetEditAreaId($arItem[\"ID\"]);?>'></div>\n\n    <? } ?>\n\n\n\n<? } ?>")
f1.close()