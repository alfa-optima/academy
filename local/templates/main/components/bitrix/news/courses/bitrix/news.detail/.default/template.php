<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<section class="section">
    <div class="container">

        <h2 class="section__title text-uppercase"><?=$arParams['program']['NAME']?></h2>

        <p><?=ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "DD", "ru")?> <?=tools\funcs::getMonthName(ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "MM", "ru"))?> <?=ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "YYYY", "ru")?> г.</p>

        <div class="row g-4">

            <div class="col-lg-6">

                <? if( intval( $arResult['PREVIEW_PICTURE']['ID'] ) > 0 ){ ?>
                    <img class="img-fluid" src="<?=tools\funcs::rIMGG( $arResult['PREVIEW_PICTURE']['ID'], 5, 434, 325 )?>">
                <? } ?>

                <div class="table-responsive">
                    <table class="table table-borderless">

                        <thead>
                            <tr>
                                <th colspan="2">Информация о курсе обучения</th>
                            </tr>
                        </thead>

                        <tbody>

                            <tr>
                                <td><strong>Продолжительность</strong></td>
                                <td><?=count($arResult['DAYS_PLANS'])?> <?=tools\funcs::pfCnt(count($arResult['DAYS_PLANS']), 'день', 'дня', 'дней')?></td>
                            </tr>

                            <tr>
                                <td><strong>Начало</strong></td>
                                <td><?=ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "DD", "ru")?> <?=tools\funcs::getMonthName(ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "MM", "ru"))?> <?=ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "YYYY", "ru")?> г. в <?=ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "HH", "ru")?>:<?=ConvertDateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE'], "MI", "ru")?></td>
                            </tr>

                            <tr>
                                <td><strong>Конец</strong></td>
                                <td><?=ConvertDateTime($arResult['PROPERTIES']['DATE_TO']['VALUE'], "DD", "ru")?> <?=tools\funcs::getMonthName(ConvertDateTime($arResult['PROPERTIES']['DATE_TO']['VALUE'], "MM", "ru"))?> <?=ConvertDateTime($arResult['PROPERTIES']['DATE_TO']['VALUE'], "YYYY", "ru")?> г. в <?=ConvertDateTime($arResult['PROPERTIES']['DATE_TO']['VALUE'], "HH", "ru")?>:<?=ConvertDateTime($arResult['PROPERTIES']['DATE_TO']['VALUE'], "MI", "ru")?></td>
                            </tr>

                            <? if( isset( $arParams['place'] ) ){ ?>
                                <tr>
                                    <td><strong>Место проведения</strong></td>
                                    <td><?=$arParams['place']?></td>
                                </tr>
                            <? } ?>

                            <? if( intval($arParams['center']['ID']) > 0 ){ ?>
                                <tr>
                                    <td><strong>Обучающий центр</strong></td>
                                    <td><?=$arParams['center']['NAME']?></td>
                                </tr>
                            <? } ?>

                        </tbody>

                    </table>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="section__text">

                    <p><?=html_entity_decode(strlen($arParams['program']['DETAIL_TEXT'])>0?$arParams['program']['DETAIL_TEXT']:$arParams['program']['PREVIEW_TEXT'])?></p>

                    <? if( $arParams['registerAvailable']['result'] ){ ?>

                        <p>
                            <a href="/course_register/<?=$arResult['ID']?>/" class="btn btn-primary">Регистрация</a>
                        </p>

                    <? } else { ?>

                        <h3><i><?=$arParams['registerAvailable']['description']?></i></h3>

                        <? if( $arParams['registerAvailable']['result_code'] == 'no_places' ){ ?>

                            <p>
                                <a href="/course_register/<?=$arResult['ID']?>/" class="btn btn-primary">Регистрация</a>
                            </p>

                        <? }
                    } ?>

                </div>
            </div>

        </div>
    </div>
</section>


<section class="section">
    <div class="container">

        <h2 class="section__title text-uppercase">Программа курса обучения</h2>

        <div class="table-responsive">
            <table class="table table-borderless">

                <? foreach( $arResult['DAYS_PLANS'] as $day => $items ){ ?>

                    <tr>
                        <td>

                            <h4 class="mb-3">День <?=$day?></h4>

                            <div class="row">
                                <div class="col-md-12">
                                    <ul>
                                        <? foreach( $items as $item ){ ?>

                                            <li><b><?=$item['TIME']?></b> - <?=$item['TEXT']?></li>

                                        <? } ?>
                                    </ul>
                                </div>
                            </div>

                        </td>
                    </tr>

                <? } ?>

            </table>
        </div>
    </div>
</section>
