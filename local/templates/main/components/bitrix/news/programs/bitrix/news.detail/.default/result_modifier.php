<?php

$arResult['DAYS'] = [];
$arResult['DAYS_PLANS'] = [];

foreach ( $arResult['PROPERTIES']['ITEMS']['VALUE'] as $item ){
    $arResult['DAYS'][] = $item['SUB_VALUES']['DAY']['VALUE'];
}
$arResult['DAYS'] = array_unique($arResult['DAYS']);
sort($arResult['DAYS']);

foreach ( $arResult['DAYS'] as $day ){

    $sorts = [];   $day_plan = [];

    foreach ( $arResult['PROPERTIES']['ITEMS']['VALUE'] as $item ){
        if( $item['SUB_VALUES']['DAY']['VALUE'] == $day ){

            $id = md5( $item['SUB_VALUES']['SORT']['VALUE'].$item['SUB_VALUES']['TIME']['VALUE'] );

            $sorts[] = $item['SUB_VALUES']['SORT']['VALUE'];
            $day_plan[] = [
                'ID' => $id,
                'SORT' => $item['SUB_VALUES']['SORT']['VALUE'],
                'TIME' => preg_replace('/\s/', '&nbsp;', $item['SUB_VALUES']['TIME']['VALUE']),
                'TEXT' => $item['SUB_VALUES']['TEXT']['~VALUE']['TEXT']
            ];

        }
    }

    array_multisort($sorts, SORT_ASC, SORT_NUMERIC, $day_plan);

    $arResult['DAYS_PLANS'][$day] = $day_plan;
}





