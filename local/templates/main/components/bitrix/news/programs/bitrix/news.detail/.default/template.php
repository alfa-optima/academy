<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>


<section class="section">
    <div class="container">

        <h2 class="section__title text-uppercase"><?=html_entity_decode($arResult['NAME'])?></h2>

        <p class="section__subtitle"><?=$arResult['PROPERTIES']['SUBTITLE']['VALUE']?></p>

        <? if( 1 ){ ?>
            <div class="section__text"><?=$arResult['DETAIL_TEXT']?></div>
        <? } ?>

    </div>
</section>


<section class="section">
    <div class="container">
        <div class="table-responsive">
            <table class="table">

                <thead>
                    <tr>
                        <th class="h4" colspan="2">Программа обучения «<?=$arResult['NAME']?>»</th>
                    </tr>
                </thead>

                <tbody>

                    <? foreach( $arResult['DAYS_PLANS'] as $day => $items ){ ?>

                        <tr>
                            <th class="text-center" colspan="2">День <?=$day?></th>
                        </tr>

                        <? foreach( $items as $item ){ ?>

                            <tr>
                                <td class="text-nowrap"><?=$item['TIME']?></td>
                                <td><?=$item['TEXT']?></td>
                            </tr>

                        <? }

                    } ?>

                </tbody>
            </table>
        </div>
    </div>
</section>

