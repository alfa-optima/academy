<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<div class="programs">

    <? foreach( $arResult["ITEMS"] as $arItem ){ ?>

        <section class="section program bg-light">
            <div class="container">

                <h2 class="section__title text-uppercase"><?=html_entity_decode($arItem['NAME'])?></h2>

                <p class="section__subtitle text-uppercase"><?=$arItem['PROPERTIES']['SUBTITLE']['VALUE']?></p>

                <div class="row">

                    <div class="col-lg-6">
                        <img class="img-fluid" src="<?=tools\funcs::rIMGG( $arItem['PREVIEW_PICTURE']['ID'], 4, 434, 325 )?>">
                    </div>

                    <div class="col-lg-6">
                        <div class="section__text">

                            <h4 class="program__title mb-4"><?=$arItem['PREVIEW_TEXT']?></h4>

                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">Подробное описание программы</a>

                        </div>
                    </div>

                </div>

            </div>
        </section>

    <? } ?>

</div>