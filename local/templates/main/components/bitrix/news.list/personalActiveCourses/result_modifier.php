<?php

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

foreach( $arResult["ITEMS"] as $key => $arItem ){
    
    if( intval( $arItem['PROPERTIES']['PROGRAM']['VALUE'] ) > 0 ){

        $arItem['PROGRAM'] = tools\el::info( $arItem['PROPERTIES']['PROGRAM']['VALUE'] );
        
        $arItem['DAYS'] = [];
        $arItem['DAYS_PLANS'] = [];

        foreach ( $arItem['PROGRAM']['PROPERTY_ITEMS_VALUE'] as $item ){
            $arItem['PROGRAM']['DAYS'][] = $item['SUB_VALUES']['DAY']['VALUE'];
        }
        $arItem['PROGRAM']['DAYS'] = array_unique($arItem['PROGRAM']['DAYS']);
        sort($arItem['PROGRAM']['DAYS']);
        
        foreach ( $arItem['PROGRAM']['DAYS'] as $day ){

            $sorts = [];   $day_plan = [];

            foreach ( $arItem['PROGRAM']['PROPERTY_ITEMS_VALUE'] as $item ){
                if( $item['SUB_VALUES']['DAY']['VALUE'] == $day ){

                    $id = md5( $item['SUB_VALUES']['SORT']['VALUE'].$item['SUB_VALUES']['TIME']['VALUE'] );

                    $sorts[] = $item['SUB_VALUES']['SORT']['VALUE'];
                    $day_plan[] = [
                        'ID' => $id,
                        'SORT' => $item['SUB_VALUES']['SORT']['VALUE'],
                        'TIME' => preg_replace('/\s/', '&nbsp;', $item['SUB_VALUES']['TIME']['VALUE']),
                        'TEXT' => $item['SUB_VALUES']['TEXT']['~VALUE']['TEXT']
                    ];

                }
            }

            array_multisort($sorts, SORT_ASC, SORT_NUMERIC, $day_plan);

            $arItem['PROGRAM']['DAYS_PLANS'][$day] = $day_plan;
        }
    }

    $arResult["ITEMS"][ $key ] = $arItem;
}