<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$max_cnt = $arParams['NEWS_COUNT']-1;

if( count( $arResult["ITEMS"] ) > 0 ){ ?>

    <div class="course bg-brown-soft personalActiveCoursesLoadArea">
        
        <? $cnt = 0;
        foreach( $arResult["ITEMS"] as $arItem ){ $cnt++;
            if( $cnt <= $max_cnt ){ ?>

                <div class="course__item" item_id="<?=$arItem['ID']?>">
                    <div class="row g-4">

                        <div class="col-lg-5">
                            <img src="<?=tools\funcs::rIMGG( $arItem['PREVIEW_PICTURE']['ID'], 5, 337, 253 )?>" class=" course__img img-fluid">
                        </div>

                        <div class="col-lg-7">

                            <h4><?=$arItem['PROGRAM']['NAME']?></h4>

                            <div class="table-responsive">
                                <table class="course__table">

                                    <? if( strlen( $arItem['PROPERTIES']['LENGTH']['VALUE'] ) >0  ){ ?>
                                        <tr>
                                            <td>Продолжительность:</td>
                                            <td><?=strip_tags($arItem['PROPERTIES']['LENGTH']['VALUE'])?></td>
                                        </tr>
                                    <? } ?>

                                    <tr>
                                        <td>Начало:</td>
                                        <td><?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "DD", "ru")?> <?=tools\funcs::getMonthName( ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "MM", "ru") )?> <?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "YYYY", "ru")?> г. в <?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "HH:MI", "ru")?></td>
                                    </tr>

                                    <tr>
                                        <td>Конец:</td>
                                        <td><?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_TO_PROP_CODE ]['VALUE'], "DD", "ru")?> <?=tools\funcs::getMonthName( ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_TO_PROP_CODE ]['VALUE'], "MM", "ru") )?> <?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_TO_PROP_CODE ]['VALUE'], "YYYY", "ru")?> г. в <?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_TO_PROP_CODE ]['VALUE'], "HH:MI", "ru")?></td>
                                    </tr>

                                    <? if( strlen($arItem['PROPERTIES']['TYPE']['VALUE']) > 0 ){ ?>
                                        <tr>
                                            <td>Форма обучения:</td>
                                            <td><?=strip_tags($arItem['DISPLAY_PROPERTIES']['TYPE']['DISPLAY_VALUE'])?></td>
                                        </tr>
                                    <? } ?>

                                    <? if( intval($arItem['PROPERTIES']['CENTER']['VALUE']) > 0 ){ ?>
                                        <tr>
                                            <td>Обучающий центр:</td>
                                            <td><?=strip_tags($arItem['DISPLAY_PROPERTIES']['CENTER']['DISPLAY_VALUE'])?></td>
                                        </tr>
                                    <? } ?>

                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="course__accordion">
                        <a href="#course-<?=$arItem['ID']?>" class="collapsible">Программа курса</a>
                        <div id="course-<?=$arItem['ID']?>" class="course__content">
                            <div class="table-responsive">
                                <table class="course__table">

                                    <? foreach( $arItem['PROGRAM']['DAYS_PLANS'] as $day => $items ){ ?>

                                        <tr>
                                            <td colspan="2">
                                                <h4>День <?=$day?></h4>
                                            </td>
                                        </tr>

                                        <? foreach( $items as $item ){ ?>

                                            <tr>
                                                <td><?=$item['TIME']?></td>
                                                <td><?=$item['TEXT']?></td>
                                            </tr>

                                        <? }
                                    } ?>

                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            <? }
        } ?>

    </div>

    <div class="personalActiveCoursesLoadBlock <? if( $cnt > $max_cnt ){ echo 'isVisible'; } ?>" style="width: 100%; margin-top: 30px; margin-bottom: 30px; text-align: center; <? if( $cnt <= $max_cnt ){ echo 'display: none;'; } ?>">
        <button class="btn btn-primary btn-sm personalActiveCoursesLoadButton to___process">Показать ещё</button>
    </div>

<? } else { ?>

    <div class="bg-brown-soft" style="padding: 40px 30px;">
        <p><i>Активных курсов у вас сейчас нет</i></p>
    </div>

<? } ?>