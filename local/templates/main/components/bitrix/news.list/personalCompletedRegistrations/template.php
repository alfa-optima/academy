<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$max_cnt = $arParams['NEWS_COUNT']-1;

if( count( $arResult["ITEMS"] ) > 0 ){ ?>

    <div class="bg-brown-soft personalCompletedCoursesLoadArea">

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="h4" colspan="8">Пройденные курсы</th>
                    </tr>
                    <tr class="text-nowrap">
                        <th>Программа</th>
                        <th>Дата обучения</th>
                        <th>Учебный центр</th>
                        <th>Город</th>
                        <th>Курс пройден</th>
                        <th>Экзамен сдан</th>
                        <th>Сертификат</th>
                    </tr>
                </thead>
                <tbody>

                    <? $cnt = 0;
                    foreach( $arResult["ITEMS"] as $reg ){ $cnt++;
                        if( $cnt <= $max_cnt ){

                            $course = $arParams['REGISTRATION_COURSES_INFO'][$reg['ID']][$reg['PROPERTIES']['COURSE']['VALUE']]; ?>

                            <tr class="reg__item" item_id="<?=$reg['ID']?>">

                                <td>
                                    <a href="<?=$course['PROGRAM']['DETAIL_PAGE_URL']?>"><?=$course['PROGRAM']['NAME']?></a>
                                </td>

                                <td><?=ConvertDateTime($course['PROPERTY_'.project\learning_course::DATE_FROM_PROP_CODE.'_VALUE'], "DD.MM.YYYY", "ru")?> - <?=ConvertDateTime($course['PROPERTY_'.project\learning_course::DATE_TO_PROP_CODE.'_VALUE'], "DD.MM.YYYY", "ru")?></td>

                                <td><?=$course['CENTER']['NAME']?></td>
                                <td><?=$course['CENTER']['PROPERTY_CITY_VALUE']?></td>

                                <td class="<?=$reg['PROPERTIES']['STATUS_1']['VALUE']?'':'text-danger'?>" ><?=$reg['PROPERTIES']['STATUS_1']['VALUE']?'да':'нет'?></td>
                                <td class="<?=$reg['PROPERTIES']['STATUS_2']['VALUE']?'':'text-danger'?>"><?=$reg['PROPERTIES']['STATUS_2']['VALUE']?'да':'нет'?></td>

                                <td>

                                    <? $cert = $arParams['REGISTRATION_CERTIFICATES'][ $reg['ID'] ];
                                    if( intval( $cert['ID'] ) > 0 ){ ?>
                                            
                                        <a href="<?=$cert['FILE_PATH']?>" target="_blank">Сертификат<br>№&nbsp;<?=$cert['NUMBER']?><br>от <?=ConvertDateTime($cert['UF_DATE'], "DD.MM.YYYY", "ru")?></a>

                                    <? } else { ?>

                                        <p style="text-align: center;">-</p>

                                    <? } ?>

                                </td>

                            </tr>

                        <? }
                    } ?>

                </tbody>
            </table>
        </div>

        <div class="text-center pt-2 pb-4 personalCompletedCoursesLoadBlock <? if( $cnt > $max_cnt ){ echo 'isVisible'; } ?>" style="<? if( $cnt <= $max_cnt ){ echo 'display: none;'; } ?>">
            <a style="cursor: pointer" class="btn btn-primary px-4 personalCompletedCoursesLoadButton to___process">Показать еще</a>
        </div>

    </div>


<? } else { ?>

    <div class="bg-brown-soft" style="padding: 40px 30px;">
        <p><i>Пройденных курсов у вас сейчас нет</i></p>
    </div>

<? } ?>