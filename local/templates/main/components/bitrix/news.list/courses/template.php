<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$max_cnt = $arParams['NEWS_COUNT']-1;

if( $arParams['IS_LOAD'] != 'Y' ){ ?>


    <div class="row g-4 coursesLoadBlock">

        <? $cnt = 0;
        foreach( $arResult["ITEMS"] as $arItem ){ $cnt++;
            if( $cnt <= $max_cnt ){
                
                $city = null;
                if( intval($arItem['PROPERTIES']['CENTER']['VALUE']) > 0 ){
                    foreach ( $arParams['centers'] as $center ){
                        if( $center['ID'] == $arItem['PROPERTIES']['CENTER']['VALUE'] ){
                            $city = $center['PROPERTY_CITY_VALUE'];
                        }
                    }
                }

                $program = null;
                if( intval($arItem['PROPERTIES']['PROGRAM']['VALUE']) > 0 ){
                    foreach ( $arParams['programs'] as $arProgram ){
                        if( $arProgram['ID'] == $arItem['PROPERTIES']['PROGRAM']['VALUE'] ){
                            $program = $arProgram;
                        }
                    }
                }

                ?>

                <div class="col-md-6 col-lg-3 course___item" item_id="<?=$arItem['ID']?>">
                    <div class="card">
                        <? if( intval( $program['PREVIEW_PICTURE'] ) > 0 ){ ?>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <img src="<?=tools\funcs::rIMGG( $program['PREVIEW_PICTURE'], 5, 310, 232 )?>" class="img-fluid">
                            </a>
                        <? } ?>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card__caption mb-2">
                            <? if( isset( $city ) ){ ?>
                                <p class="mb-0"><small><?=$city?></small></p>
                            <? } ?>
                            <p class="card__date mb-1"><?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "DD.MM.YYYY", "ru")?></p>
                            <p class="mb-0"><small>Начало в <?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "HH:MI", "ru")?></small></p>
                        </a>
                        <div class="card__body">
                            <h4><?=$program['NAME']?></h4>
                            <p><?=$program['PREVIEW_TEXT']?></p>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">Записаться</a>
                        </div>
                    </div>
                </div>

            <? }
        } ?>

    </div>

    <div class="coursesMoreBlock" style="width: 100%; margin-top: 30px; margin-bottom: 30px; text-align: center; <? if( $cnt <= $max_cnt ){ echo 'display: none;'; } ?>">
        <button class="btn btn-primary btn-sm coursesMoreButton to___process">Показать ещё</button>
    </div>


<? } else if( $arParams['IS_LOAD'] == 'Y' ){


    $cnt = 0;
    foreach( $arResult["ITEMS"] as $arItem ){ $cnt++;
        if( $cnt <= $max_cnt ){ ?>

            <div class="col-md-6 col-lg-3 course___item" item_id="<?=$arItem['ID']?>">
                <div class="card">
                    <img src="<?=tools\funcs::rIMGG( $arItem['PREVIEW_PICTURE']['ID'], 5, 310, 232 )?>" class="img-fluid">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="card__caption mb-2">
                        <p class="mb-0"><small><?=tools\funcs::week_day_string(ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "DD.MM.YYYY", "ru"))?></small></p>
                        <p class="card__date mb-1"><?=tools\funcs::week_day_string(ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "DD.MM.YYYY", "ru"))?></p>
                        <p class="mb-0"><small>Начало в <?=ConvertDateTime($arItem['PROPERTIES'][ project\learning_course::DATE_FROM_PROP_CODE ]['VALUE'], "HH:MI", "ru")?></small></p>
                    </a>
                    <div class="card__body">
                        <h4><?=$arItem['NAME']?></h4>
                        <p><?=$arItem['PREVIEW_TEXT']?></p>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">Записаться</a>
                    </div>
                </div>
            </div>

        <? } else {
            echo '<ost></ost>';
        }
    }


} ?>
