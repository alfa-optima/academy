// Lessons slider
var lessons = new Swiper(".lessons", {
  slidesPerView: 1,
  spaceBetween: 26,
  grabCursor: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    571: {
      slidesPerView: 2,
    },
    1001: {
      slidesPerView: 4,
    }
  }
});

// Image slider
var images = new Swiper(".images", {
  slidesPerView: 2,
  spaceBetween: 26,
  grabCursor: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  breakpoints: {
    571: {
      slidesPerView: 3,
    },
    1001: {
      slidesPerView: 4,
    }
  }
});

if($('[data-fancybox="gallery"]').length > 0){
  $('[data-fancybox="gallery"]').fancybox({
    hash: false,
    buttons: [
      "close"
    ],
  });
}

// Collapse
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}


// Scroll to anchor
$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .on('click', function(event) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 100);
      }
    }
  });


// Datepicker

if($('.datepicker').length) {
  $('.datepicker').datetimepicker({
    format:'d.m.Y',
    firstDay: 1,
    scrollMonth: false,
    timepicker:false,
  });

  jQuery.datetimepicker.setLocale('ru');
}
