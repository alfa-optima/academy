
var search_interval;


function coursesLoad ( params ){

    if( !params ){   var params = {};   }

    var url = rawurldecode( document.URL );
    params['url'] = url;

    process(true);
    $.post("/ajax/coursesLoad.php", params, function(data){
        if( data.status == 'ok' ){

            var html = data.html;

            if( params['is_filter'] == 'Y' ){

                $('.coursesMoreBlock').remove();
                $('.coursesLoadBlock').replaceWith( html );
                $('.coursesFilterBlock').replaceWith( data.filter_html )

                var new_url = url;
                if( params['program'] != undefined ){
                    if( params['program'] == 'empty' ){
                        new_url = removeFromRequestURI( new_url, 'program' );
                    } else {
                        new_url = addToRequestURI( new_url, 'program', params['program'] );
                    }
                }
                if( params['country'] != undefined ){
                    if( params['country'] == 'empty' ){
                        new_url = removeFromRequestURI( new_url, 'country' );
                    } else {
                        new_url = addToRequestURI( new_url, 'country', params['country'] );
                    }
                }
                if( params['city'] != undefined ){
                    if( params['city'] == 'empty' ){
                        new_url = removeFromRequestURI( new_url, 'city' );
                    } else {
                        new_url = addToRequestURI( new_url, 'city', params['city'] );
                    }
                }
                if( params['learn_type'] != undefined ){
                    if( params['learn_type'] == 'empty' ){
                        new_url = removeFromRequestURI( new_url, 'learn_type' );
                    } else {
                        new_url = addToRequestURI( new_url, 'learn_type', params['learn_type'] );
                    }
                }
                if( params['min_date'] != undefined ){
                    new_url = addToRequestURI( new_url, 'min_date', params['min_date'] );
                }
                if( params['max_date'] != undefined ){
                    new_url = addToRequestURI( new_url, 'max_date', params['max_date'] );
                }
                history.replaceState({}, null, new_url);

            } else if( params['is_load'] == 'Y' ){

                $('.coursesLoadBlock').append( html );

                if( $('ost').length > 0 ){
                    $('.coursesMoreBlock').show();
                } else {
                    $('.coursesMoreBlock').hide();
                }
                $('ost').remove();
            }

        } else if (data.status == 'error'){
            alert( data.text );
        }
    }, 'json')
    .fail(function(data, status, xhr){
        alert( 'Ошибка запроса' );
    })
    .always(function(data, status, xhr){
        process(false);
    })
}



$(document).ready(function(){



    //$('#birthday').datepicker();



    // Заявка на уведомления о появлении свободных мест
    $(document).on('click', '.notyCourseFreePlacesButton', function(e){
        e.preventDefault();
        var button = $(this);
        if( !is_process(this) ){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var email = $(this_form).find("input[name=email]").val();
            var errors = [];
            // Проверки
            if (email.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=email]"),
                    text: 'Введите, пожалуйста, Ваш Email!'
                });
            } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))){
                errors.push({
                    link: $(this_form).find("input[name=email]"),
                    text: 'Email содержит ошибки!'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/addNotyCourseFreePlaces.php", { form_data:form_data }, function(data){
                    if (data.status == 'ok'){

                        alert('Заявка успешно отправлена');

                        window.location.href = data.course.DETAIL_PAGE_URL;

                    } else if (data.status == 'error'){

                        show_message( data.text, 'warning', 'hover' );
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                show_error(this_form, errors);
            }
        }
    });



    // Запись на курс
    $(document).on('click', '.registerCourseButton', function(e){
        e.preventDefault();
        var button = $(this);
        if( !is_process(this) ){

            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var f = $(this_form).find("input[name=f]").val();
            var i = $(this_form).find("input[name=i]").val();
            var o = $(this_form).find("input[name=o]").val();
            var gender = $(this_form).find("input[name=gender]:checked").val();
            var birthday = $(this_form).find("input[name=birthday]").val();
            var city = $(this_form).find("input[name=city]").val();
            var phone = $(this_form).find("input[name=phone]").val();
            var sfera = $(this_form).find("select[name=sfera]").val();
            var agree_1 = $(this_form).find("input[name=agree_1]:checked").length > 0;
            var errors = [];
            // Проверки
            if (phone.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=phone]"),
                    text: 'Введите, пожалуйста, Ваш телефон!'
                });
            } else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))){
                errors.push({
                    link: $(this_form).find("input[name=phone]"),
                    text: 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'
                });
            } else if (city.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=city]"),
                    text: 'Введите, пожалуйста, Ваш город!'
                });
            } else if (f.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=f]"),
                    text: 'Введите, пожалуйста, Вашу фамилию!'
                });
            } else if (i.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=i]"),
                    text: 'Введите, пожалуйста, Ваше имя!'
                });
            } else if (o.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=o]"),
                    text: 'Введите, пожалуйста, Ваше отчество!'
                });
            } else if (gender == undefined){
                errors.push({
                    link: $(this_form).find("input[name=gender]"),
                    text: 'Укажите, пожалуйста, ваш пол!'
                });
            } else if (birthday.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=birthday]"),
                    text: 'Укажите, пожалуйста, дату Вашего рождения!'
                });
            } else if ( sfera == 'empty' ){
                errors.push({
                    link: $(this_form).find("select[name=sfera]"),
                    text: 'Укажите, пожалуйста, вашу сферу интересов'
                });
            } else if ( !agree_1 ){
                errors.push({
                    link: $(this_form).find("input[name=agree_1]"),
                    text: 'Отметьте, пожалуйста, согласие на обработку персональных данных'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){

                process(true);
                var form_data = $(this_form).serialize();

                $.post("/ajax/registerCourse.php", { form_data:form_data }, function(data){
                    if (data.status == 'ok'){

                        alert('Вы успешно зарегистрировались на курс');

                        window.location.href = data.course.DETAIL_PAGE_URL;

                    } else if (data.status == 'error'){

                        show_message( data.text, 'warning', 'hover' );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, errors);
            }

        }
    })





    // Подгрузка
    $(document).on('click', '.coursesMoreButton', function(e){
        if( !is_process(this) ){
            var stop_ids = [];
            $('.course___item').each(function(){
                var item_id = $(this).attr('item_id');
                stop_ids.push(item_id);
            })
            var params = {
                stop_ids: stop_ids,
                is_load: 'Y',
                form_data: $('.coursesFilterForm').serialize(),
            };
            coursesLoad( params );
        }
    })


    // Сброс фильтра
    $(document).on('click', '.resetCoursesFilterButton', function(e){
        if( !is_process(this) ){
            $('.coursesFilterForm input[name=q]').val('');
            $('.coursesFilterForm input[name=min_date]').val('');
            $('.coursesFilterForm input[name=max_date]').val('');
            $('.coursesFilterForm select option').prop('selected', false);
            var params = {
                is_filter: 'Y',
                form_data: $(this).parents('form').serialize(),
            };
            coursesLoad( params );
        }
    })


    // Ввод в поисковую строку
    $(document).on('input', '.coursesFilterForm input[name=q]', function(e){
        var q = $(this).val();
        var form_data = $(this).parents('form').serialize();
        clearInterval(search_interval);
        search_interval = setInterval(function(){
            clearInterval(search_interval);
            var params = {
                is_filter: 'Y',
                q: q,
                form_data: form_data,
            };
            coursesLoad( params );
        }, 700);
    })



    // Фильтрация по программе
    $(document).on('change', '.coursesFilterForm select[name=program]', function(e){
        var params = {
            is_filter: 'Y',
            program: $(this).val(),
            form_data: $(this).parents('form').serialize(),
        };
        coursesLoad( params );
    })
    // Фильтрация по стране
    $(document).on('change', '.coursesFilterForm select[name=country]', function(e){
        var params = {
            is_filter: 'Y',
            country: $(this).val(),
            form_data: $(this).parents('form').serialize(),
        };
        coursesLoad( params );
    })
    // Фильтрация по городу
    $(document).on('change', '.coursesFilterForm select[name=city]', function(e){
        var params = {
            is_filter: 'Y',
            city: $(this).val(),
            form_data: $(this).parents('form').serialize(),
        };
        coursesLoad( params );
    })
    // Фильтрация по форме обучения
    $(document).on('change', '.coursesFilterForm select[name=learn_type]', function(e){
        var params = {
            is_filter: 'Y',
            learn_type: $(this).val(),
            form_data: $(this).parents('form').serialize(),
        };
        coursesLoad( params );
    })
    // Фильтрация по дате начала - "с"
    $(document).on('change', '.coursesFilterForm input[name=min_date]', function(e){
        var params = {
            is_filter: 'Y',
            min_date: $(this).val(),
            form_data: $(this).parents('form').serialize(),
        };
        coursesLoad( params );
    })
    // Фильтрация по дате начала - "по"
    $(document).on('change', '.coursesFilterForm input[name=max_date]', function(e){
        var params = {
            is_filter: 'Y',
            max_date: $(this).val(),
            form_data: $(this).parents('form').serialize(),
        };
        coursesLoad( params );
    })





    $(document).on('click', '.team_country_button', function(e){
        if( !is_process(this) ){
            var button = $(this);
            var url = rawurldecode( document.URL );
            process(true);
            var postParams = {};
            if( $(button).hasClass('active') ){
                $(button).removeClass('active');
            } else {
                $('.team_country_button').removeClass('active');
                $(button).addClass('active');
                postParams.country = $(button).attr('item_id');
            }
            process(true);
            $.post("/ajax/team_filter.php", postParams, function(data){
                if (data.status == 'ok'){
                    var new_url = url;
                    if( postParams.country == undefined ){
                        new_url = removeFromRequestURI( new_url, 'country' );
                    } else {
                        new_url = addToRequestURI( new_url, 'country', postParams.country );
                    }
                    history.replaceState({}, null, new_url);
                    $('.teamLoadArea').html( data.html );
                } else if (data.status == 'error'){
                    alert( data.text );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                alert( 'Ошибка запроса' );
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })












});