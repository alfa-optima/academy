



$(document).ready(function(){



    // Авторизация
    $(document).on('click', '.authButton', function(e){
        var button = $(this);
        if( !is_process(button) ){

            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var login = $(this_form).find("input[name=login]").val();
            var password = $(this_form).find("input[name=password]").val();
            var errors = [];

            // Проверки
            if (login.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=login]"),
                    text: 'Введите, пожалуйста, Ваш Email / Логин!'
                });
            } else if (password.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=password]"),
                    text: 'Введите, пожалуйста, Ваш пароль!'
                });
            }

            if ( errors.length == 0 ){

                process(true);

                var postParams = {
                    form_data: $(this_form).serialize()
                };
                $.post("/ajax/auth.php", postParams, function(data){
                    if (data.status == 'ok'){

                        var url = document.URL;
                        var ar = url.split('#');
                        window.location.href = ar[0];

                    } else if (data.status == 'error'){
                        show_error( this_form, data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    show_error( this_form, 'Ошибка запроса' );
                })
                .always(function(data, status, xhr){
                    process(false);
                })

            // Ошибка
            } else {
                show_error(this_form, errors);
            }
        }
    })



    // Регистрация
    $(document).on('click', '.registerButton', function(e){
        e.preventDefault();
        var button = $(this);
        if( !is_process(this) ){

            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var email = $(this_form).find("input[name=email]").val();
            var f = $(this_form).find("input[name=f]").val();
            var i = $(this_form).find("input[name=i]").val();
            var o = $(this_form).find("input[name=o]").val();
            var password = $(this_form).find("input[name=password]").val();
            var confirm_password = $(this_form).find("input[name=confirm_password]").val();
            var userAgreement = $(this_form).find("input[name=userAgreement]:checked").length > 0;
            var errors = [];
            // Проверки
            if (email.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=email]"),
                    text: 'Введите, пожалуйста, Ваш Email!'
                });
            } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))){
                errors.push({
                    link: $(this_form).find("input[name=email]"),
                    text: 'Email содержит ошибки!'
                });
            } else if (f.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=f]"),
                    text: 'Введите, пожалуйста, Вашу фамилию!'
                });
            } else if (i.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=i]"),
                    text: 'Введите, пожалуйста, Ваше имя!'
                });
            // } else if (o.length == 0){
            //     errors.push({
            //         link: $(this_form).find("input[name=o]"),
            //         text: 'Введите, пожалуйста, Ваше отчество!'
            //     });
            } else if (password.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=password]"),
                    text: 'Придумайте и введите пароль!'
                });
            } else if ( password.length < 6 ){
                errors.push({
                    link: $(this_form).find("input[name=password]"),
                    text: 'Минимальная длина пароля 6 символов'
                });
            } else if (confirm_password.length == 0){
                errors.push({
                    link: $(this_form).find("input[name=confirm_password]"),
                    text: 'Повторите ввод пароля!'
                });
            } else if ( password != confirm_password ){
                errors.push({
                    link: $(this_form).find("input[name=confirm_password]"),
                    text: 'Пароли отличаются'
                });
            } else if ( !userAgreement ){
                errors.push({
                    link: $(this_form).find("input[name=userAgreement]"),
                    text: 'Отметьте, пожалуйста, согласие с пользовательским соглашением'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/register.php", { form_data:form_data }, function(data){
                    if (data.status == 'ok'){

                        show_message( "На указанную почту <u>отправлена ссылка</u> для подтверждения регистрации", 'success', 'click' );

                        $(this_form).replaceWith( "<p class='success___p'>На указанную почту <u>отправлена ссылка</u> для подтверждения регистрации</p>" );

                    } else if (data.status == 'error'){

                        show_error( this_form, data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    show_error( this_form, "Ошибка запроса" );
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, errors);
            }

        }
    })



    // Запрос ссылки для восстановления пароля
    $(document).on('click', '.passwordRecoveryRequestButton', function(e){
        var button = $(this);
        $.noty.closeAll();
        if( !is_process(button) ){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var email = $(this_form).find("input[name=email]").val();
            var errors = [];
            // Проверки
            if ( email.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=email]"),
                    'text': 'Введите, пожалуйста, Ваш Email!'
                });
            } else if ( email.length > 0 && !(/^.+@.+\..+$/.test(email)) ){
                errors.push({
                    'link': $(this_form).find("input[name=email]"),
                    'text': 'Email содержит ошибки!'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/passwordRecoveryRequest.php", { form_data:form_data }, function(data){
                    if( data.status == 'ok' ){

                        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: 'Ссылка для&nbsp;восстановления пароля отправлена на&nbsp;указанный&nbsp;Email'});

                        $(button).parents('div.col-lg-6').html( "<p class='success___p'>На указанную почту <u>отправлена ссылка</u> для подтверждения регистрации</p>" );

                    } else if( data.status == 'error' ){
                        errors.push({ 'link': null, 'text': data.text });
                        show_error( this_form, errors );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса...'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error( this_form, errors );
            }
        }
    })

    // Установка нового пароля - восстановление
    $(document).on('click', '.passwordRecoverySaveButton', function(e){
        var button = $(this);
        $.noty.closeAll();
        if( !is_process(button) ){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var password = $(this_form).find("input[name=password]").val();
            var password_confirm = $(this_form).find("input[name=password_confirm]").val();
            var errors = [];
            // Проверки
            if( password.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=password]"),
                    'text': 'Введите, пожалуйста, новый пароль!'
                });
            } else if( password.length < minPassLengthNum ){
                errors.push({
                    'link': $(this_form).find("input[name=password]"),
                    'text': 'Длина пароля - не менее '+minPassLength
                });
            } else if( password_confirm.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=password_confirm]"),
                    'text': 'Введите, пожалуйста, пароль повторно!'
                });
            } else if( password != password_confirm ){
                errors.push({
                    'link': $(this_form).find("input[name=password_confirm]"),
                    'text': 'Пароли не совпадают!'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/passwordRecoverySave.php", { form_data:form_data }, function(data){
                    if( data.status == 'ok' ){
                        history.replaceState({}, null, '/password_recovery/');
                        $('.recoveryArea').replaceWith( data.html );
                    } else if( data.status == 'error' ){
                        errors.push({ 'link': null, 'text': data.text });
                        show_error( this_form, errors );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса...'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error( this_form, errors );
            }
        }
    });




    // Редактирование личных данных
    $(document).on('click', '.personalEditDataSave', function(e){
        var button = $(this);
        $.noty.closeAll();
        if( !is_process(button) ){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            //var EMAIL = $(this_form).find("input[name=EMAIL]").val();
            var LAST_NAME = $(this_form).find("input[name=LAST_NAME]").val();
            var NAME = $(this_form).find("input[name=NAME]").val();
            var SECOND_NAME = $(this_form).find("input[name=SECOND_NAME]").val();
            var PERSONAL_BIRTHDAY = $(this_form).find("input[name=PERSONAL_BIRTHDAY]").val();
            var PERSONAL_GENDER = $(this_form).find("select[name=PERSONAL_GENDER]").val();
            var PERSONAL_PHONE = $(this_form).find("input[name=PERSONAL_PHONE]").val();
            var PERSONAL_CITY = $(this_form).find("input[name=PERSONAL_CITY]").val();
            var UF_AREA_OF_ACTIVITY = $(this_form).find("select[name=UF_AREA_OF_ACTIVITY]").val();
            var WORK_COMPANY = $(this_form).find("input[name=WORK_COMPANY]").val();
            var WORK_POSITION = $(this_form).find("input[name=WORK_POSITION]").val();
            var UF_HOW_DID_YOU_KNOW = $(this_form).find("select[name=UF_HOW_DID_YOU_KNOW]").val();
            var errors = [];
            // Проверки
            if( LAST_NAME.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=LAST_NAME]"),
                    'text': 'Введите, пожалуйста, вашу фамилию!'
                });
            } else if( NAME.length == 0){
                errors.push({
                    'link': $(this_form).find("input[name=NAME]"),
                    'text': 'Введите, пожалуйста, ваше имя!'
                });
            // } else if( SECOND_NAME.length == 0){
            //     errors.push({
            //         'link': $(this_form).find("input[name=SECOND_NAME]"),
            //         'text': 'Введите, пожалуйста, ваше отчество!'
            //     });
            } else if( PERSONAL_BIRTHDAY.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=PERSONAL_BIRTHDAY]"),
                    'text': 'Укажите, пожалуйста, вашу дату рождения!'
                });
            } else if( PERSONAL_GENDER == 'empty'){
                errors.push({
                    'link': $(this_form).find("select[name=PERSONAL_GENDER]"),
                    'text': 'Укажите, пожалуйста, ваш пол!'
                });
            } else if( PERSONAL_PHONE.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=PERSONAL_PHONE]"),
                    'text': 'Укажите, пожалуйста, ваш номер телефона!'
                });
            } else if (PERSONAL_PHONE.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(PERSONAL_PHONE))){
                errors.push({
                    'link': $(this_form).find("input[name=PERSONAL_PHONE]"),
                    'text': 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'
                });
            } else if( PERSONAL_CITY.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=PERSONAL_CITY]"),
                    'text': 'Укажите, пожалуйста, ваш город!'
                });
            } else if( UF_AREA_OF_ACTIVITY == 'empty'){
                errors.push({
                    'link': $(this_form).find("select[name=UF_AREA_OF_ACTIVITY]"),
                    'text': 'Укажите, пожалуйста, вашу сферу деятельности!'
                });
            } else if( WORK_COMPANY.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=WORK_COMPANY]"),
                    'text': 'Укажите, пожалуйста, ваше место работы!'
                });
            } else if( WORK_POSITION.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=WORK_POSITION]"),
                    'text': 'Укажите, пожалуйста, вашу должность!'
                });
            } else if( UF_HOW_DID_YOU_KNOW == 'empty'){
                errors.push({
                    'link': $(this_form).find("select[name=UF_HOW_DID_YOU_KNOW]"),
                    'text': 'Укажите, пожалуйста, откуда вы о нас узнали!'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/personalEditDataSave.php", { form_data:form_data }, function(data){
                    if( data.status == 'ok' ){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'success', text: 'Сохранено...'});

                    } else if( data.status == 'error' ){
                        errors.push({ 'link': null, 'text': data.text });
                        show_error( this_form, errors );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса...'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error( this_form, errors );
            }
        }
    });



    // Редактирование пароля - ЛК
    $(document).on('click', '.personalEditPasswordSave', function(e){
        var button = $(this);
        $.noty.closeAll();
        if( !is_process(button) ){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            //var EMAIL = $(this_form).find("input[name=EMAIL]").val();
            var OLD_PASSWORD = $(this_form).find("input[name=OLD_PASSWORD]").val();
            var PASSWORD = $(this_form).find("input[name=PASSWORD]").val();
            var CONFIRM_PASSWORD = $(this_form).find("input[name=CONFIRM_PASSWORD]").val();
            var errors = [];
            // Проверки
            if( OLD_PASSWORD.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=OLD_PASSWORD]"),
                    'text': 'Введите, пожалуйста, действующий пароль!'
                });
            } else if( PASSWORD.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=PASSWORD]"),
                    'text': 'Введите, пожалуйста, новый пароль!'
                });
            } else if( PASSWORD.length < minPassLengthNum ){
                errors.push({
                    'link': $(this_form).find("input[name=PASSWORD]"),
                    'text': 'Длина пароля - не менее '+minPassLength
                });
            } else if( CONFIRM_PASSWORD.length == 0 ){
                errors.push({
                    'link': $(this_form).find("input[name=CONFIRM_PASSWORD]"),
                    'text': 'Введите, пожалуйста, пароль повторно!'
                });
            } else if( PASSWORD != CONFIRM_PASSWORD ){
                errors.push({
                    'link': $(this_form).find("input[name=CONFIRM_PASSWORD]"),
                    'text': 'Пароли не совпадают!'
                });
            }
            // Если нет ошибок
            if ( errors.length == 0 ){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/personalEditPasswordSave.php", { form_data:form_data }, function(data){
                    if( data.status == 'ok' ){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'success', text: 'Сохранено...'});

                        $(this_form).find("input[name=OLD_PASSWORD]").val('');
                        $(this_form).find("input[name=PASSWORD]").val('');
                        $(this_form).find("input[name=CONFIRM_PASSWORD]").val('');

                    } else if( data.status == 'error' ){
                        errors.push({ 'link': null, 'text': data.text });
                        show_error( this_form, errors );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса...'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error( this_form, errors );
            }
        }
    });



    // Подгрузка активных курсов в ЛК
    $(document).on('click', '.personalActiveCoursesLoadButton', function(e){
        var button = $(this);
        if( !is_process(button) ){

            var stop_ids = [];
            $('.course__item').each(function(){
                stop_ids.push( $(this).attr('item_id') );
            });

            var postParams = {
                stop_ids: stop_ids
            };
            process(true);
            $.post("/ajax/personalActiveCoursesLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    var html = data.html;
                    var loadHtml = $(html).find('.personalActiveCoursesLoadArea').html();

                    if( loadHtml != undefined ){
                        $('.personalActiveCoursesLoadArea').append( loadHtml );
                        if( $(html).find('.personalActiveCoursesLoadBlock.isVisible').length > 0 ){
                            $('.personalActiveCoursesLoadBlock').show();
                        } else {
                            $('.personalActiveCoursesLoadBlock').hide();
                        }
                    } else {
                        $('.personalActiveCoursesLoadBlock').hide();
                    }

                } else if (data.status == 'error'){
                    show_message( data.text );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                show_message( 'Ошибка запроса' );
            })
            .always(function(data, status, xhr){
                process(false);
            })

        }
    })



    // Подгрузка пройденных курсов в ЛК
    $(document).on('click', '.personalCompletedCoursesLoadButton', function(e){
        var button = $(this);
        if( !is_process(button) ){

            var stop_ids = [];
            $('.reg__item').each(function(){
                stop_ids.push( $(this).attr('item_id') );
            });

            var postParams = {
                stop_ids: stop_ids
            };
            process(true);
            $.post("/ajax/personalCompletedCoursesLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    var html = data.html;
                    var loadHtml = $(html).find('.personalCompletedCoursesLoadArea tbody').html();

                    if( loadHtml != undefined ){
                        $('.personalCompletedCoursesLoadArea tbody').append( loadHtml );
                        if( $(html).find('.personalCompletedCoursesLoadBlock.isVisible').length > 0 ){
                            $('.personalCompletedCoursesLoadBlock').show();
                        } else {
                            $('.personalCompletedCoursesLoadBlock').hide();
                        }
                    } else {
                        $('.personalCompletedCoursesLoadBlock').hide();
                    }

                } else if (data.status == 'error'){
                    show_message( data.text );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                show_message( 'Ошибка запроса' );
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })



})