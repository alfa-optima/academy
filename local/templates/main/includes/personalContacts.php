<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="form-text mt-4 pt-2">
    <p>Консультация по продуктам <a href="mailto:info@knauf.ru">info@knauf.ru</a></p>
    <p>Телефон <a href="tel:88007707667">8 (800) 770-76-67</a></p>
</div>

<div class="form-text">
    <p>По вопросам работы сайта обращайтесь,</p>
    <p>e-mail <a href="mailto:knauf.content@knauf.ru">knauf.content@knauf.ru</a></p>
    <p>Телефон <a href="tel:+79253225001">+7 (925) 322-50-01</a></p>
</div>
