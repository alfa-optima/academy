<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead()?>

    <? if( 0 ){ ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <? } ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" integrity="sha512-f0tzWhCwVFS3WeYaofoLWkTP62ObhewQ1EZn65oSYDZUg1+CyywGKkWzm8BxaJj5HGKI72PnMH9jYyIFz+GH7g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.2.0/swiper-bundle.css" integrity="sha512-blNStuTXAH9ZQGXnfUBp11D2aqRfiK/8pmAUpbNgTsjKgu6uha0S8Zp+aFlk+usVZ3jNzwHgkf5yxvPe3yBdyg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/style.css?v=<?=time()?>">
    <link href="<?=SITE_TEMPLATE_PATH?>/assets/css/my.css?v=<?=time()?>" rel="stylesheet">

    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <? if( 0 ){ ?>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <? } ?>

    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/noty/packaged/jquery.noty.packaged.min.js"></script>

    <script>
    const minPassLengthNum = <?=project\user::PASSWORD_MIN_LENGTH?>;
    const minPassLength = "<?=project\user::PASSWORD_MIN_LENGTH?> <?=tools\funcs::pfCnt(project\user::PASSWORD_MIN_LENGTH, "символ", "символа", "символов")?>";
    </script>

</head>

<body>

    <div id="panel"><?$APPLICATION->ShowPanel();?></div>

    <div class="wrapper">
