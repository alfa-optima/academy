<?php

// Массив событий модуля.


$MODULE_EVENTS = array(

    array(
        'iblock',
        'OnAfterIBlockAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnAfterIBlockUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnIBlockDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnIBlockDelete',
        100
    ),

    array(
        'iblock',
        'OnBeforeIBlockAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockDelete',
        100
    ),





    array(
        'iblock',
        'OnAfterIBlockElementAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockElementDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockElementDelete',
        100,
    ),


    array(
        'iblock',
        'OnAfterIBlockSectionAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockSectionDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockSectionDelete',
        100,
    ),



    array(
        'catalog',
        'OnSuccessCatalogImport1C',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnSuccessCatalogImport1C',
        100,
    ),



    array(
        'main',
        'OnAfterUserAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterUserAdd',
        100,
    ),
    array(
        'main',
        'OnAfterUserUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterUserUpdate',
        100,
    ),
    array(
        'main',
        'OnBeforeUserDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeUserDelete',
        100,
    ),





);

