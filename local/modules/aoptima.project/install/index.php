<?
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config as Conf;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

Loc::loadMessages(__FILE__);
Class AOPTIMA_PROJECT extends CModule {
	
    var $exclusionAdminFiles;

	
	function __construct(){
		
		$arModuleVersion = array();
		include(__DIR__."/version.php");
		
        $MODULE_EVENTS = array();
        include(__DIR__."/events.php");
		$this->events = $MODULE_EVENTS;

        $this->exclusionAdminFiles=array(
            '..', '.', 'menu.php',
			'operation_description.php', 'task_description.php'
        );

        $this->MODULE_ID = 'aoptima.project';
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("AOPTIMA_PROJECT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("AOPTIMA_PROJECT_MODULE_DESC");

		$this->PARTNER_NAME = Loc::getMessage("AOPTIMA_PROJECT_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("AOPTIMA_PROJECT_PARTNER_URI");

        $this->MODULE_SORT = 1;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS='Y';
        $this->MODULE_GROUP_RIGHTS = "Y";
	}

	
    // Определяем место размещения модуля
    public function GetPath($notDocumentRoot=false){
        if($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(),'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

	
    //Проверяем что система поддерживает D7
    public function isVersionD7(){
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

	
    function InstallDB(){}

	
    function UnInstallDB(){}

	
	function InstallEvents(){
		$eventManager = \Bitrix\Main\EventManager::getInstance();
        foreach ($this->events as $event) {
            $eventManager->registerEventHandler(
				$event[0], $event[1], $event[2],
				$event[3], $event[4]
			);
        }
        return true;
	}

	
	function UnInstallEvents(){
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        foreach ($this->events as $event) {
            $eventManager->unRegisterEventHandler(
				$event[0], $event[1], $event[2],
				$event[3], $event[4]
			);
        }
        return true;
	}

	
	function InstallFiles( $arParams = array() ){
		
		$doc_root = $_SERVER['DOCUMENT_ROOT'];
 
 
 		$admin_path = $this->GetPath().'/install/admin/';
        if ( \Bitrix\Main\IO\Directory::isDirectoryExists( $admin_path ) ){
            if ( $dir = opendir( $admin_path ) ){
                while (false !== $item = readdir($dir)){
					if ( in_array($item, $this->exclusionAdminFiles) ){   continue;   }
                    file_put_contents(
						$doc_root.'/bitrix/admin/'.$this->MODULE_ID.'_'.$item,
                        '<'.'? require($_SERVER["DOCUMENT_ROOT"]."'.$this->GetPath(true).'/install/admin/'.$item.'");?'.'>'
					);
                }
                closedir($dir);
            }
        }
 
 

        return true;
	}

	function UnInstallFiles(){
		
		$doc_root = $_SERVER['DOCUMENT_ROOT'];
		

        
        if ( \Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/install/admin') ){
            if ($dir = opendir($path)) {
                while (false !== $item = readdir($dir)) {
                    if (in_array($item, $this->exclusionAdminFiles)){   continue;   }
                    \Bitrix\Main\IO\File::deleteFile( 
						$doc_root.'/bitrix/admin/'.$this->MODULE_ID.'_'.$item
					);
                }
                closedir($dir);
            }
        }
		
		
		
		return true;
	}

	
	function DoInstall(){
		global $APPLICATION;
        if($this->isVersionD7()){
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
			$this->InstallAgents();

            #работа с .settings.php
            $configuration = Conf\Configuration::getInstance();
            $aoptima_module_project=$configuration->get('aoptima_module_project');
            $aoptima_module_project['install']=$aoptima_module_project['install']+1;
            $configuration->add('aoptima_module_project', $aoptima_module_project);
            $configuration->saveConfiguration();
            #работа с .settings.php
        } else {
            $APPLICATION->ThrowException(Loc::getMessage("AOPTIMA_PROJECT_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("AOPTIMA_PROJECT_INSTALL_TITLE"), $this->GetPath()."/install/step.php");
	}

	
	function DoUninstall(){
        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if( $request["step"] < 2 ){
			
            $APPLICATION->IncludeAdminFile(Loc::getMessage("AOPTIMA_PROJECT_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep1.php");
			
        } elseif( $request["step"] == 2 ) {
			
            $this->UnInstallFiles();
			$this->UnInstallEvents();
			$this->UnInstallAgents();

            if($request["savedata"] != "Y")
                $this->UnInstallDB();

			Option::delete($this->MODULE_ID);
			
            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            #работа с .settings.php
            $configuration = Conf\Configuration::getInstance();
            $aoptima_module_project=$configuration->get('aoptima_module_project');
            $aoptima_module_project['uninstall']=$aoptima_module_project['uninstall']+1;
            $configuration->add('aoptima_module_project', $aoptima_module_project);
            $configuration->saveConfiguration();
            #работа с .settings.php

            $APPLICATION->IncludeAdminFile(Loc::getMessage("AOPTIMA_PROJECT_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep2.php");
        }
	}

	
    protected function InstallAgents() {
        return true;
    }

	
    protected function UnInstallAgents() {
        $agent = new CAgent();
        $agent->RemoveModuleAgents($this->MODULE_ID);
        return true;
    }
	
	
    function GetModuleRightList(){
        return array(
            "reference_id" => array("D","K","S","W"),
            "reference" => array(
                "[D] ".Loc::getMessage("AOPTIMA_PROJECT_DENIED"),
                "[K] ".Loc::getMessage("AOPTIMA_PROJECT_READ_COMPONENT"),
                "[S] ".Loc::getMessage("AOPTIMA_PROJECT_WRITE_SETTINGS"),
                "[W] ".Loc::getMessage("AOPTIMA_PROJECT_FULL"))
        );
    }
	
	
} 

?>