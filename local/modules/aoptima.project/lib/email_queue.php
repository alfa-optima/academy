<?php

namespace AOptima\Project;
use AOptima\Project as project;



class email_queue {

    const PROCESS_MAX_TIME = 56;



    // ID хайлоад-блока
    static function getHiblockID(){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        return \AOptima\Tools\config::getValue('EMAIL_QUEUE_HIBLOCK_ID');
    }



    // Добавление в очередь
    static function add( $mail_title, $mail_html, $email_to ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $logPrefix = 'Очередь Email - добавление в очередь '.json_encode([
                'email_to' => $email_to,
            ], JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE).' - ';

        $hiblock = new \AOptima\Tools\hiblock( static::getHiblockID() );

        $arData = [
            'UF_EMAIL_TO' => trim(strip_tags($email_to)),
            'UF_MAIL_TITLE' => trim(strip_tags($mail_title)),
            'UF_MAIL_HTML' => trim(html_entity_decode($mail_html)),
        ];
        $res = $hiblock->entity_data_class::add( $arData );
        if( $res->isSuccess() ){
            return true;
        } else {
            $error = implode(', ', $res->getErrors());
            \AOptima\Tools\logger::addError( $logPrefix.$error );
        }
        return false;
    }



    // Продвижение очереди
    static function go(){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $hiblock = new \AOptima\Tools\hiblock( static::getHiblockID() );

        $start_time = microtime( true );

        $used_emails = [];

        // Запросим записи в очереди
        $rsData = $hiblock->entity_data_class::getList([
            "select" => [ '*' ],  "filter" => [],  "order" => [ "ID" => "ASC" ]
        ]);
        $items = new \CDBResult($rsData, $sTableID);

        while( $item = $items->Fetch() ){

            if( ( microtime( true ) - $start_time ) < static::PROCESS_MAX_TIME ){

                if( !in_array( $item['UF_EMAIL_TO'], $used_emails ) ){

                    \AOptima\Tools\feedback::sendMainEvent(
                        $item['UF_MAIL_TITLE'],
                        $item['UF_MAIL_HTML'],
                        $item['UF_EMAIL_TO']
                    );

                    $used_emails[] = $item['UF_EMAIL_TO'];

                    // Удаляем запись из очереди
                    $hiblock->entity_data_class::delete( $item['ID'] );
                }

            } else {

                break;
            }
        }
    }



}