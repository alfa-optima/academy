<? namespace AOptima\Project;
use AOptima\Project as project;



class program {

    const IBLOCK_ID = 1;



    static function all_items(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $filter = [
        	"IBLOCK_ID" => static::IBLOCK_ID,
        	"ACTIVE" => "Y"
        ];
        $fields = [ "ID", "NAME", 'PREVIEW_PICTURE', 'PREVIEW_TEXT' ];
        $sort = [ "SORT" => "ASC" ];
        $hash = md5(json_encode($filter).json_encode($fields).json_encode($sort));
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'all_programs_'.$hash;
        $cache_path = '/all_programs/'.$hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbElements = \CIBlockElement::GetList(
                $sort, $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[] = $element;
            }
        $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }






}