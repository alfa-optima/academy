<? namespace AOptima\Project;
use AOptima\Project as project;

use Bitrix\Main\Config\Configuration;



class certificate {


    const PATH = '/local/php_interface/certificates';



    // ID хайлоад-блока
    static function getHiblockID(){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        return \AOptima\Tools\config::getValue('CERTIFICATES_HIBLOCK_ID');
    }


    function getPDO(){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $config = Configuration::getInstance();
        $conn = $config->get( 'connections' )['default'];
        if( isset( $conn ) ){
            $dsn = "mysql:host=127.0.0.1;dbname=".$conn['database'].";charset=utf8";
            $opt = array(
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            );
            $pdo = new \PDO($dsn, $conn['login'], $conn['password'], $opt);
            return $pdo;
        }
        return null;
    }



    static function multi_add( $cnt ){
        ///
        ini_set('memory_limit', '1524M');
        ///
        $conn = static::getPDO();
        if( isset( $conn ) ){
            $cnts = [];
            for ( $n = 1; $n <= $cnt; $n++ ){    $cnts[] = $n;    }
            $chunks = array_chunk($cnts, 300);
            unset($cnts);
            foreach ( $chunks as $chunk ){
                $sql = "INSERT INTO aoptima_certificates ( `UF_NUM_PP`, `UF_DATE`, `UF_YEAR`, `UF_REGISTER`, `UF_FILE_LINK` ) VALUES ";
                $k = 0;
                foreach ( $chunk as $n ){ $k++;
                    $months = [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ];
                    $years = [ "2019", "2020", "2021" ];
                    shuffle($years);
                    shuffle($months);
                    //$number = 'RU1-TC1-'.str_pad($n, 8, '0', STR_PAD_LEFT).'-'.substr($years[0], -2, 2);
                    $date = $years[0].'-'.$months[0].'-01';
                    if( $k > 1 ){    $sql .= ", ";    }
                    $sql .= "\n"."( \"".$n."\", \"".$date."\", \"".substr($years[0], -2, 2)."\", \"".(54 + $n - 1)."\", NULL )";
                }
                $result = $conn->query($sql);
            }
        }
    }

    

    // Следующий номер сертификата
    static function getNextNumPP( $y ){
        $conn = static::getPDO();
        if( isset( $conn ) ){
            $sql = "SELECT * FROM aoptima_certificates ";
            $sql .= "WHERE UF_YEAR = ".$y." ";
            $sql .= "ORDER BY UF_NUM_PP DESC ";
            $sql .= "LIMIT 1";
            $result = $conn->query($sql);
            if ( $row = $result->fetch() ){
                return $row['UF_NUM_PP']+1;
            } else {
                return 1;
            }
        }
        return null;
    }



    static function generateNumber( $register_id, $num_pp, $short_year ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $register = \AOptima\Tools\el::info( $register_id );
        if( intval( $register['ID'] ) > 0 ){
            $course = \AOptima\Tools\el::info( $register['PROPERTY_COURSE_VALUE'] );
            if( intval( $course['ID'] ) > 0 ){
                $program = \AOptima\Tools\el::info( $course['PROPERTY_PROGRAM_VALUE'] );
                $center = \AOptima\Tools\el::info( $course['PROPERTY_CENTER_VALUE'] );
                if(
                    intval( $program['ID'] ) > 0
                    &&
                    intval( $center['ID'] ) > 0
                ){
                    $number = $center['PROPERTY_REGION_CODE_VALUE'].'-'.$center['PROPERTY_TYPE_NUMBER_VALUE'].'-'.str_pad($num_pp, 8, '0', STR_PAD_LEFT).'-'.$short_year;
                    return $number;
                }
            }
        }
        return null;
    }



    static function getByRegisterID( $register_id ){
        $hiblock = new \AOptima\Tools\hiblock( static::getHiblockID() );
        $certificates = $hiblock->entity_data_class::getList([
            "select" => [ "ID", "UF_NUM_PP", "UF_DATE", "UF_YEAR", "UF_REGISTER" ],
            "filter" => [ "UF_REGISTER" => $register_id ],
            "limit" => 1,
        ]);
        if ( $certificate = $certificates->Fetch() ){
            $filePath = static::filePath( $certificate['UF_REGISTER'] );
            if( file_exists( $filePath ) ){
                $certificate['NUMBER'] = '/certificate/'.$certificate['UF_REGISTER'].'/';
                $certificate['FILE_PATH'] = '/certificate/'.$certificate['UF_REGISTER'].'/';
                return $certificate;
            }
        }
        return null;
    }
    
    

    // Создание/регистрация сертификата
    static function create( $register_id ){

        global $APPLICATION;
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $register = \AOptima\Tools\el::info( $register_id );

        if( intval( $register['ID'] ) > 0 ){

            $hiblock = new \AOptima\Tools\hiblock( static::getHiblockID() );

            // путь к файлу сертификата
            $cert_file_path = static::filePath( $register_id );

            $logPrefix = 'Создание/регистрация (в БД) сертификата '.json_encode([
                'register_id' => $register_id,
            ], JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE).' - ';

            // Проверим есть ли уже запись в БД о сертификате
            $notes = $hiblock->entity_data_class::getList([
                "select" => [ "ID", "UF_REGISTER", "UF_DATE", "UF_YEAR", "UF_NUM_PP" ],
                "filter" => [ "UF_REGISTER" => $register_id ],
                "limit" => 1,
            ]);
            // Если запись УЖЕ ЕСТЬ
            if ( $note = $notes->Fetch() ){

                // Номер сертификата
                $certificateNumber = static::generateNumber(
                    $register['ID'],
                    $note['UF_NUM_PP'],
                    $note['UF_YEAR']
                );

                $certificateDate = ConvertDateTime( $note['UF_DATE'], "DD.MM.YYYY", "ru" );

                // Получим разметку сертификата
                ob_start();
                    $APPLICATION->IncludeComponent(
                        "aoptima:certificate", "",
                        [
                            'REGISTER_ID' => $register_id,
                            'certificateNumber' => $certificateNumber,
                            'certificateDate' => $certificateDate,
                        ]
                    );
                    $cert_html = ob_get_contents();
                ob_end_clean();

                // сгенерируем сертификат
                project\pdf::generateFromHtml( $cert_html, $cert_file_path );

                // Сохраним ссылку на файл - в запись
                $file_src = file_exists( $cert_file_path )?str_replace(static::getPath(), "", $cert_file_path):null;
                $arData = [ 'UF_FILE_LINK' => $file_src ];
                $res = $hiblock->entity_data_class::update( $note['ID'], $arData );
                if( $res->isSuccess() ){} else {
                    $error = implode(', ', $res->getErrors());
                    \AOptima\Tools\logger::addError( $logPrefix.$error );
                }

            // Если записи ЕЩЁ НЕТ
            } else {

                // Текущий год
                $shortYear = date('y');
                $certificateDate = date('d.m.Y');

                // Сформируем новый порядковый номер сертификата
                $next_num_pp = project\certificate::getNextNumPP( $shortYear );

                // Создаём запись
                $arData = [
                    'UF_NUM_PP' => $next_num_pp,
                    'UF_DATE' => $certificateDate,
                    'UF_YEAR' => $shortYear,
                    'UF_REGISTER' => $register_id
                ];
                $res = $hiblock->entity_data_class::add( $arData );
                if( $res->isSuccess() ){

                    $id = $res->getId();

                    // Сгенерируем номер сертификата
                    $certificateNumber = static::generateNumber( $register['ID'], $next_num_pp, $shortYear );

                    // Получим разметку сертификата
                    ob_start();
                        $APPLICATION->IncludeComponent(
                            "aoptima:certificate", "",
                            [
                                'REGISTER_ID' => $register_id,
                                'certificateNumber' => $certificateNumber,
                                'certificateDate' => $certificateDate,
                            ]
                        );
                        $cert_html = ob_get_contents();
                    ob_end_clean();

                    // сгенерируем сертификат
                    project\pdf::generateFromHtml( $cert_html, $cert_file_path );

                    // Сохраним ссылку на файл - в запись
                    $file_src = file_exists( $cert_file_path )?str_replace(static::getPath(), "", $cert_file_path):null;
                    $arData = [ 'UF_FILE_LINK' => $file_src ];
                    $res = $hiblock->entity_data_class::update( $id, $arData );
                    if( $res->isSuccess() ){} else {
                        $error = implode(', ', $res->getErrors());
                        \AOptima\Tools\logger::addError( $logPrefix.$error );
                    }

                } else {
                    $error = implode(', ', $res->getErrors());
                    \AOptima\Tools\logger::addError( $logPrefix.$error );
                }
            }

            return file_exists( $cert_file_path );
        }

        return false;
    }



    static function getPath(){
        return $_SERVER['DOCUMENT_ROOT'].static::PATH;
    }


    static function getDir( $register_id ){
        $salt = 'jgA-2309jg-kF3qw-0gk23-09jtg0921Djg0923jg';
        $dir = static::getPath().'/'.md5($salt.$register_id);
        return $dir;
    }


    static function filePath( $register_id ){
        if( !file_exists( static::getPath() ) ){
            mkdir( static::getPath(), 0770 );
        }
        $dir = static::getDir( $register_id );
        if( !file_exists( $dir ) ){
            mkdir( $dir, 0770 );
        }
        $file_path = $dir.'/certificate.pdf';
        return $file_path;
    }


    static function delete( $register_id ){
        $dir = static::getDir( $register_id );
        $file_path = $dir.'/certificate.pdf';
        unlink( $file_path );
        rmdir( $dir );
    }



}