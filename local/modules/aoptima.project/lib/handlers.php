<? namespace AOptima\Project;
use AOptima\Project as project;



class handlers {



    static function OnAfterIBlockAdd( $arFields ){}
    static function OnAfterIBlockUpdate( $arFields ){}
    static function OnIBlockDelete( $ID ){}


    static function OnBeforeIBlockAdd( &$arFields ){}
    static function OnBeforeIBlockUpdate( &$arFields ){}
    static function OnBeforeIBlockDelete( $ID ){
        // Отмена удаления
        /*global $APPLICATION;
        $APPLICATION->throwException("Удаление инфоблоков заблокировано");
        return false;*/
    }





    static function OnAfterIBlockElementAdd( $arFields ){
        // Тренеры
        if( $arFields['IBLOCK_ID'] == project\team_member::IBLOCK_ID ){
            BXClearCache(true, "/team_members/");
        }
        // Программы
        if( $arFields['IBLOCK_ID'] == project\program::IBLOCK_ID ){
            BXClearCache(true, "/all_programs/");
        }
        // Страны
        if( $arFields['IBLOCK_ID'] == project\country::IBLOCK_ID ){
            BXClearCache(true, "/all_countries/");
        }
        // Центры
        if( $arFields['IBLOCK_ID'] == project\center::IBLOCK_ID ){
            BXClearCache(true, "/all_centers/");
        }
        // Формы обучения
        if( $arFields['IBLOCK_ID'] == project\learning_type::IBLOCK_ID ){
            BXClearCache(true, "/all_learning_types/");
        }
    }
    static function OnAfterIBlockElementUpdate( $arFields ){
        // Тренеры
        if( $arFields['IBLOCK_ID'] == project\team_member::IBLOCK_ID ){
            BXClearCache(true, "/team_members/");
        }
        // Программы
        if( $arFields['IBLOCK_ID'] == project\program::IBLOCK_ID ){
            BXClearCache(true, "/all_programs/");
        }
        // Страны
        if( $arFields['IBLOCK_ID'] == project\country::IBLOCK_ID ){
            BXClearCache(true, "/all_countries/");
        }
        // Центры
        if( $arFields['IBLOCK_ID'] == project\center::IBLOCK_ID ){
            BXClearCache(true, "/all_centers/");
        }
        // Формы обучения
        if( $arFields['IBLOCK_ID'] == project\learning_type::IBLOCK_ID ){
            BXClearCache(true, "/all_learning_types/");
        }
        // Курсы
        if( $arFields['IBLOCK_ID'] == project\learning_course::IBLOCK_ID ){
            project\learning_course::moderationEvent( $arFields );
        }
        // Регистрации на курсы
        if( $arFields['IBLOCK_ID'] == project\learning_course::REG_IBLOCK_ID ){
            project\learning_course::registerModerationEvent( $arFields );
            project\learning_course::certificateEvent( $arFields );
        }
    }
    static function OnAfterIBlockElementDelete(){}

    static function OnBeforeIBlockElementAdd( &$arFields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        // Курсы
        if( $arFields['IBLOCK_ID'] == project\learning_course::IBLOCK_ID ){

            $date_from_prop = \AOptima\Tools\prop::getByCode( project\learning_course::DATE_FROM_PROP_CODE, project\learning_course::IBLOCK_ID );
            $date_to_prop = \AOptima\Tools\prop::getByCode( project\learning_course::DATE_TO_PROP_CODE, project\learning_course::IBLOCK_ID );
            if(
                intval( $date_from_prop['ID'] ) > 0
                &&
                intval( $date_to_prop['ID'] ) > 0
            ){
                if( is_array( $arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ] ) ){
                    $date_from_str = $arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ])[0] ]['VALUE'];
                } else {
                    $date_from_str = $arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ];
                }
                if( is_array( $arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ] ) ){
                    $date_to_str = $arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ])[0] ]['VALUE'];
                } else {
                    $date_to_str = $arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ];
                }
                if(
                    strlen( $date_from_str ) > 0
                    &&
                    strlen( $date_to_str ) > 0
                ){
                    $date_from = date_create(ConvertDateTime($date_from_str, "YYYY-MM-DD HH:MI:SS", "ru"));
                    $date_to = date_create(ConvertDateTime($date_to_str, "YYYY-MM-DD HH:MI:SS", "ru"));
                    $invert = $date_from->diff($date_to)->invert;
                    if( $invert ){
                        global $APPLICATION;
                        $APPLICATION->throwException("Дата окончания (".$date_to_str.") не может быть меньше даты начала (".$date_from_str.")");
                        return false;
                    }
                }
            }
        }

        // Страны
        if( $arFields['IBLOCK_ID'] == project\country::IBLOCK_ID ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $arFields['NAME'] = trim(strip_tags($arFields['NAME']));
            $filter = [
                "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                "=NAME" => trim(strip_tags(mb_strtolower($arFields['NAME']))),
            ];
            $fields = [ "ID", "NAME" ];
            $sort = [ "SORT" => "ASC" ];
            $dbElements = \CIBlockElement::GetList(
                $sort, $filter, false, [ "nTopCount" => 1 ], $fields
            );
            if ($element = $dbElements->GetNext()){
                global $APPLICATION;
                $APPLICATION->throwException("Такая страна уже есть в справочнике");
                return false;
            }
        }

        // Формы обучения
        if( $arFields['IBLOCK_ID'] == project\learning_type::IBLOCK_ID ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $arFields['NAME'] = trim(strip_tags($arFields['NAME']));
            $filter = [
                "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                "=NAME" => trim(strip_tags(mb_strtolower($arFields['NAME']))),
            ];
            $fields = [ "ID", "NAME" ];
            $sort = [ "SORT" => "ASC" ];
            $dbElements = \CIBlockElement::GetList(
                $sort, $filter, false, [ "nTopCount" => 1 ], $fields
            );
            if ($element = $dbElements->GetNext()){
                global $APPLICATION;
                $APPLICATION->throwException("Такая форма обучения уже есть в справочнике");
                return false;
            }
        }

    }
    static function OnBeforeIBlockElementUpdate( &$arFields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        // Курсы
        if( $arFields['IBLOCK_ID'] == project\learning_course::IBLOCK_ID ){
            $date_from_prop = \AOptima\Tools\prop::getByCode( project\learning_course::DATE_FROM_PROP_CODE, project\learning_course::IBLOCK_ID );
            $date_to_prop = \AOptima\Tools\prop::getByCode( project\learning_course::DATE_TO_PROP_CODE, project\learning_course::IBLOCK_ID );
            if(
                intval( $date_from_prop['ID'] ) > 0
                &&
                intval( $date_to_prop['ID'] ) > 0
            ){
                if( is_array( $arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ] ) ){
                    $date_from_str = $arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ])[0] ]['VALUE'];
                } else {
                    $date_from_str = $arFields['PROPERTY_VALUES'][ $date_from_prop['ID'] ];
                }
                if( is_array( $arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ] ) ){
                    $date_to_str = $arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ])[0] ]['VALUE'];
                } else {
                    $date_to_str = $arFields['PROPERTY_VALUES'][ $date_to_prop['ID'] ];
                }
                if(
                    strlen( $date_from_str ) > 0
                    &&
                    strlen( $date_to_str ) > 0
                ){
                    $date_from = date_create(ConvertDateTime($date_from_str, "YYYY-MM-DD HH:MI:SS", "ru"));
                    $date_to = date_create(ConvertDateTime($date_to_str, "YYYY-MM-DD HH:MI:SS", "ru"));
                    $invert = $date_from->diff($date_to)->invert;
                    if( $invert ){
                        global $APPLICATION;
                        $APPLICATION->throwException("Дата окончания не может быть меньше даты начала");
                        return false;
                    }
                }
            }
        }

        // Страны
        if( $arFields['IBLOCK_ID'] == project\country::IBLOCK_ID ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $arFields['NAME'] = trim(strip_tags($arFields['NAME']));
            $filter = [
            	"IBLOCK_ID" => $arFields['IBLOCK_ID'],
            	"!ID" => $arFields['ID'],
            	"=NAME" => trim(strip_tags(mb_strtolower($arFields['NAME']))),
            ];
            $fields = [ "ID", "NAME" ];
            $sort = [ "SORT" => "ASC" ];
            $dbElements = \CIBlockElement::GetList(
            	$sort, $filter, false, [ "nTopCount" => 1 ], $fields
            );
            if ($element = $dbElements->GetNext()){
                global $APPLICATION;
                $APPLICATION->throwException("Такая страна уже есть в справочнике");
                return false;
            }
        }

        // Формы обучения
        if( $arFields['IBLOCK_ID'] == project\learning_type::IBLOCK_ID ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $arFields['NAME'] = trim(strip_tags($arFields['NAME']));
            $filter = [
                "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                "!ID" => $arFields['ID'],
                "=NAME" => trim(strip_tags(mb_strtolower($arFields['NAME']))),
            ];
            $fields = [ "ID", "NAME" ];
            $sort = [ "SORT" => "ASC" ];
            $dbElements = \CIBlockElement::GetList(
                $sort, $filter, false, [ "nTopCount" => 1 ], $fields
            );
            if ($element = $dbElements->GetNext()){
                global $APPLICATION;
                $APPLICATION->throwException("Такая форма обучения уже есть в справочнике");
                return false;
            }
        }

        // Регистрации на курсы
        if( $arFields['IBLOCK_ID'] == project\learning_course::REG_IBLOCK_ID ){
            $result = project\learning_course::checkCertificateEvent( $arFields );
            if( $result['status'] == 'error' ){
                global $APPLICATION;
                $APPLICATION->throwException( $result['text'] );
                return false;
            }
        }
        
    }
    static function OnBeforeIBlockElementDelete( $ID ){
        if( \Bitrix\Main\Loader::includeModule('aoptima.tools') ){
            $iblock_id = \AOptima\Tools\el::getIblock($ID);
            // Тренеры
            if( $iblock_id == project\team_member::IBLOCK_ID ){
                BXClearCache(true, "/team_members/");
            }
            // Программы
            if( $iblock_id == project\program::IBLOCK_ID ){
                BXClearCache(true, "/all_programs/");
            }
            // Страны
            if( $iblock_id == project\country::IBLOCK_ID ){
                BXClearCache(true, "/all_countries/");
            }
            // Центры
            if( $iblock_id == project\center::IBLOCK_ID ){
                BXClearCache(true, "/all_centers/");
            }
            // Формы обучения
            if( $iblock_id == project\learning_type::IBLOCK_ID ){
                BXClearCache(true, "/all_learning_types/");
            }
            // Курсы
            if( $iblock_id == project\learning_course::IBLOCK_ID ){
                // Удалим привязанные к курсу регистрации
                \Bitrix\Main\Loader::includeModule('iblock');
                $filter = [
                	"IBLOCK_ID" => project\learning_course::REG_IBLOCK_ID,
                	"ACTIVE" => "Y",
                	"PROPERTY_COURSE" => $ID
                ];
                $fields = [ "ID", "PROPERTY_COURSE" ];
                $sort = [ "SORT" => "ASC" ];
                $dbElements = \CIBlockElement::GetList(
                	$sort, $filter, false, false, $fields
                );
                while ( $element = $dbElements->GetNext() ){
                    \CIBlockElement::Delete( $element['ID'] );
                }
                // Удалим привязанные к курсу записи на уведомления
                \Bitrix\Main\Loader::IncludeModule("highloadblock");
                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(project\free_places_noty::HIBLOCK_ID)->fetch();
                $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                $entityDataClass = $entity->getDataClass();
                $result = $entityDataClass::getList(array(
                    "select" => [ "*" ],
                    "order" => [ "ID" => "DESC" ],
                    "filter" => [ "UF_COURSE" => $ID ],
                ));
                while ( $arRow = $result->Fetch() ){
                    $entityDataClass::delete( $arRow['ID'] );
                }
            }
        }
	}






    static function OnAfterIBlockSectionAdd( $arFields ){}
    static function OnAfterIBlockSectionUpdate( $arFields ){}
    static function OnAfterIBlockSectionDelete(){}


    static function OnBeforeIBlockSectionAdd( &$arFields ){}
    static function OnBeforeIBlockSectionUpdate( &$arFields ){}
    static function OnBeforeIBlockSectionDelete( $ID ){
        if( \Bitrix\Main\Loader::includeModule('aoptima.tools') ){
            $iblock_id = \AOptima\Tools\el::getIblock($ID);
        }
    }




    static function OnSuccessCatalogImport1C( $arParams, $filePath ){}





    static function OnAfterUserAdd(&$arFields){}
    static function OnAfterUserUpdate(&$arFields){}
    static function OnBeforeUserDelete($ID){}






}