<?php

namespace AOptima\Project;
use AOptima\Project as project;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;



class ispring_api {

    const ACCOUNT_URL = 'https://knauf-lms.ispringlearn.ru';
    const ACCOUNT_LOGIN = 'timofey.kuznecov@knauf.com';
    const ACCOUNT_PASSWORD = 'E2yJKm';


    static function baseUrl(){
        return 'https://api.direct.yandex.com/json/v5';
    }



    static function getUsers(){

        $logPrefix = 'Получение списка пользователей - ';

        $requestMethod = 'GET';
        $requestUrl = static::baseUrl().'/user';

        // Запрос к API
        $result = static::request(
            $logPrefix,
            $requestMethod,
            $requestUrl
        );

        return $result;
    }



    // Запрос к API
    static function request(
        $logPrefix,
        $requestMethod, // метод запроса
        $requestUrl, // URL запроса
        $arBody = null // массив тела запроса
    ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $body = null;
        if( isset( $arBody ) ){
            $body = json_encode( $arBody );
        }

        $client = new Client([ 'timeout' => 10, 'verify' => false ]);

        try {

            $headers = [
                'X-Auth-Account-Url' => static::ACCOUNT_URL,
                'X-Auth-Email' => static::ACCOUNT_LOGIN,
                'X-Auth-Password' => static::ACCOUNT_PASSWORD,
            ];

            $response = $client->request(
                $requestMethod, $requestUrl,
                [
                    'headers' => $headers,
                    'body' => $body
                ]
            );

            $code = $response->getStatusCode();
            $body = $response->getBody();
            $json = $body->getContents();

            if( $code == 200 ){

                $result = \AOptima\Tools\funcs::json_to_array( $json );

                if( is_array( $result ) ){

                    return $result;

                } else {
                    $error = $logPrefix.'некорректный json ответа - '.$result;
                    \AOptima\Tools\logger::addError( $error );
                    return $error;
                }
            } else {
                $error = $logPrefix.'код ответа '.$code;
                \AOptima\Tools\logger::addError( $error );
                return $error;
            }
        } catch(ClientException $ex){
            $error = $logPrefix.$ex->getMessage();
            \AOptima\Tools\logger::addError( $error );
            return $error;
        } catch(RequestException $ex){
            $error = $logPrefix.$ex->getMessage();
            \AOptima\Tools\logger::addError( $error );
            return $error;
        } catch(ConnectException $ex){
            $error = $logPrefix.$ex->getMessage();
            \AOptima\Tools\logger::addError( $error );
            return $error;
        }
    }





}