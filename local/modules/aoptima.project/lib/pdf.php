<? namespace AOptima\Project;
use AOptima\Project as project;

use Dompdf\Dompdf;



class pdf {





    static function generateFromHtml(
        $html, $file_path,
        $format = 'A4',
        $orient = 'portrait'
    ){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        $dompdf->setPaper($format, $orient);
        $dompdf->set_option('isRemoteEnabled', true);
        $dompdf->set_option('isHtml5ParserEnabled', true);

        $dompdf->render();

        //$dompdf->stream('certificate.pdf');

        $output = $dompdf->output();
        file_put_contents($file_path, $output);

        return file_exists( $file_path );
    }







}