<? namespace AOptima\Project;
use AOptima\Project as project;



class free_places_noty {

    const HIBLOCK_ID = 1;



    // ID хайлоад-блока
    static function getHiblockID(){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        return \AOptima\Tools\config::getValue('FREE_PLACES_NOTY_HIBLOCK_ID');
    }



    static function getList( $course_id = false ){
        \Bitrix\Main\Loader::IncludeModule("highloadblock");
        $list = [];
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(static::getHiblockID())->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();
        $filter = [];
        if( intval( $course_id ) > 0 ){
            $filter["UF_COURSE"] = $course_id;
        }
        $result = $entityDataClass::getList(array(
            "select" => [ "*" ],
            "order" => [ "ID" => "DESC" ],
            "filter" => $filter,
            "limit" => 1,
        ));
        while ( $arRow = $result->Fetch() ){
            $list[] = $arRow;
        }
        return $list;
    }



    static function hasItemForCourse( $course_id, $email ){
        \Bitrix\Main\Loader::IncludeModule("highloadblock");
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(static::getHiblockID())->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();
        $result = $entityDataClass::getList(array(
            "select" => [ "*" ],
            "order" => [ "ID" => "DESC" ],
            "filter" => [ "UF_COURSE" => $course_id, "UF_EMAIL" => $email ],
            "limit" => 1,
        ));
        if ( $arRow = $result->Fetch() ){
            return true;
        }
        return false;
    }



    static function add( $course_id, $email ){

        $logPrefix = 'Создание заявки на уведомления о новых местах на курс - ';

        \Bitrix\Main\Loader::IncludeModule("highloadblock");
        \Bitrix\Main\Loader::IncludeModule("aoptima.tools");

        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(static::getHiblockID())->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();

        $arFields = [
            'UF_COURSE' => $course_id,
            'UF_EMAIL' => $email,
        ];
        $result = $entityDataClass::add( $arFields );

        if( $result->isSuccess() ){

            return true;

        } else {
            $errors = $result->getErrorMessages();
            \AOptima\Tools\logger::addError( $logPrefix.implode("\n", $errors) );
        }
        return false;
    }









}