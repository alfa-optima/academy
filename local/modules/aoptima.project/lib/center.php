<? namespace AOptima\Project;
use AOptima\Project as project;



class center {

    const IBLOCK_ID = 3;



    static function countries(){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $countries = [];   $countriesNames = [];

        $items = static::all_items();

        foreach ( $items as $key => $item ){
            $country = \AOptima\Tools\el::info( $item['PROPERTY_COUNTRY_VALUE'] );
            if( intval( $country['ID'] ) > 0 ){
                $countries[ $country['ID'] ] = [
                    'ID' => $country['ID'],
                    'NAME' => $country['NAME'],
                    'CODE' => $country['CODE'],
                ];
                $countriesNames[ $country['ID'] ] = $country['NAME'];
            }
        }
        array_multisort($countriesNames, SORT_ASC, SORT_STRING, $countries);

        foreach ( $countries as $key => $country ){
            if( trim(mb_strtolower($country['NAME'])) == 'россия' ){
                unset( $countries[ $key ] );
                $countries = array_merge([$country], $countries);
            }
        }

        return $countries;
    }


    static function cities( $country_id = null ){
        $cities = [];
        $items = static::all_items( $country_id );
        $hasMoscow = false;
        foreach ( $items as $key => $item ){
            if( trim(mb_strtolower($item['PROPERTY_CITY_VALUE'])) == 'москва' ){
                $hasMoscow = true;
            } else {
                $cities[] = $item['PROPERTY_CITY_VALUE'];
            }
        }
        $cities = array_unique( $cities );
        sort($cities);
        if( $hasMoscow ){
            $cities = array_merge(['Москва'], $cities);
        }
        return $cities;
    }


    static function all_items_ids( $country_id = null, $city = null ){
        $ids = [];
        $items = static::all_items( $country_id, $city );
        foreach ( $items as $key => $item ){
            $ids[] = $item['ID'];
        }
        return $ids;
    }



    static function all_items( $country_id = null, $city = null ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $filter = [
            "IBLOCK_ID" => static::IBLOCK_ID,
            "ACTIVE" => "Y"
        ];
        if( intval($country_id) > 0 ){
            $filter['PROPERTY_COUNTRY'] = intval($country_id);
        }
        if( strlen(strip_tags(trim($city))) > 0 ){
            $filter['=PROPERTY_CITY'] = mb_strtolower(strip_tags(trim($city)));
        }
        $fields = [
            "ID", "NAME", "DETAIL_PAGE_URL", "CODE", "PROPERTY_CITY",
            "PROPERTY_COUNTRY", "PROPERTY_ADDRESS", "PROPERTY_PHONES", "PROPERTY_EMAIL"
        ];
        $sort = [ "SORT" => "ASC" ];
        $hash = md5(json_encode($filter).json_encode($fields).json_encode($sort));
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'all_centers_'.$hash;
        $cache_path = '/all_centers/'.$hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbElements = \CIBlockElement::GetList(
                $sort, $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[] = $element;
            }
            $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }








}