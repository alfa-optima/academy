<? namespace AOptima\Project;
use AOptima\Project as project;



class learning_course {

    const IBLOCK_ID = 2;
    const REG_IBLOCK_ID = 7;

    const DATE_FROM_PROP_CODE = 'DATE_FROM';
    const DATE_TO_PROP_CODE = 'DATE_TO';
    const LIST_CNT = 12;
    const PERSONAL_ACTIVE_LIST_CNT = 12;
    const PERSONAL_COMPLETED_LIST_CNT = 12;

    static $list_fields = [];

    static $list_props = [
        'DATE_FROM', 'DATE_TO', 'PROGRAM', 'CENTER', 'TYPE'
    ];



    static function moderationEvent( $arFields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');

        $filter = [
            "IBLOCK_ID" => $arFields['IBLOCK_ID'],
            "ID" => $arFields['ID']
        ];
        $fields = [ "ID", "PROPERTY_SEND_FREE_PLACES_NOTIES" ];
        $sort = [ "SORT" => "ASC" ];
        $courses = \CIBlockElement::GetList(
            $sort, $filter, false, [ "nTopCount" => 1 ], $fields
        );
        while ( $course = $courses->GetNext() ){

            // Рассылка писем подписавшимся на уведомления об освобождении свободных мест
            if(
                // Если проставлена галочка
                $course['PROPERTY_SEND_FREE_PLACES_NOTIES_VALUE']
                &&
                // Если количество свободных мест больше нуля
                project\learning_course::freePlacesCnt( $course['ID'] ) > 0
            ){
                
                // Получим записи на уведомления
                $items = project\free_places_noty::getList( $course['PROPERTY_COURSE_VALUE'] );

                // Переберём записи
                foreach ( $items as $key => $item ){

                    // параметры письма
                    $email_to = $item['UF_EMAIL'];
                    $mail_title = 'Уведомление о свободных местах на курс в Академии Кнауф';
                    ob_start();
                        global $APPLICATION;
                        $APPLICATION->IncludeComponent(
                            "aoptima:mail_courseFreePlacesNoty", "",
                            [
                                "course_id" => $course['ID'],
                            ]
                        );
                        $mail_html = ob_get_contents();
                    ob_end_clean();

                    // Добавляем в очередь
                    project\email_queue::add( $mail_title, $mail_html, $email_to );
                }

                // Уберём галочку
                $set_prop = ["SEND_FREE_PLACES_NOTIES" => 0];
                \CIBlockElement::SetPropertyValuesEx($course['ID'], $arFields['IBLOCK_ID'], $set_prop);
            }
        }
    }



    static function registerModerationEvent( $arFields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');

        $filter = [
        	"IBLOCK_ID" => $arFields['IBLOCK_ID'],
        	"ID" => $arFields['ID']
        ];
        $fields = [
            "ID", "PROPERTY_MODERATION", "PROPERTY_MODERATION_MAIL_SENT",
            "PROPERTY_EMAIL", "PROPERTY_COURSE", "PROPERTY_USER"
        ];
        $sort = [ "SORT" => "ASC" ];
        $registers = \CIBlockElement::GetList(
        	$sort, $filter, false, [ "nTopCount" => 1 ], $fields
        );
        while ( $register = $registers->GetNext() ){

        	$moder_enum = \AOptima\Tools\prop_enum::getByID( $register['PROPERTY_MODERATION_ENUM_ID'] );

        	if(
                (
                    $moder_enum['XML_ID'] == 'accepted'
                    ||
                    $moder_enum['XML_ID'] == 'rejected'
                )
                &&
                !$register['PROPERTY_MODERATION_MAIL_SENT_VALUE']
            ){

                $user = \AOptima\Tools\user::info( $register['PROPERTY_USER_VALUE'] );

                // Письму участнику
                $email_to = $user['EMAIL'];
                if( $moder_enum['XML_ID'] == 'accepted' ){
                    $mail_title = 'Регистрация на курс в Академии КНАУФ одобрена!';
                } else if( $moder_enum['XML_ID'] == 'rejected' ){
                    $mail_title = 'Регистрация на курс в Академии КНАУФ отклонена!';
                }

                ob_start();
                    global $APPLICATION;
                    $APPLICATION->IncludeComponent(
                        "aoptima:mail_courseRegisterStatus", "client",
                        [
                            "course_id" => $register['PROPERTY_COURSE_VALUE'],
                            "reg_item_id" => $register['ID'],
                        ]
                    );
                    $mail_html = ob_get_contents();
                ob_end_clean();

                \AOptima\Tools\feedback::sendMainEvent( $mail_title, $mail_html, $email_to );
                /// /// ///

                // Проставим галочку
                $set_prop = ["MODERATION_MAIL_SENT" => 1];
                \CIBlockElement::SetPropertyValuesEx($register['ID'], $arFields['IBLOCK_ID'], $set_prop);
            }
        }
    }



    static function checkCertificateEvent( $arFields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');

        $result = [ 'status' => 'ok' ];

        $sendCertProp = \AOptima\Tools\prop::getByCode( 'SEND_CERT', $arFields['IBLOCK_ID'] );
        $sendCertPropValue = $arFields['PROPERTY_VALUES'][ $sendCertProp['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $sendCertProp['ID'] ])[0] ]['VALUE'];

        // Если поставлена галочка отправки сертификата
        if( $sendCertPropValue ){

            // Проверка модерации записи
            $moderProp = \AOptima\Tools\prop::getByCode( 'MODERATION', $arFields['IBLOCK_ID'] );
            $moderPropValue = $arFields['PROPERTY_VALUES'][ $moderProp['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $moderProp['ID'] ])[0] ]['VALUE'];
            if( intval( $moderPropValue ) > 0 ){
                $moder_enum = \AOptima\Tools\prop_enum::getByID( $moderPropValue );
            } else {
                $moder_enum = null;
            }

            if( !isset( $moder_enum ) || $moder_enum['XML_ID'] != 'accepted' ){
                $result = [
                    'status' => 'error',
                    'text' => "Регистрация ещё не даже одобрена"
                ];
                return $result;
            }

            // Проверка на завершенность курса
            $propCourse = \AOptima\Tools\prop::getByCode( 'COURSE', $arFields['IBLOCK_ID'] );
            $propCourseID = $arFields['PROPERTY_VALUES'][ $propCourse['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $propCourse['ID'] ])[0] ]['VALUE'];
            $course = \AOptima\Tools\el::info($propCourseID);
            $date1 = date_create(ConvertDateTime(date('d.m.Y H:i:s'), "YYYY-MM-DD HH:MI:SS", "ru"));
            $date2 = date_create(ConvertDateTime($course['PROPERTY_DATE_TO_VALUE'], "YYYY-MM-DD HH:MI:SS", "ru"));
            $courseFinished = $date1->diff($date2)->invert;

            if( !$courseFinished ){
                $result = [
                    'status' => 'error',
                    'text' => "Курс ещё не завершился"
                ];
                return $result;
            }

            // Проверка установки галочки о прохождении курса
            $propStatus1 = \AOptima\Tools\prop::getByCode( 'STATUS_1', $arFields['IBLOCK_ID'] );
            $propStatus1Value = $arFields['PROPERTY_VALUES'][ $propStatus1['ID'] ][ array_keys($arFields['PROPERTY_VALUES'][ $propStatus1['ID'] ])[0] ]['VALUE'];

            if( !$propStatus1Value ){
                $result = [
                    'status' => 'error',
                    'text' => "Не проставлена галочка о прохождении курса"
                ];
                return $result;
            }
        }

        return $result;
    }

    static function certificateEvent( $arFields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');

        $filter = [
            "IBLOCK_ID" => $arFields['IBLOCK_ID'],
            "ID" => $arFields['ID']
        ];
        $fields = [
            "ID", "PROPERTY_STATUS_1", "PROPERTY_STATUS_2", "PROPERTY_MODERATION",
            "PROPERTY_EMAIL", "PROPERTY_SEND_CERT", "PROPERTY_COURSE", "PROPERTY_USER"
        ];
        $sort = [ "SORT" => "ASC" ];
        $registers = \CIBlockElement::GetList(
            $sort, $filter, false, [ "nTopCount" => 1 ], $fields
        );
        if ( $register = $registers->GetNext() ){

            $user = \AOptima\Tools\user::info( $register['PROPERTY_USER_VALUE'] );

            $set_prop = [ "SEND_CERT" => 0 ];

            // и если нажата кнопка отправки
            if( $register['PROPERTY_SEND_CERT_VALUE'] ){

                // Создание сертификата
                project\certificate::create( $register['ID'] );

                // Получим путь к файлу сертификата
                $pdfFilePath = project\certificate::filePath( $register['ID'] );

                if( file_exists( $pdfFilePath ) ){

                    // Отправляем письмо с прикреплением файла PDF
                    ob_start();
                        global $APPLICATION;
                        $APPLICATION->IncludeComponent(
                            "aoptima:mail_sendCertificate", "",
                            [ "register_id" => $register['ID'] ]
                        );
                        $mail_html = ob_get_contents();
                    ob_end_clean();

                    $to          = $user['EMAIL'];
                    $from        = \AOptima\Tools\feedback::getEmailFrom();
                    $subject     = 'Ваш сертификат по курсу в Академии Кнауф';
                    $body        = $mail_html;
                    $pdfLocation = $pdfFilePath;
                    $pdfName     = "certificate.pdf";
                    $filetype    = "application/pdf";

                    // create headers and mime boundry
                    $eol = PHP_EOL;
                    $semi_rand = md5(time());
                    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
                    $headers = "From: $from$eol" .
                        "MIME-Version: 1.0$eol" .
                        "Content-Type: multipart/mixed;$eol" .
                        " boundary=\"$mime_boundary\"";

                    // add html message body
                    $message = "--$mime_boundary$eol" .
                        "Content-Type: text/html; charset=\"utf-8\"$eol" .
                        "Content-Transfer-Encoding: 7bit$eol$eol" .
                        $body . $eol;

                    // fetch pdf
                    $file = fopen($pdfLocation, 'rb');
                    $data = fread($file, filesize($pdfLocation));
                    fclose($file);
                    $pdf = chunk_split(base64_encode($data));

                    // attach pdf to email
                    $message .= "--$mime_boundary$eol" .
                        "Content-Type: $filetype; charset=\"utf-8\"$eol" .
                        " name=\"$pdfName\"$eol" .
                        "Content-Disposition: attachment;$eol" .
                        " filename=\"$pdfName\"$eol" .
                        "Content-Transfer-Encoding: base64$eol$eol" .
                        $pdf . $eol .
                        "--$mime_boundary--";

                    mail( $to, $subject, $message, $headers );
                }

                /// /// ///
            }

            if( count( $set_prop ) > 0 ){
                \CIBlockElement::SetPropertyValuesEx($register['ID'], $arFields['IBLOCK_ID'], $set_prop);
            }
        }
    }



    // Доступность регистрации на курс
    static function registerAvailable( $courseID ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        global $USER;
        if( !$USER->IsAuthorized() ){
            return [
                'result' => false,
                'result_code' => 'auth_error',
                'description' => 'для регистрации на курс необходимо авторизоваться',
            ];
        }

        $course = \AOptima\Tools\el::info( $courseID );

        if( intval( $course['ID'] ) > 0 ){

            $dateTo = ConvertDateTime( $course['PROPERTY_DATE_TO_VALUE'], "DD.MM.YYYY HH:MI:SS", "ru" );
            $curDate = date('d.m.Y H:i:s');
            $datetime1 = date_create(ConvertDateTime($curDate, "YYYY-MM-DD HH:MI:SS", "ru"));
            $datetime2 = date_create(ConvertDateTime($dateTo, "YYYY-MM-DD HH:MI:SS", "ru"));
            $invert = $datetime1->diff($datetime2)->invert;

            if( $invert ){
                return [
                    'result' => false,
                    'result_code' => 'already_finished',
                    'description' => 'курс уже завершился',
                ];
            }

            $dateFrom = ConvertDateTime( $course['PROPERTY_DATE_FROM_VALUE'], "DD.MM.YYYY HH:MI:SS", "ru" );
            $curDate = date('d.m.Y H:i:s');
            $datetime1 = date_create(ConvertDateTime($curDate, "YYYY-MM-DD HH:MI:SS", "ru"));
            $datetime2 = date_create(ConvertDateTime($dateFrom, "YYYY-MM-DD HH:MI:SS", "ru"));
            $invert = $datetime1->diff($datetime2)->invert;

            if( $invert ){
                return [
                    'result' => false,
                    'result_code' => 'already_started',
                    'description' => 'курс уже начался',
                ];
            }

            $regDateFrom = ConvertDateTime( $course['PROPERTY_REG_DATE_FROM_VALUE'], "DD.MM.YYYY HH:MI:SS", "ru" );
            $datetime1 = date_create(ConvertDateTime($regDateFrom, "YYYY-MM-DD HH:MI:SS", "ru"));
            $datetime2 = date_create(ConvertDateTime($curDate, "YYYY-MM-DD HH:MI:SS", "ru"));
            $regFromInvert = $datetime1->diff($datetime2)->invert;

            $regDateTo = ConvertDateTime( $course['PROPERTY_REG_DATE_TO_VALUE'], "DD.MM.YYYY HH:MI:SS", "ru" );
            $datetime1 = date_create(ConvertDateTime($curDate, "YYYY-MM-DD HH:MI:SS", "ru"));
            $datetime2 = date_create(ConvertDateTime($regDateTo, "YYYY-MM-DD HH:MI:SS", "ru"));
            $regToInvert = $datetime1->diff($datetime2)->invert;

            if( $regFromInvert ){
                return [
                    'result' => false,
                    'result_code' => 'reg_not_started_yet',
                    'description' => 'регистрация ещё не стартовала',
                ];
            }

            if( $regToInvert ){
                return [
                    'result' => false,
                    'result_code' => 'reg_already_finished',
                    'description' => 'регистрация уже завершилась',
                ];
            }

            $alreadyRegistered = static::alreadyRegistered( $course['ID'], $USER->GetID() );
            if( isset( $alreadyRegistered ) && $alreadyRegistered ){
                return [
                    'result' => false,
                    'result_code' => 'already_registered',
                    'description' => 'вы уже зарегистрированы на этот курс',
                ];
            }

            $freePlacesCnt = static::freePlacesCnt( $course['ID'] );
            if( $freePlacesCnt == 0 ){
                return [
                    'result' => false,
                    'result_code' => 'no_places',
                    'description' => 'в данный момент нет свободных мест',
                ];
            }

            return [
                'result' => true
            ];
        }
        return [
            'result' => false,
            'result_code' => 'data_error',
            'description' => 'ошибка получения данных по курсу',
        ];
    }


    static function allRegisteredItems( $courseID ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $allItems = [];
        $filter = [
            "IBLOCK_ID" => static::REG_IBLOCK_ID,
            "PROPERTY_COURSE" => $courseID,
        ];
        $fields = [ "ID", "PROPERTY_COURSE", 'PROPERTY_MODERATION', 'PROPERTY_USER' ];
        $sort = [ "SORT" => "ASC" ];
        $dbElements = \CIBlockElement::GetList(
            $sort, $filter, false, false, $fields
        );
        while ( $element = $dbElements->GetNext() ){
            $allItems[] = $element;
        }
        return $allItems;
    }


    // Проверка наличия регистрации пользователя на курс
    static function alreadyRegistered( $courseID, $userID ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $course = \AOptima\Tools\el::info( $courseID );
        $user = \AOptima\Tools\user::info( $userID );
        if( intval( $course['ID'] ) > 0 && intval( $user['ID'] ) > 0 ){
            // Получим все записи
            $allItems = static::allRegisteredItems( $course['ID'] );
            foreach ( $allItems as $item ){
                if( $item['PROPERTY_USER_VALUE'] == $user['ID'] ){
                    return true;
                }
            }
            return false;
        }
        return null;
    }

    
    // Количество свободных мест на курс
    static function freePlacesCnt( $courseID ){
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $course = \AOptima\Tools\el::info( $courseID );
        if( intval( $course['ID'] ) > 0 ){
            // Получим все записи
            $allItems = static::allRegisteredItems( $course['ID'] );
            // Количество необработанных записей
            $noProcessedCnt = 0;
            // Количество одобренных записей
            $acceptedCnt = 0;
            // Количество отклонённых записей
            $rejectedCnt = 0;
            foreach ( $allItems as $key => $item ){
                if( intval( $item['PROPERTY_MODERATION_ENUM_ID'] ) > 0 ){
                    $moderEnum = \AOptima\Tools\prop_enum::getByID( $item['PROPERTY_MODERATION_ENUM_ID'] );
                    if( $moderEnum['XML_ID'] == 'accepted' ){
                        $acceptedCnt++;
                    } else if( $moderEnum['XML_ID'] == 'rejected' ){
                        $rejectedCnt++;
                    }
                } else {
                    $noProcessedCnt++;
                }
            }
            $occupiedPlacesCnt = $acceptedCnt + $noProcessedCnt;
            /// /// ///
            $freePlaces = intval( $course['PROPERTY_MAX_REG_CNT_VALUE'] ) - $occupiedPlacesCnt;
            if( $freePlaces < 0 ){    $freePlaces = (int) 0;    }
            /// /// ///
            return $freePlaces;
        }
        return (int) 0;
    }



    static function search( $q ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $q = strip_tags( trim( $q ) );
        $ids = [];
        /////
        $filter = [
        	"IBLOCK_ID" => static::IBLOCK_ID,
        	"ACTIVE" => "Y",
        	"NAME" => $q
        ];
        $fields = [ "ID", "NAME" ];
        $sort = [ "SORT" => "ASC" ];
        $hash = md5(json_encode($filter).json_encode($fields).json_encode($sort));
        $dbElements = \CIBlockElement::GetList(
        	$sort, $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $ids[] = $element['ID'];
        }
        /////
        $filter = [
            "IBLOCK_ID" => static::IBLOCK_ID,
            "ACTIVE" => "Y",
            "NAME" => $q."%",
            "!ID" => $ids
        ];
        $fields = [ "ID", "NAME" ];
        $sort = [ "SORT" => "ASC" ];
        $hash = md5(json_encode($filter).json_encode($fields).json_encode($sort));
        $dbElements = \CIBlockElement::GetList(
            $sort, $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $ids[] = $element['ID'];
        }
        /////
        $filter = [
            "IBLOCK_ID" => static::IBLOCK_ID,
            "ACTIVE" => "Y",
            "NAME" => "%".$q."%",
            "!ID" => $ids
        ];
        $fields = [ "ID", "NAME" ];
        $sort = [ "SORT" => "ASC" ];
        $hash = md5(json_encode($filter).json_encode($fields).json_encode($sort));
        $dbElements = \CIBlockElement::GetList(
            $sort, $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $ids[] = $element['ID'];
        }
        return $ids;
    }



    // Фильтр для страницы курсов
    static function pageFilter( $fields ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $filter = null;

        if( strlen($fields['q']) > 0 ){
            $search_items = static::search( $fields['q'] );
            if( count( $search_items ) > 0 ){
                $filter['ID'] = $search_items;
            } else {
                $filter['ID'] = 0;
            }
        }

        if( intval($fields['program']) > 0 ){
            $filter['PROPERTY_PROGRAM'] = intval($fields['program']);
        }

        if( intval($fields['learn_type']) > 0 ){
            $filter['PROPERTY_TYPE'] = intval($fields['learn_type']);
        }

        if( strlen(strip_tags(trim($fields['min_date']))) > 0 ){
            $min_date = ConvertDateTime(strip_tags(trim($fields['min_date'])).' 00:00:00', "YYYY-MM-DD HH:MI:SS", "ru");
            $filter['>=PROPERTY_DATE_FROM'] = $min_date;
        }

        if( strlen(strip_tags(trim($fields['max_date']))) > 0 ){
            $max_date = ConvertDateTime(strip_tags(trim($fields['max_date'])).' 23:59:59', "YYYY-MM-DD HH:MI:SS", "ru");
            $filter['<=PROPERTY_DATE_FROM'] = $max_date;
        }

        $need_centers_filter = true;
        if(
            intval($fields['learn_type']) > 0
            &&
            trim(mb_strtolower( \AOptima\Tools\el::info(intval($fields['learn_type']))['NAME'] )) == 'вебинар'
        ){    $need_centers_filter = false;    }

        if( $need_centers_filter ){

            $country_centers_ids = null;
            if(
                strlen( strip_tags(trim($fields['country'])) ) > 0
                &&
                strip_tags(trim($fields['country'])) != 'empty'
            ){
                $country_centers_ids = [];
                $country = \AOptima\Tools\el::info_by_code( strip_tags(trim($fields['country'])), project\country::IBLOCK_ID );
                
                if( intval( $country['ID'] ) > 0 ){
                    $country_centers_ids = project\center::all_items_ids( $country['ID'] );
                }
            }

            $city_centers_ids = null;
            if(
                strlen( strip_tags(trim($fields['city'])) ) > 0
                &&
                strip_tags(trim($fields['city'])) != 'empty'
            ){
                $city_centers_ids = project\center::all_items_ids(
                    null,
                    strip_tags(trim($fields['city']))
                );
            }

            if( isset( $country_centers_ids ) && isset( $city_centers_ids ) ){
                $centers_ids = array_intersect( $country_centers_ids, $city_centers_ids );
                if( count($centers_ids) > 0 ){
                    $filter['PROPERTY_CENTER'] = $centers_ids;
                } else {
                    $filter['ID'] = 0;
                }
            } else if( isset( $country_centers_ids ) ){
                if( count($country_centers_ids) > 0 ){
                    $filter['PROPERTY_CENTER'] = $country_centers_ids;
                } else {
                    $filter['ID'] = 0;
                }
            } else if( isset( $city_centers_ids ) ){
                if( count($city_centers_ids) > 0 ){
                    $filter['PROPERTY_CENTER'] = $city_centers_ids;
                } else {
                    $filter['ID'] = 0;
                }
            }

        }

        return $filter;
    }










}