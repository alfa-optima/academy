<?php

namespace AOptima\Project;
use AOptima\Project as project;



class user {


    const REGISTERED_GROUP_ID_PARAM = 'REGISTERED_GROUP_ID';
    const PASSWORD_MIN_LENGTH = 6;





    // Регистрация пользователя
    static function register( $fields ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $logPrefix = 'Регистрация пользователя - ';

        $email = strip_tags(trim($fields['email']));
        $f = strip_tags(trim($fields['f']));
        $i = strip_tags(trim($fields['i']));
        $o = strip_tags(trim($fields['o']));
        $password = strip_tags(trim($fields['password']));
        $confirm_password = strip_tags(trim($fields['confirm_password']));

        global $USER;

        // Проверки
        if( !( strlen($email) > 0 ) ){
            return [
                "status" => "error",
                "text" => "Введите, пожалуйста, Ваш Email!"
            ];
        }
        if( !preg_match("/^.+@.+\..+$/", $email, $matches, PREG_OFFSET_CAPTURE) ){
            return [
                "status" => "error",
                "text" => "Email содержит ошибки!"
            ];
        }
        if( !( strlen($f) > 0 ) ){
            return [
                "status" => "error",
                "text" => "Введите, пожалуйста, Вашу фамилию!"
            ];
        }
        if( !( strlen($i) > 0 ) ){
            return [
                "status" => "error",
                "text" => "Введите, пожалуйста, Ваше имя!"
            ];
        }
        if( !( strlen($o) > 0 ) ){
            return [
                "status" => "error",
                "text" => "Введите, пожалуйста, Ваше отчество!"
            ];
        }
        if( !( strlen($password) > 0 ) ){
            return [
                "status" => "error",
                "text" => "Введите, пожалуйста, Ваш пароль!"
            ];
        }
        if( strlen($password) < project\user::PASSWORD_MIN_LENGTH ){
            return [
                "status" => "error",
                "text" => 'Длина пароля - не менее '.project\user::PASSWORD_MIN_LENGTH
            ];
        }
        if( !( strlen($confirm_password) > 0 ) ){
            return [
                "status" => "error",
                "text" => "Введите, пожалуйста, пароль повторно!"
            ];
        }
        if( $password != $confirm_password ){
            return [
                "status" => "error",
                "text" => "Пароли не совпадают!"
            ];
        }

        $userID = null;

        // проверяем Email на логин
        $existUser = \AOptima\Tools\user::getByLogin( $email );
        if( !( intval( $existUser['ID'] ) > 0 ) ){
            // проверяем EMAIL на наличие у других пользователей
            $existUser = \AOptima\Tools\user::getByEmail( $email );
        }
        
        // Если пользователь найден
        if( intval( $existUser['ID'] ) > 0 ){
            // возвращаем ошибку
            return [
                "status" => "error",
                "text" => "Данный&nbsp;Email уже&nbsp;зарегистрирован на&nbsp;сайте!"
            ];
        }

        $confirm_code = md5('43ghswege45wh'.time().randString(5, [ "0123456789" ]));

        // Регистрируем пользователя
        $obUser = new \CUser;
        $arUserFields = [
            "ACTIVE" => "N",
            "LOGIN" => $email,
            "EMAIL" => $email,
            "LAST_NAME" => $f,
            "NAME" => $i,
            "SECOND_NAME" => $o,
            "EMAIL" => $email,
            "PASSWORD" => $password,
            "CONFIRM_PASSWORD" => $password,
            "UF_REGISTER_CONFIRM_CODE" => $confirm_code,
            "GROUP_ID" => [
                \AOptima\Tools\config::getValue( static::REGISTERED_GROUP_ID_PARAM )
            ]
        ];

        $userID = $obUser->Add($arUserFields);
        if( !( intval( $userID ) > 0 ) ){
            $error = $logPrefix.$obUser->LAST_ERROR;
            \AOptima\Tools\logger::addError( $error );
        }

        if ( intval( $userID ) > 0 ){

            // Регистрационная информация на Email
            // (со ссылкой для подтверждения регистрации)
            \AOptima\Tools\user::sendRegInfo( $userID, null, $confirm_code );

            return [ "status" => "ok" ];

        } else {

            return [
                "status" => "error",
                "text" => "Ошибка регистрации"
            ];
        }
    }






}