<?php

namespace AOptima\Project;
use AOptima\Project as project;



class personal {


    static function pages(){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $pages = [
            [
                'url' => '/personal/',
                'name' => 'Текущие курсы',
            ],
            [
                'url' => '/personal/completed_courses/',
                'name' => 'Пройденные курсы',
            ],
//            [
//                'url' => '/personal/certificates/',
//                'name' => 'Сертификаты',
//            ],
            [
                'url' => '/personal/edit_data/',
                'name' => 'Профиль',
            ]
        ];

        foreach ( $pages as $key => $page ){
            if( $page['url'] == \AOptima\Tools\funcs::pureURL() ){
                $pages[ $key ]['active'] = 'Y';
            }
        }

        return $pages;
    }



    static $activities = [
        'Строители',
        'Дилеры и субдилеры',
        'Студенты',
        'Дизайнеры',
        'Конечные потребители',
        'Сотрудники КНАУФ',
        'Архитекторы и проектировщики',
        'Преподаватели',
        'Мастера и бригадиры',
        'Другие категории'
    ];


    static $didYouKnow = [
        'Реклама',
        'СМИ',
        'Сотрудники КНАУФ',
        'Сайт knauf.ru',
        'Рассылка от k/profi',
        'Совет профессионала',
        'Соц. сети КНАУФ',
        'Дилеры/субдилеры',
        'Другое'
    ];





}