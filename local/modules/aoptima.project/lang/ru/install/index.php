<?

$MESS["AOPTIMA_PROJECT_MODULE_NAME"] = "Aoptima project";
$MESS["AOPTIMA_PROJECT_MODULE_DESC"] = "Aoptima project";
$MESS["AOPTIMA_PROJECT_PARTNER_NAME"] = "Aoptima";
$MESS["AOPTIMA_PROJECT_PARTNER_URI"] = "http://alfa-optima.ru/";

$MESS["AOPTIMA_PROJECT_DENIED"] = "Доступ закрыт";
$MESS["AOPTIMA_PROJECT_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["AOPTIMA_PROJECT_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["AOPTIMA_PROJECT_FULL"] = "Полный доступ";

$MESS["AOPTIMA_PROJECT_INSTALL_TITLE"] = "Установка модуля";
$MESS["AOPTIMA_PROJECT_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";

#работа с .settings.php
$MESS["AOPTIMA_PROJECT_INSTALL_COUNT"] = "Количество установок модуля: ";
$MESS["AOPTIMA_PROJECT_UNINSTALL_COUNT"] = "Количество удалений модуля: ";

$MESS["AOPTIMA_PROJECT_NO_CACHE"] = 'Внимание, на сайте выключено кеширование!<br>Возможно замедление в работе модуля.';
#работа с .settings.php
?>