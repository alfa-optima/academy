<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class prop_enum {



    static function list( $iblock_id, $prop_code ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $filter = [ 'IBLOCK_ID' => $iblock_id, 'CODE' => $prop_code ];
        $sort = [ 'SORT' => 'ASC' ];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'prop_enum_list_'.$iblock_id.'_'.$prop_code;
        $cache_path = '/prop_enum_list/'.$iblock_id.'/'.$prop_code.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif( $obCache->StartDataCache() ){
            $enums = \CIBlockPropertyEnum::GetList( $sort, $filter );
            while($enum = $enums->GetNext()){    $list[] = $enum;    }
        $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }



    static function getByID( $enum_id = null ){
		\Bitrix\Main\Loader::includeModule('iblock');
        $arEnum = null;
        if( isset( $enum_id ) && intval( $enum_id ) > 0 ){
            $filter = [ 'ID' => $enum_id ];
            $enums = \CIBlockPropertyEnum::GetList( ["SORT" => "ASC"], $filter );
            if( $enum = $enums->GetNext() ){    $arEnum = $enum;    }
        }
        return $arEnum;
    }



    static function getByXmlID( $enum_xml_id = null ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $arEnum = null;
        if( isset( $enum_xml_id ) && strlen( $enum_xml_id ) > 0 ){
            $filter = [ 'XML_ID' => $enum_xml_id ];
            $enums = \CIBlockPropertyEnum::GetList( ["SORT" => "ASC"], $filter );
            if( $enum = $enums->GetNext() ){    $arEnum = $enum;    }
        }
        return $arEnum;
    }



}