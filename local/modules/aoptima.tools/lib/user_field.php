<? namespace AOptima\Tools;
use AOptima\Tools as tools;


class user_field {



    // Получение списка пользовательских полей
    // Пример, \AOptima\Tools\user_field::getList( 'IBLOCK_3_SECTION', 'UF_GENDER_TYPE' );
    static function getList( $entity_id = false, $field_name = false ){
        $list = [];
        $filter = [];
        if( isset($entity_id) ){
            $filter['ENTITY_ID'] = $entity_id;
        }
        if( isset($field_name) ){
            $filter['FIELD_NAME'] = $field_name;
        }
        $UFs = \CUserTypeEntity::GetList( [$by=>$order], $filter );
        while($UF = $UFs->Fetch()){
            $list[] = $UF;
        }
        return $list;
    }



    // Получение пользовательского поля по имени поля
    // Пример, \AOptima\Tools\user_field::getByFieldName( 'IBLOCK_3_SECTION', 'UF_GENDER_TYPE' );
    static function getByFieldName( $entity_id, $field_name ){
        $filter = ['ENTITY_ID' => $entity_id, 'FIELD_NAME' => $field_name];
        $UFs = \CUserTypeEntity::GetList( [$by=>$order], $filter );
        while($UF = $UFs->Fetch()){
            return $UF;
        }
        return false;
    }



    // Получение вариантов значений типа Список для польз. полей
    // Пример, \AOptima\Tools\user_field::getEnumList( 52 );
    static function getEnumList( $user_field_id = false, $enum_xml_id = false ){
        $list = [];
        $filter = [];
        if( isset($user_field_id) ){
            $filter['USER_FIELD_ID'] = $user_field_id;
        }
        if( isset($enum_xml_id) ){
            $filter['XML_ID'] = $enum_xml_id;
        }
        $enums = \CUserFieldEnum::GetList([], $filter);
        while($enum = $enums->GetNext()){
            $list[] = $enum;
        }
        return $list;
    }



    // Получение ID вариантов значений типа Список для польз. полей
    // Пример, \AOptima\Tools\user_field::getEnumList( 52 );
    static function getEnumIDSList( $user_field_id = false, $enum_xml_id = false ){
        $list = [];
        $filter = [];
        if( isset($user_field_id) ){
            $filter['USER_FIELD_ID'] = $user_field_id;
        }
        if( isset($enum_xml_id) ){
            $filter['XML_ID'] = $enum_xml_id;
        }
        $enums = \CUserFieldEnum::GetList([], $filter);
        while($enum = $enums->GetNext()){
            $list[] = $enum['ID'];
        }
        return $list;
    }



    // Получение варианта значения для польз. поля типа Список - по XML_ID
    // Пример, \AOptima\Tools\user_field::getEnumByXMLID( 52, 'woman' );
    static function getEnumByXMLID( $user_field_id, $enum_xml_id ){
        $filter = [];
        if( isset($user_field_id) ){
            $filter['USER_FIELD_ID'] = $user_field_id;
        }
        if( isset($enum_xml_id) ){
            $filter['XML_ID'] = $enum_xml_id;
        }
        $enums = \CUserFieldEnum::GetList([], $filter);
        while($enum = $enums->GetNext()){
            return $enum;
        }
        return false;
    }
	
	
	
	// Получение варианта значения для польз. поля типа Список - по ID
    // Пример, \AOptima\Tools\user_field::getEnumByID( 52, 5 );
    static function getEnumByID( $user_field_id, $id ){
        $filter = [];
        if( isset($user_field_id) ){
            $filter['USER_FIELD_ID'] = $user_field_id;
        }
        if( isset($id) ){
            $filter['ID'] = $id;
        }
        $enums = \CUserFieldEnum::GetList([], $filter);
        while($enum = $enums->GetNext()){
            return $enum;
        }
        return false;
    }



}
