<?php

namespace AOptima\Tools;
use AOptima\Tools as tools;


\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Main;
use Bitrix\Sale;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Internals;
use \Bitrix\Main\Type\DateTime;



class coupon {


    // Проверка на существование купона в принципе
    static function exists($coupon){
        DiscountCouponsManager::init();
        $res = DiscountCouponsManager::getData($coupon);
        return ( intval($res['ID']) > 0 );
    }


	// Проверка на существование активного купона
	static function checkActivity($coupon){
		DiscountCouponsManager::init();
		$res = DiscountCouponsManager::getData($coupon);
		return (intval($res['ID']) > 0 && $res['ACTIVE'] == 'Y');
	}


    // Получение DISCOUNT_ID
    static function getProgramID($coupon){
        DiscountCouponsManager::init();
        $arCoupon = DiscountCouponsManager::getData( $coupon );
        if( intval( $arCoupon['ID'] ) > 0 ){
            return $arCoupon['DISCOUNT_ID'];
        }
        return null;
    }

	
	// Установка купона
	static function set($coupon){
		DiscountCouponsManager::init();
		DiscountCouponsManager::add($coupon);
	}
	
	
	// Удаления купона из куки
	static function clear(){
		DiscountCouponsManager::init();
		$arCoupons = DiscountCouponsManager::get(true, array(), true, true);
		foreach($arCoupons as $coupon => $ar){
			DiscountCouponsManager::delete($coupon);
		}
	}
	
	
	// Запрос активного купона
	static function get_active_coupon(){
        DiscountCouponsManager::init();
        $arCoupons = DiscountCouponsManager::get(true, array(), true, true);
	    foreach ( $arCoupons as $coupon => $arCoupon ){
            return $arCoupon;
	    }
	    return false;
	}
	
	
	// Получение ID купона
	static function get_coupon_id($coupon){
        // Проверим наличие такого купона
        $couponIterator = Internals\DiscountCouponTable::getList([
            'select' => [ 'ID', 'COUPON', ],
            'filter' => [ 'COUPON' => $coupon ],
        ]);
        // Если такой купон есть
        if ( $arCoupon = $couponIterator->fetch() ){
            return $arCoupon['ID'];
        }
		return false;
	}


    // Получение инфы по купону
    static function get_coupon($coupon){
        // Проверим наличие такого купона
        $couponIterator = Internals\DiscountCouponTable::getList([
            'select' => [ '*' ],
            'filter' => [ 'COUPON' => $coupon ],
        ]);
        // Если такой купон есть
        if ( $arCoupon = $couponIterator->fetch() ){
            return $arCoupon;
        }
        return false;
    }
	
	
	// Применение/деактивация купона в БД
	static function apply($coupon){
        $logPrefix = 'Применение/деактивация купона - ';
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $arCoupon = static::get_coupon($coupon);
        if( intval($arCoupon['ID']) > 0 ){
            $res = Internals\DiscountCouponTable::update(
                $arCoupon['ID'],
                [
                    // деактивируем
                    'ACTIVE' => 'N',
                    // отмечаем дату применения купона
                    'DATE_APPLY' => new DateTime(date('d.m.Y H:i:s'))
                ]
            );
            $errors = $res->getErrors();
            if( count($errors) > 0 ){
                foreach ( $errors as $error ){
                    \AOptima\Tools\logger::addError($logPrefix.$error->getMessage());
                }
            }
        }
	}


    // Деактивация купона в БД
    static function deactivateByID($coupon_id){
        Internals\DiscountCouponTable::update( $coupon_id, ['ACTIVE' => 'N'] );
    }
	
	
	// Удаление купона в БД
	static function delete($coupon){
		\CCatalogDiscountCoupon::Delete(static::get_coupon_id($coupon));  
	}
	
	
}