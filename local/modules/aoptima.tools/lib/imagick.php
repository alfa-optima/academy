<?php

namespace AOptima\Tools;
use AOptima\Tools as tools;
use Bitrix\Main\DB\Exception;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*   Для работы необходима php-библиотека Imagick
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


class imagick {


    const RESIZE_PATH = '/upload/aoptima_imagick_resizes';
    const BLUR_PATH = '/upload/aoptima_imagick_blurs';


    static $fileTypes = [
        'image/jpeg',
        'image/png',
        'image/webp',
    ];




    // Ресайз
    static function resize(
        $file_path,
        $width,
        $height,
        $bestFit = 1, // 1 - "наилучшее встраивание в размеры" с сохранением пропорций
        $blur = 1 // коэффициент размытия, где >1 - размытость, <1 - резкость.
    ){
        $logPrefix = 'imagick resize - ';
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path;
        // Если файл существует
        if( file_exists( $file_path ) ){
            // Определение пути к новому файлу ресайза
            $new_file_path = static::RESIZE_PATH.'/toBestFit_'.($bestFit?'Y':'N').'/';
            $new_file_path .= 'w_'.$width.'_h_'.$height.'/';
            $new_file_path .= 'blur_'.$blur.'/';
            $new_file_path .= md5($file_path . filesize($file_path)).'/';
            if( !file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                mkdir( $_SERVER['DOCUMENT_ROOT'].$new_file_path, 0700, true );
            }
            $new_file_path .= basename($file_path);
            // Если файл ресайза уже существует
            if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                // Сразу возвращаем
                return $new_file_path;
            // Если файла ресайза ещё нет
            } else {
                // Получим тип файла
                $file_type = static::getFileType( $file_path );
                if(
                    // Если тип файла определён
                    isset( $file_type )
                    &&
                    // Если это допустимый тип файла
                    in_array( $file_type, static::$fileTypes )
                ){
                    /// /// ///

                    try {

                        $magic = new \Imagick();
                        $magic->readimage( $file_path );

                        $magic->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, $blur, $bestFit);

                        $magic->writeimage( $_SERVER['DOCUMENT_ROOT'].$new_file_path );

                        /// /// ///
                        if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                            return $new_file_path;
                        } else {
                            tools\logger::addError($logPrefix.'не удалось создать файл');
                        }
                    } catch ( \ImagickException $ex ){
                        tools\logger::addError($logPrefix.$ex->getMessage());
                    } catch ( \Exception $ex ){
                        tools\logger::addError($logPrefix.$ex->getMessage());
                    }
                } else {
                    tools\logger::addError($logPrefix.'тип файла ('.$file_type.') не входит в список допустимых');
                }
            }
        } else {
            tools\logger::addError($logPrefix.'передан путь к несуществующему файлу');
        }
        return false;
    }




    static function blur( $file_path, $radius, $sigma ){
        $logPrefix = 'imagick blur - ';
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path;
        // Если файл существует
        if( file_exists( $file_path ) ){
            // Определение пути к новому файлу
            $new_file_path = static::BLUR_PATH.'/';
            $new_file_path .= 'r_'.$radius.'_s_'.$sigma.'/';
            $new_file_path .= md5($file_path . filesize($file_path)).'/';
            if( !file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                mkdir( $_SERVER['DOCUMENT_ROOT'].$new_file_path, 0700, true );
            }
            $new_file_path .= basename($file_path);
            // Если файл уже существует
            if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                // Сразу возвращаем
                return $new_file_path;
            // Если файла ещё нет
            } else {
                // Получим тип файла
                $file_type = static::getFileType( $file_path );
                if(
                    // Если тип файла определён
                    isset( $file_type )
                    &&
                    // Если это допустимый тип файла
                    in_array( $file_type, static::$fileTypes )
                ){
                    try {

                        /// /// ///
                        $magic = new \Imagick();
                        $magic->readimage( $file_path );
                        $magic->blurImage($radius, $sigma);
                        $magic->writeimage( $_SERVER['DOCUMENT_ROOT'].$new_file_path );
                        /// /// ///

                        if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                            return $new_file_path;
                        } else {
                            tools\logger::addError($logPrefix.'не удалось создать файл');
                        }

                    } catch ( \ImagickException $ex ){
                        tools\logger::addError( $logPrefix.$ex->getMessage() );
                    }
                } else {
                    tools\logger::addError($logPrefix.'тип файла ('.$file_type.') не входит в список допустимых');
                }
            }
        } else {
            tools\logger::addError($logPrefix.'передан путь к несуществующему файлу');
        }
        return false;
    }



    // Получение типа файла
    static function getFileType( $file_path ){
        if( file_exists( $file_path ) ){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $file_type = finfo_file( $finfo, $file_path );
            finfo_close($finfo);
            return $file_type;
        }
        return null;
    }



}