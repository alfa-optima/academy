<?php

namespace AOptima\Tools;
use AOptima\Tools as tools;

use Bitrix\Main\Config\Configuration;



class config {

    const CONFIG_SECT_NAME = 'aoptima_settings';

    static function getAllValues(){
        $config = Configuration::getInstance();
        $params = $config->get( static::CONFIG_SECT_NAME );
        return $params;
    }

    static function getValue( $param_name ){
        $config = Configuration::getInstance();
        $params = static::getAllValues( static::CONFIG_SECT_NAME );
        return $params[ $param_name ];
    }


    static function setValue( $param_name, $value ){
        $config = Configuration::getInstance();
        $params = static::getAllValues( static::CONFIG_SECT_NAME );
        $params[ $param_name ] = $value;
        $result = $config->setValue( static::CONFIG_SECT_NAME, $params );
        return $result;
    }





}