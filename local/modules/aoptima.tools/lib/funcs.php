<? namespace AOptima\Tools;
use AOptima\Tools as tools;  

use Bitrix\Main\Application;   
use Bitrix\Main\Page\Asset;
global $asset;   $asset = Asset::getInstance();



class funcs {





	// Проверка "начинается с ..."
	static function string_begins_with($needle, $haystack) {
		return (substr($haystack, 0, strlen($needle))==$needle);
	}





	// Транслит текста
    static function translit($s) {
		$s = (string) $s; // преобразуем в строковое значение
		$s = strip_tags($s); // убираем HTML-теги
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
		$s = trim($s); // убираем пробелы в начале и конце строки
		$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
		$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'zh','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
		$s = str_replace(" ", "_", $s); // заменяем пробелы знаком _
		return $s; // возвращаем результат
	}




	// Обрезка текста
    static function obrezka($str, $limit, $end_simvol){
		$str = strip_tags($str);
		$words = explode(' ', $str); // создаём из строки массив слов
		if ($limit < 1 || sizeof($words) <= $limit) { // если лимит указан не верно или количество слов меньше лимита, то возвращаем исходную строку
			return $str;
		}
		$words = array_slice($words, 0, $limit); // укорачиваем массив до нужной длины
		$out = implode(' ', $words);
		return $out.$end_simvol; //возвращаем строку + символ/строка завершения
	}



	// Получение массива из JSON
    static function json_to_array($json_string){
		$json_array = array();
		$error = false;
		if ($json_string && strlen($json_string) > 0){
			$json = htmlspecialchars_decode($json_string);
			if(strlen($json) > 0){
				$json_array = json_decode($json, true);
				if ( is_null($json_array) ){
					switch (json_last_error()) {
						case JSON_ERROR_DEPTH:
							$error = 'Достигнута максимальная глубина стека';
						break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Некорректные разряды или не совпадение режимов';
						break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Некорректный управляющий символ';
						break;
						case JSON_ERROR_SYNTAX:
							$error = 'Синтаксическая ошибка, не корректный JSON';
						break;
						case JSON_ERROR_UTF8:
							$error = 'Некорректные символы UTF-8, возможно неверная кодировка';
						break;
						case JSON_ERROR_RECURSION:
							$error = 'Одна или несколько зацикленных ссылок в кодируемом значении';
						break;
						case JSON_ERROR_INF_OR_NAN:
							$error = 'Одно или несколько значений NAN или INF в кодируемом значении';
						break;
						case JSON_ERROR_UNSUPPORTED_TYPE:
							$error = 'Передано значение с неподдерживаемым типом';
						break;
						default:
							$error = 'Неизвестная ошибка';
						break;
					}
				}
				
			}
		}
		return $error?$error:$json_array;
	}



	// Многомерный массив в строку GET-параметров
    static function array_to_get_string($array) {
		$get_string = '?';   $cnt = 0;
		foreach ($array as $key_1 => $value_1){
			if ( is_array($value_1) && count($value_1) > 0 ){
				foreach ($value_1 as $key_2 => $value_2){
					if ( is_array($value_2) && count($value_2) > 0 ){
						foreach ($value_2 as $key_3 => $value_3){
							if ( is_array($value_3) && count($value_3) > 0 ){
								foreach ($value_3 as $key_4 => $value_4){
									if ( is_array($value_4) && count($value_4) > 0 ){} else {
										$cnt++;
										$get_string .= (($cnt==1)?'':'&').$key_1.'['.$key_2.']['.$key_3.']['.$key_4.']='.$value_4;
									}
								}
							} else {
								$cnt++;
								$get_string .= (($cnt==1)?'':'&').$key_1.'['.$key_2.']['.$key_3.']='.$value_3;
							}
						}
					} else {
						$cnt++;
						$get_string .= (($cnt==1)?'':'&').$key_1.'['.$key_2.']='.$value_2;
					}
				}
			} else {
				$cnt++;
				$get_string .= (($cnt==1)?'':'&').$key_1.'='.$value_1;
			}
		}
		return $get_string;
	}


	// Удаление пробелов по бокам значений массива
    static function trim_array_values($array) {
		$ret_arr = array();
		foreach($array as $val){
			if (!empty($val)){
				$ret_arr[] = trim($val);
			}
		}
		return $ret_arr;
	}


	// arURI
    static function arURI( $url = false ){
	    if( !$url ){   $url = $_SERVER["REQUEST_URI"];   }
		return explode("/", $url);
	}


	// pureURL
    static function pureURL($url = false){
		if ( !$url ){   $url = $_SERVER["REQUEST_URI"];   }
		$pureURL = $url;
		if (substr_count($pureURL, "?")){
			$pos = strpos($pureURL, "?");
			$pureURL = substr($pureURL, 0, $pos);
		}
		return $pureURL;
	}



	// pre
    static function pre($varriable){
		echo "<pre>"; print_r($varriable); echo "</pre>";
	}



	// pfCnt
    static function pfCnt($n, $form1, $form2, $form5){ // pfCnt($productCount, "товар", "товара", "товаров")
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}


	
	// Получение размера удалённого файла
    static function remoteFileSize($remote_url){
		$fp = fopen($remote_url,"r");
		$inf = stream_get_meta_data($fp);
		fclose($fp);
		foreach($inf["wrapper_data"] as $v)
		if ( stristr($v,"content-length") ){
			$v = explode(":",$v);
			return trim($v[1]);
		}
	}
	
	

    // Добавление GET-параметра в URL
    static function addGetParamToURL($url, $key, $val) {
        if (substr_count($url, "?")) {
            $arURI = explode("?", $url);
            $pureURI = $arURI[0];
            $arRequests = explode("&", $arURI[1]);
            $arNewRequests = Array();
            foreach ($arRequests as $request){
                $arTMP = explode("=", $request);
                if ($arTMP[0] != $key){    $arNewRequests[] = $request;    }
            }
            $arNewRequests[] = $key."=".$val;
            return $pureURI."?".implode("&", $arNewRequests);
        } else {
            return $url."?".$key."=".$val;
        }
    }
	
	

    // Удаление GET-параметра из URL
    static function removeGetParamFromURL($url, $key) {
        if (substr_count($url, "?")) {
            $arURI = explode("?", $url);
            $pureURI = $arURI[0];
            $arRequests = explode("&", $arURI[1]);
            $arNewRequests = Array();
            foreach ($arRequests as $request){
                $arTMP = explode("=", $request);
                if ($arTMP[0] != $key){    $arNewRequests[] = $request;    }
            }
            return $pureURI."?".implode("&", $arNewRequests);
        } else {
            return $url;
        }
    }



    // Добавление под-значения в GET-параметре URL
    static function addSubValueToGetParamInURL($url, $key, $subValue){
        if (substr_count($url, "?")) {
            $arURI = explode("?", $url);
            $pureURI = $arURI[0];
            $arRequests = explode("&", $arURI[1]);
            $arNewRequests = [];  $arNewKeys = [];
            foreach ( $arRequests as $request ){
                $arTMP = explode("=", $request);
                if ($arTMP[0] != $key){
                    $arNewKeys[] = $arTMP[0];
                    $arNewRequests[] = $request;
                } else {
                    $arNewKeys[] = $arTMP[0];
                    $arValues = explode(",", $arTMP[1]);
                    $arValues[] = $subValue;
                    $arTMP[1] = implode(',', $arValues);
                    $request = $arTMP[0].'='.$arTMP[1];
                    $arNewRequests[] = $request;
                }
            }
            if( !in_array($key, $arNewKeys) ){
                $arNewRequests[] = $key."=".$subValue;
            }
            return $pureURI."?".implode("&", $arNewRequests);
        } else {
            return $url."?".$key."=".$val;
        }
    }



    // Удаление под-значения в GET-параметре URL
    static function removeSubValueFromGetParamInURL($url, $key, $subValue){
        if (substr_count($url, "?")) {
            $arURI = explode("?", $url);
            $pureURI = $arURI[0];
            $arRequests = explode("&", $arURI[1]);
            $arNewRequests = [];
            foreach ($arRequests as $request){
                $arTMP = explode("=", $request);
                if( $arTMP[0] == $key ){
                    $arValues = explode(",", $arTMP[1]);
                    $newValues = [];
                    foreach ( $arValues as $k => $v ){
                        if(  strlen($v) > 0  &&  $v != $subValue  ){
                            $newValues[] = $v;
                        }
                    }
                    if( count($newValues) > 0 ){
                        $arTMP[1] = implode(',', $newValues);
                        $request = $arTMP[0].'='.$arTMP[1];
                        $arNewRequests[] = $request;
                    }
                } else {
                    $arNewRequests[] = $request;
                }
            }
            return $pureURI."?".implode("&", $arNewRequests);
        } else {
            return $url;
        }
    }



	// isMAIN
    static function isMain() {
		$arURI = explode("/", $_SERVER["REQUEST_URI"]);
		if (substr_count($arURI[1], "?")) {
			$pos = strpos($arURI[1], "?");
			$arURI[1] = substr($arURI[1], 0, $pos);
		}
		if (substr_count($arURI[1], "index.php")) {
			return true;
		} elseif (count($arURI) > 1 && strlen(trim($arURI[1])) == 0) {
			return true;
		} else {
			return false;
		}
	}



	// getFormatDate
    static function getFormatDate($date) {
		$date = substr($date, 0, 10);
		$months = array(
			"01" => "января",
			"02" => "февраля",
			"03" => "марта",
			"04" => "апреля",
			"05" => "мая",
			"06" => "июня",
			"07" => "июля",
			"08" => "августа",
			"09" => "сентября",
			"10" => "октября",
			"11" => "ноября",
			"12" => "декабря"
		);
		$arData = explode(".", $date);
		$d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
		$newData = $d." ".$months[$arData[1]]." ".$arData[2];
		return $newData;
	}



	// getFormatDate2
    static function getFormatDate2($date) {
		$date = substr($date, 0, 10);
		$months = array(
			"01" => "января",
			"02" => "февраля",
			"03" => "марта",
			"04" => "апреля",
			"05" => "мая",
			"06" => "июня",
			"07" => "июля",
			"08" => "августа",
			"09" => "сентября",
			"10" => "октября",
			"11" => "ноября",
			"12" => "декабря"
		);
		$arData = explode(".", $date);
		$d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
		$newData = $d." ".$months[$arData[1]];
		return $newData;
	}



	// Текущий день недели
	static function week_day($date = false){
		if ( !$date ){   $date = date('d.m.Y');   }
		$day=date('w', strtotime($date) );
		if ($day==1){	$week_day = 1;	}
		if ($day==2){	$week_day = 2;		}
		if ($day==3){	$week_day = 3;		}
		if ($day==4){	$week_day = 4;		}
		if ($day==5){	$week_day = 5;		}
		if ($day==6){	$week_day = 6;		}
		if ($day==0){	$week_day = 7;   }
		return $week_day;
	}

    // Текущий день недели (словом)
    static function week_day_string($date = false){
        if ( !$date ){   $date = date('d.m.Y');   }
        $day=date('w', strtotime($date) );
        if ($day==1){	$week_day = 'Понедельник';	}
        if ($day==2){	$week_day = 'Вторник';		}
        if ($day==3){	$week_day = 'Среда';		}
        if ($day==4){	$week_day = 'Четверг';		}
        if ($day==5){	$week_day = 'Пятница';		}
        if ($day==6){	$week_day = 'Суббота';		}
        if ($day==0){	$week_day = 'Воскресенье';   }
        return $week_day;
    }



	// Дней до даты
	static function daysToDate( $date, $cur_date = false ){
		if ( $date ){
			if( !$cur_date ){   $cur_date = date("d.m.Y");   }
			// cur_date_timestamp
			$dd = ConvertDateTime($cur_date, "DD", "ru");
			$mm = ConvertDateTime($cur_date, "MM", "ru");
			$yyyy = ConvertDateTime($cur_date, "YYYY", "ru");
			$obDate = new DateTime($yyyy.'-'.$mm.'-'.$dd);
			$cur_date_timestamp = $obDate->getTimestamp();
			// date_timestamp
			$dd = ConvertDateTime($date, "DD", "ru");
			$mm = ConvertDateTime($date, "MM", "ru");
			$yyyy = ConvertDateTime($date, "YYYY", "ru");
			$obDate = new DateTime($yyyy.'-'.$mm.'-'.$dd);
			$date_timestamp = $obDate->getTimestamp();
			$days = ($date_timestamp - $cur_date_timestamp) / 60 / 60 / 24;
			return $days;
		}
		return false;
	}



	// Проверка времени на признак "до $time"
	static function check_time($time){
		$datetime1 = date_create(ConvertDateTime(date('d.m.Y H:i:s'), "YYYY-MM-DD HH:MI:SS", "ru"));
		$datetime2 = date_create(ConvertDateTime(date('d.m.Y').' '.$time, "YYYY-MM-DD HH:MI:SS", "ru"));
		$interval = date_diff($datetime1, $datetime2);
		$invert = $datetime1->diff($datetime2)->invert;
		if ($invert == 0){
			return true;
		} else {
			return false;
		}
	}



	// месяц по номеру
	static function getMonthName($number, $nominative_case = false){
		$months = array(
			'01' => array('января', 'январь'),
			'02' => array('февраля', 'февраль'),
			'03' => array('марта', 'март'),
			'04' => array('апреля', 'апрель'),
			'05' => array('мая', 'май'),
			'06' => array('июня', 'июнь'),
			'07' => array('июля', 'июль'),
			'08' => array('августа', 'август'),
			'09' => array('сентября', 'сентябрь'),
			'10' => array('октября', 'октябрь'),
			'11' => array('ноября', 'ноябрь'),
			'12' => array('декабря', 'декабрь')
		);
		return $months[$number][$nominative_case?1:0];
	}



	// Очистка массива от полей с символом "~"
	static function clean_array($array, $not_unset_fields = false){
		if ( is_array($array) && count($array) > 0 ){
			// Стоп-поля
			$not_unset_array = array('PREVIEW_TEXT', 'DETAIL_TEXT', 'IBLOCK_SECTION_ID', 'DESCRIPTION');
			// Добавляем стоп-поля при наличии
			if ( $not_unset_fields && is_array($not_unset_fields) && count($not_unset_fields) > 0 ){
				foreach($not_unset_fields as $field){
					if ( !in_array( $field, $not_unset_array ) ){
						$not_unset_array[] = $field;
					}
				}
			}
			// Перебор полей
			foreach($array as $key => $value){
				if ( strlen($key) > 1 ){
					$key_first_symbol = substr($key, 0, 1);
					$field = substr($key, 1, strlen($key));
					if ($key_first_symbol == '~' && !in_array($field, $not_unset_array)){
						unset($array[$key]);
					}
				}
			}
		}
		return $array;
	}





	// rIMG
	static function rIMG($image, $mode, $width, $height, $watermark_path = false) {
		if (!$image) { $image = false; }
		if (!$mode) { $mode = "4"; }
		if (!$width) { $width = ""; }
		if (!$height) { $height = ""; }
		if ( \CModule::IncludeModule("imager") && $image ){
			global $APPLICATION;
			$image = $APPLICATION->IncludeComponent("soulstream:imager", "", Array(
				"MODE" => $mode,
				"RETURN" => "src",
				"RESIZE_SMALL" => "Y",
				"IMAGE" => is_numeric($arParams['IMAGE'])?CFile::GetPath($image):$image,
				"FILE_NAME" => "",
				"WIDTH" => $width,
				"HEIGHT" => $height,
				"SAVE_DIR" => "",
				"BG" => "#ffffff",
				"QUALITY" => "90",
				"DEBUG" => "N",
				"FILTERTYPE" => "",
				"ADD_CORNER" => "N",
				"CORNER_PATH" => "",
				"ADD_WATERMARK" => ( strlen($watermark_path) > 0 )?"Y":"N",
				"WATERMARK_PATH" => ( strlen($watermark_path) > 0 )?$watermark_path:"",
				"WATERMARK_POSITION" => ( strlen($watermark_path) > 0 )?"mc":"",
				"CACHE_IMAGE" => "Y",
				"ADD_TEXT" => "N",
				"TEXT" => "",
				"TEXT_SIZE" => "",
				"TEXT_COLOR" => "",
				"TEXT_Y" => "",
				"TEXT_X" => "",
				"TEXT_POSITION" => "",
				"TEXT_ANGLE" => "",
				"FONT_PATH" => ""
				),
				false, array("HIDE_ICONS"=>"Y")
			);
			return $image;
		}
	}



	// rIMGG
	static function rIMGG($image, $mode, $width, $height) {
		$max_width=$width;  $max_height=$height;
		$koef = ($width/$height);
		if ($koef<=1){$width=$max_height*$koef; $height=$max_height;}
		if ($koef>1){$width=$max_width; $height=$max_width/$koef;}
		if ($mode == 4){
			$image_array = \CFile::ResizeImageGet($image, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		}
		if ($mode == 5  || !$mode){
			$image_array = \CFile::ResizeImageGet($image, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_EXACT, true);
		}
		$image = $image_array['src'];
		return $image;
	}



	// получение значения параметра $_POST
	static function param_post($param){
		if ( strlen($param) > 0 ){
			$request = Application::getInstance()->getContext()->getRequest(); 
			$value = $request->getPost($param);
			return $value;
		} else {
			return false;
		}
	}
	// получение значения параметра $_GET
	static function param_get($param){
		if ( strlen($param) > 0 ){
			$request = Application::getInstance()->getContext()->getRequest(); 
			$value = $request->getQuery($param);
			return $value;
		} else {
			return false;
		}
	}





	static function addLeftBorder($lb) {
		if ($lb > 1) {
			$lb = $lb - 1;
		}
		return $lb;
	}
	 
	static function addRightBorder($rb,$max) {
		if ($rb < $max) {
			$rb = $rb + 1;
		}
		return $rb;
	}

	
	
}