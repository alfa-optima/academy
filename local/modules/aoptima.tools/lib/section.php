<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class section {
	

	const SECTION_CACHE_NAME = 'section';
	const SUBSECTIONS_CACHE_NAME = 'sub_sects';
	const SECTION_CHAIN_CACHE_NAME = 'section_chain';
	const SECTION_IBLOCK_CACHE_NAME = 'sectionIblock';
	const SECT_CACHE_TIME = 2592000;
	
	
	
	// Получение ID инфоблока (для элемента)
	static function getIblock($sect_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$iblock_id = false;
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'SECT_CACHE_TIME');
		if( strlen($cache_time) > 0 ){} else{
			$cache_time = static::SECT_CACHE_TIME;
		}
		$cache_id = static::SECTION_IBLOCK_CACHE_NAME.'sectionIblock_'.$sect_id;
		$cache_path = '/'.static::SECTION_IBLOCK_CACHE_NAME.'/'.$sect_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$dbSection = \CIBlockSection::GetByID($sect_id);
			if($section = $dbSection->GetNext()){
				$iblock_id = $section['IBLOCK_ID'];
			}
		$obCache->EndDataCache(array('iblock_id' => $iblock_id));
		}
		return $iblock_id;
	}
	
	

	static function getFields(){
		$fields = array();
		$dir_path = $_SERVER['DOCUMENT_ROOT'].'/local/modules/aoptima.tools/install/module_settings/';
		if( !file_exists($dir_path) ){   $dir_path = false;   }
		if( !$dir_path ){
			$dir_path = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/aoptima.tools/install/module_settings/';
			if( !file_exists($dir_path) ){   $dir_path = false;   }
		}
		if( $dir_path ){
			$file_path = $dir_path.'iblock_fields.json';
			if( file_exists($file_path) ){
				$json = file_get_contents($file_path);
				$fields = tools\funcs::json_to_array($json); 
			}
		}
		return $fields;
	}
	
	
	
		
	// Получение инфо о разделе
	static function info($section_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$section = false;
		if ( intval($section_id) > 0 ){
			// ID инфоблока
			$iblock_id = static::getIblock($section_id);
			// Поля для запроса
			$fields_array = array();
			// Доп. поля для разных инфоблоков
			$add_iblock_fields = array();
			// поля для отдельных инфоблоков
			$add_iblock_fields = static::getFields()[$iblock_id]['SECT_FIELDS'];
			// Кешируем
			$obCache = new \CPHPCache();
			$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'SECT_CACHE_TIME');
			if( strlen($cache_time) > 0 ){} else{
				$cache_time = static::SECT_CACHE_TIME;
			}
			$cache_id = static::SECTION_CACHE_NAME.'_'.$section_id;    
			$cache_path = '/'.static::SECTION_CACHE_NAME.'/'.$section_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				// добавляем доп. поля
				if ( !empty($add_iblock_fields) ){
					$fields_array = array_merge($fields_array, $add_iblock_fields);
				}
				// Запрашиваем раздел
				$r_sects = \CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_id, "ID" => $section_id), false, $fields_array);
				while ($sect = $r_sects->GetNext()){    
					$section = tools\funcs::clean_array($sect);
				}
			$obCache->EndDataCache(array('section' => $section));   }
		}
		return $section;
	}
	
	
	
	
	
	// Получение инфо о разделе (по коду)
	static function info_by_code($section_code, $iblock_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$section = false;
		if (  strlen($section_code) > 0  &&  intval($iblock_id) > 0  ){
			// Поля для запроса
			$fields_array = array();
			// Доп. поля для разных инфоблоков
			$add_iblock_fields = static::getFields()[$iblock_id]['SECT_FIELDS'];
			// Кешируем
			$obCache = new \CPHPCache();
			$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'SECT_CACHE_TIME');
			if( strlen($cache_time) > 0 ){} else{
				$cache_time = static::SECT_CACHE_TIME;
			}
			$cache_id = static::SECTION_CACHE_NAME.'_by_code_'.$iblock_id.'_'.$section_code;
			$cache_path = '/'.static::SECTION_CACHE_NAME.'_by_code/'.$iblock_id.'/'.$section_code.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				// добавляем доп. поля
				if ( !empty($add_iblock_fields) ){
					$fields_array = array_merge($fields_array, $add_iblock_fields);
				}
				// Запрашиваем раздел
				$r_sects = \CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_id, "CODE" => $section_code), false, $fields_array);
				while ($sect = $r_sects->GetNext()){
					$section = tools\funcs::clean_array($sect);
				}
			$obCache->EndDataCache(array('section' => $section));   }
		}
		return $section;
	}
	
	
	
	
	
	// Подсчёт подразделов текущего раздела
	static function sub_sects_cnt($section_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'SECT_CACHE_TIME');
		if( strlen($cache_time) > 0 ){} else{
			$cache_time = static::SECT_CACHE_TIME;
		}
		$cache_id = static::SUBSECTIONS_CACHE_NAME.'_cnt_'.$section_id;    
		$cache_path = '/'.static::SUBSECTIONS_CACHE_NAME.'_cnt/'.$section_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			// ID инфоблока
			$iblock_id = static::getIblock($section_id);
			$sects = \CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id, "SECTION_ID" => $section_id), false);    $sub_sects_cnt = $sects->SelectedRowsCount();
		$obCache->EndDataCache(array('sub_sects_cnt' => $sub_sects_cnt));
		}
		return $sub_sects_cnt;
	}


	
	

	// Подразделы текущего раздела
	static function sub_sects($section_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'SECT_CACHE_TIME');
		if( strlen($cache_time) > 0 ){} else{
			$cache_time = static::SECT_CACHE_TIME;
		}
		$cache_id = static::SUBSECTIONS_CACHE_NAME.'_'.$section_id;    
		$cache_path = '/'.static::SUBSECTIONS_CACHE_NAME.'/'.$section_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			// ID инфоблока
			$iblock_id = static::getIblock($section_id);
			$sub_sects = array();
			$sects = \CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id, "SECTION_ID" => $section_id), false);
			while ($sect = $sects->GetNext()){
				$sub_sects[] = $sect;
			}
		$obCache->EndDataCache(array('sub_sects' => $sub_sects));
		}
		return $sub_sects;
	}
	
	
	
	
	
	// Получение цепочки разделов для раздела
	static function chain($section_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'SECT_CACHE_TIME');
		if( strlen($cache_time) > 0 ){} else{
			$cache_time = static::SECT_CACHE_TIME;
		}
		$cache_id = static::SECTION_CHAIN_CACHE_NAME.'_'.$section_id;    
		$cache_path = '/'.static::SECTION_CHAIN_CACHE_NAME.'/'.$section_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			// ID инфоблока
			$iblock_id = static::getIblock($section_id);
			// Получаем цепочку разделов элемента
			$section_chain = array();
			$r_sects = \CIBlockSection::GetNavChain($iblock_id, $section_id);
			while ($sect = $r_sects->GetNext()) {   $section_chain[] = $sect;   }
		$obCache->EndDataCache(array('section_chain' => $section_chain));
		}
		return $section_chain;
	}



	
	
}