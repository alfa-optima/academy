<?php

namespace AOptima\Tools;
use AOptima\Tools as tools;

use \Gumlet\ImageResize;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*   Для работы необходим модуль https://packagist.org/packages/gumlet/php-image-resize
*
*   Установка:
*   composer require gumlet/php-image-resize
*
*   use \Gumlet\ImageResize;
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


class gumlet_image {


    const RESIZE_PATH = '/upload/aoptima_gumlet_resizes';
    const BLUR_PATH = '/upload/aoptima_gumlet_blurs';


    static $fileTypes = [
        'image/jpeg',
        'image/png',
    ];



    // Ресайз "наилучшее встраивание в размеры" с сохранением пропорций
    static function resizeToBestFit( $file_path, $width, $height, int $setQuality = 85 ){
        $logPrefix = 'gumlet_image resizeToBestFit - ';
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path;
        // Если файл существует
        if( file_exists( $file_path ) ){
            // Определение пути к новому файлу ресайза
            $new_file_path = static::RESIZE_PATH.'/toBestFit/';
            $new_file_path .= 'w_'.$width.'_h_'.$height.'/';
            $new_file_path .= 'q_'.intval($setQuality).'/';
            $new_file_path .= md5($file_path).'/';
            if( !file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                mkdir( $_SERVER['DOCUMENT_ROOT'].$new_file_path, 0700, true );
            }
            $new_file_path .= basename($file_path);
            // Если файл ресайза уже существует
            if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                // Сразу возвращаем
                return $new_file_path;
            // Если файла ресайза ещё нет
            } else {
                // Получим тип файла
                $file_type = static::getFileType( $file_path );
                if(
                    // Если тип файла определён
                    isset( $file_type )
                    &&
                    // Если это допустимый тип файла
                    in_array( $file_type, static::$fileTypes )
                ){
                    /// /// ///
                    $image = new ImageResize( $file_path );
                    if( $file_type == 'image/jpeg' ){
                        $image->quality_jpg = $setQuality;
                    } else if( $file_type == 'image/png' ){
                        $image->quality_png = $setQuality;
                    }
                    $image->resizeToBestFit( $width, $height );
                    $image->save( $_SERVER['DOCUMENT_ROOT'].$new_file_path );
                    /// /// ///
                    if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                        return $new_file_path;
                    } else {
                        tools\logger::addError($logPrefix.'не удалось создать файл');
                    }
                } else {
                    tools\logger::addError($logPrefix.'тип файла ('.$file_type.') не входит в список допустимых');
                }
            }
        } else {
            tools\logger::addError($logPrefix.'передан путь к несуществующему файлу');
        }
        return false;
    }



    // Размытие гаусса
    static function gaussBlur( $file_path, $blurCnt = 1 ){
        $logPrefix = 'gumlet_image gaussBlur - ';
        if( $blurCnt <= 0 ){    $blurCnt = 1;    }
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path;
        // Если файл существует
        if( file_exists( $file_path ) ){
            // Определение пути к новому файлу
            $new_file_path = static::BLUR_PATH.'/';
            $new_file_path .= 'blurCnt_'.$blurCnt.'/';
            $new_file_path .= md5($file_path).'/';
            if( !file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                mkdir( $_SERVER['DOCUMENT_ROOT'].$new_file_path, 0700, true );
            }
            $new_file_path .= basename($file_path);
            // Если файл уже существует
            if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                // Сразу возвращаем
                return $new_file_path;
            // Если файла ещё нет
            } else {
                // Получим тип файла
                $file_type = static::getFileType( $file_path );
                if(
                    // Если тип файла определён
                    isset( $file_type )
                    &&
                    // Если это допустимый тип файла
                    in_array( $file_type, static::$fileTypes )
                ){
                    /// /// ///
                    $image = new ImageResize( $file_path );
                    for ( $n = 1; $n <= $blurCnt; $n++ ){
                        $image->addFilter(function ($imageDesc) {
                            imagefilter($imageDesc, IMG_FILTER_GAUSSIAN_BLUR);
                        });
                    }
                    $image->save( $_SERVER['DOCUMENT_ROOT'].$new_file_path );
                    /// /// ///
                    if( file_exists( $_SERVER['DOCUMENT_ROOT'].$new_file_path ) ){
                        return $new_file_path;
                    } else {
                        tools\logger::addError($logPrefix.'не удалось создать файл');
                    }
                } else {
                    tools\logger::addError($logPrefix.'тип файла ('.$file_type.') не входит в список допустимых');
                }
            }
        } else {
            tools\logger::addError($logPrefix.'передан путь к несуществующему файлу');
        }
        return false;
    }



    // Получение типа файла
    static function getFileType( $file_path ){
        if( file_exists( $file_path ) ){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $file_type = finfo_file( $finfo, $file_path );
            finfo_close($finfo);
            return $file_type;
        }
        return null;
    }



}