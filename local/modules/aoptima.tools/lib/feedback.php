<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class feedback {

    const IBLOCK_TYPE = 'aoptima_feedback';
    const MAIL_EVENT_TYPE = 'AOPTIMA_MAIN';




    // Создание типа инфоблока для обратной связи
    static function addIblockType(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $types = \CIBlockType::GetList(array(), array('=ID' => static::IBLOCK_TYPE));
        if( $type = $types->GetNext() ){
            return true;
        } else {
            $fields = Array(
                'ID' => static::IBLOCK_TYPE,
                'SECTIONS' => 'N',
                'SORT' => 1000,
                'LANG' => Array(
                    'ru' => Array(
                        'NAME'=>'Обратная связь',
                        'ELEMENT_NAME' => 'Элементы'
                    ),
                    'en' => Array(
                        'NAME'=>'Feedback',
                        'ELEMENT_NAME' => 'Elements'
                    )
                )
            );
            $obBlocktype = new \CIBlockType;
            $res = $obBlocktype->Add($fields);
            if( $res ){
                return true;
            }
        }
        return false;
    }



    static function addIblock( $iblock_code, $iblock_name ){
        if( static::addIblockType() ){
            $iblocks = \CIBlock::GetList(
                Array(),  Array( "CODE" => $iblock_code ), true
            );
            if( $iblock = $iblocks->GetNext() ){
                return $iblock['ID'];
            } else {
                $ib = new \CIBlock;
                $fields = Array(
                    "ACTIVE" => 'Y',
                    "NAME" => $iblock_name,
                    "CODE" => $iblock_code,
                    "IBLOCK_TYPE_ID" => static::IBLOCK_TYPE,
                    "SITE_ID" => Array("s1"),
                    "SORT" => 100,
                    "WORKFLOW" => "N",
                    "GROUP_ID" => Array("2" => "R")
                );
                $ID = $ib->Add($fields);
                if( intval($ID) > 0 ){    return $ID;    }
            }
        }
        return false;
    }



    // Создание типа почтового шаблона
    static function addMailEventType(){
        $arFilter = array(
            "TYPE_ID" => static::MAIL_EVENT_TYPE
        );
        $types = \CEventType::GetList($arFilter);
        if ( $type = $types->GetNext() ){
            return true;
        } else {
            $et = new \CEventType;
            $id = $et->Add(array(
                "LID"           => 'ru',
                "EVENT_NAME"    => static::MAIL_EVENT_TYPE,
                "NAME"          => 'Основной шаблон AoptimaTools'
            ));
            if( intval($id) > 0 ){
                return true;
            }
        }
        return false;
    }

    // Создание почтового шаблона
    static function addMailEvent(){
        if( static::addMailEventType() ){
            $arFilter = Array(
                "TYPE_ID" => static::MAIL_EVENT_TYPE,
                "SITE_ID" => 's1',
            );
            $items = \CEventMessage::GetList($by="site_id", $order="desc", $arFilter);
            if($item = $items->GetNext()){
                return true;
            } else {
                $arr["ACTIVE"] = "Y";
                $arr["EVENT_NAME"] = static::MAIL_EVENT_TYPE;
                $arr["LID"] = array("s1");
                $arr["EMAIL_FROM"] = "#EMAIL_FROM#";
                $arr["EMAIL_TO"] = "#EMAIL_TO#";
                $arr["CC"] = "#COPY_EMAIL_TO#";
                $arr["BCC"] = "#HIDDEN_COPY_EMAIL_TO#";
                $arr["SUBJECT"] = "#TITLE#";
                $arr["BODY_TYPE"] = "html";
                $arr["MESSAGE"] = "#HTML#";

                $emess = new \CEventMessage;
                $id = $emess->Add($arr);

                if( intval($id) > 0 ){
                    return true;
                }
            }
        }
        return false;
    }



    static function getHtml( $arFields, $params ){
        $cms_html = [];
        foreach ( $params['settings'] as $paramCode => $paramName ){
            foreach ( $arFields as $input_name => $value ){
                if( $paramCode == $input_name ){
                    $params['mail_html'] .= html_entity_decode(htmlspecialchars_decode('<p>'.$paramName.': &nbsp; '.strip_tags($value).'</p>'));
                    $cms_html[] = html_entity_decode(htmlspecialchars_decode($paramName.':&nbsp;&nbsp;&nbsp;'.strip_tags($value)));
                }
            }
        }
        $cms_html = implode("\n", $cms_html);
        return [
            'mail_html' => $params['mail_html'],
            'cms_html' => $cms_html,
        ];
    }



    // Отправка
    static function send( $params, $arFields = null ){
        if(
            strlen($params['iblock_code']) > 0
            &&
            strlen($params['iblock_name'])
            &&
            strlen($params['email_to']) > 0
        ){
            // Проверка создания инфоблока
            $iblock_id = tools\feedback::addIblock(
                $params['iblock_code'],
                $params['iblock_name']
            );
            if( intval($iblock_id) > 0 ){
                // данные в массив
                if( !isset( $arFields ) ){
                    $arFields = Array();
                    parse_str($_POST["form_data"], $arFields);
                }
                $htmlInfo = static::getHtml( $arFields, $params );
                $el = new \CIBlockElement;
                $fields = Array(
                    "IBLOCK_ID"      => $iblock_id,
                    "NAME"           => 'Новый элемент',
                    "PREVIEW_TEXT"   => $htmlInfo['cms_html'],
                    "ACTIVE"         => "Y"
                );
                $id = $el->Add($fields);
                if( intval($id) > 0 ){
                    // отправка на почту
                    static::sendMainEvent (
                        $params['mail_title'],
                        $htmlInfo['mail_html'],
                        $params['email_to'],
                        $params['copy_email_to'],
                        $params['hidden_copy_email_to'],
                    );
                    return true;
                }
            }
        }
        return false;
    }




    // отправка основного почтового шаблона
    static function sendMainEvent (
        $title,
        $html,
        $email_to,
        $copy_email_to = "",
        $hidden_copy_email_to = ""
    ){
        static::addMailEvent();
        $arEventFields = [
            "TITLE" => $title,
            "HTML" => $html,
            "EMAIL_FROM" => static::getEmailFrom(),
            "EMAIL_TO" => $email_to,
            "COPY_EMAIL_TO" => $copy_email_to,
            "HIDDEN_COPY_EMAIL_TO" => $hidden_copy_email_to,
        ];
        \CEvent::SendImmediate(static::MAIL_EVENT_TYPE, "s1", $arEventFields);
    }





    static function getEmailFrom (){
        $email_from = \Bitrix\Main\Config\Option::get('aoptima.tools', 'FORMS_EMAIL_FROM');
        if( !$email_from ){
            $email_from = 'noreply@'.\Bitrix\Main\Config\Option::get('main', 'server_name');
        }
        return $email_from;
    }





}