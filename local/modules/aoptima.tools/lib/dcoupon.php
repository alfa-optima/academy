<?php

namespace AOptima\Tools;
use AOptima\Tools as tools;


use Bitrix\Sale;
use Bitrix\Sale\Internals;
use Bitrix\Main\Entity;


class DcouponTable extends Entity\DataManager {

    public static function getTableName(){
        \Bitrix\Main\Loader::includeModule('sale');
        return Internals\DiscountCouponTable::getTableName();
    }

    public static function getMap(){
        return Internals\DiscountCouponTable::getMap();
    }


    public static function list(){
        $list = [];
        $res = static::getList([
            'select' => [ '*' ],
            'filter' => [],
            //'limit' => 1,
        ]);
        while( $element = $res->fetch() ){
            $list[$element['ID']] = $element;
        }
        return $list;
    }


}