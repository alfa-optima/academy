<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class iblock {


    // инфоблок по его ID
    static function getByID( $iblockID ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblock = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'iblockByID_'.$iblockID;
        $cache_path = '/iblockByID/'.$iblockID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $iblocks = \CIBlock::GetList(
                Array(),
                Array( '=ID' => $iblockID ), true
            );
            if( $ib = $iblocks->GetNext() ){     $iblock = $ib;     }
        $obCache->EndDataCache(array('iblock' => $iblock));
        }
        return $iblock;
    }


	
    // инфоблок по его симв. коду
    static function getByCode( $iblockCode ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblock = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'iblockByCode_'.$iblockCode;
        $cache_path = '/iblockByCode/'.$iblockCode.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $iblocks = \CIBlock::GetList(
                Array(),
                Array( '=CODE' => $iblockCode ), true
            );
            if( $ib = $iblocks->GetNext() ){     $iblock = $ib;     }
        $obCache->EndDataCache(array('iblock' => $iblock));
        }
        return $iblock;
    }


    // инфоблок по его XML_ID
    static function getByXMLID( $xmlID ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblock = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'iblockByXMLID_'.$xmlID;
        $cache_path = '/iblockByXMLID/'.$xmlID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $iblocks = \CIBlock::GetList(
                Array(),
                Array( '=XML_ID' => $xmlID ), true
            );
            if( $ib = $iblocks->GetNext() ){     $iblock = $ib;     }
        $obCache->EndDataCache(array('iblock' => $iblock));
        }
        return $iblock;
    }


	
	
}