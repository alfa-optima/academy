<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class el {
	
	
	const EL_CACHE_NAME = 'el_info';
	const EL_SECTIONS_CACHE_NAME = 'el_sections';
	const EL_IBLOCK_CACHE_NAME = 'elementIblock';
	const EL_CACHE_TIME = 2592000;
	
	
	
    static function getUniqueCode( $string, $iblock_id ){
        $code = strtolower(trim(\AOptima\Tools\funcs::translit( $string )));
		$code_cnt = 1;
        $resultCode = $code;
        $hasCode = true;
        while( $hasCode ){
            $el = static::info_by_code( $resultCode, $iblock_id );
            if( intval($el['ID']) > 0 ){
                $code_cnt++;
                $resultCode = $code.'_'.$code_cnt;
            } else {
                $hasCode = false;
            }
        }
        return $resultCode;
    }
	
	
	
	// Получение ID инфоблока (для элемента)
	static function getIblock($el_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$iblock_id = false;
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'EL_CACHE_TIME');
		if( strlen($cache_time) > 0 ){} else{
			$cache_time = static::EL_CACHE_TIME;
		}
		$cache_id = static::EL_IBLOCK_CACHE_NAME.'_'.$el_id;
		$cache_path = '/'.static::EL_IBLOCK_CACHE_NAME.'/'.$el_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$dbElement = \CIBlockElement::GetByID($el_id);
			if( $element = $dbElement->GetNext() ){
				$iblock_id = $element['IBLOCK_ID'];
			}
		$obCache->EndDataCache(array('iblock_id' => $iblock_id));
		}
		return $iblock_id;
	}
	
	
	
	static function getName($el_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$res = \CIBlockElement::GetByID($el_id);
		if($ar_res = $res->GetNext()){   $el_name = $ar_res['NAME'];   }
		return $el_name;
	}
	

	
	static function getFields(){
		$fields = array();
		$dir_path = $_SERVER['DOCUMENT_ROOT'].'/local/modules/aoptima.tools/install/module_settings/';
		if( !file_exists($dir_path) ){   $dir_path = false;   }
		if( !$dir_path ){
			$dir_path = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/aoptima.tools/install/module_settings/';
			if( !file_exists($dir_path) ){   $dir_path = false;   }
		}
		if( $dir_path ){
			$file_path = $dir_path.'iblock_fields.json';
			if( file_exists($file_path) ){
				$json = file_get_contents($file_path);
				$fields = tools\funcs::json_to_array($json); 
			}
		}
		return $fields;
	}



	// Получение инфо об элементе
	static function info($el_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$el = false;
		if ( intval($el_id) > 0 ){
			// ID инфоблока
			$iblock_id = static::getIblock($el_id);
			// Поля для запроса
			$fields_array = Array('ID', 'ACTIVE', 'NAME', 'CODE', 'XML_ID', 'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID');
			// поля для отдельных инфоблоков
			$iblock_fields = static::getFields()[$iblock_id]['EL_FIELDS'];
			// Свойства для разных инфоблоков
			$add_iblock_props = static::getFields()[$iblock_id]['EL_PROPS'];
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'EL_CACHE_TIME');
			if( strlen($cache_time) > 0 ){} else{
				$cache_time = static::EL_CACHE_TIME;
			}
			$cache_id = static::EL_CACHE_NAME.'_'.$el_id;    
			$cache_path = '/'.static::EL_CACHE_NAME.'/'.$el_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				if( $iblock_fields ){  
					$fields_array = array_merge( $fields_array, $iblock_fields );
					$fields_array = array_unique($fields_array);
				}
				$fields_array = array_unique($fields_array);
				// подтянем все не стопнутые свойства
				// добавляем свойства
				if ( !empty($add_iblock_props) ){
					$multi_props = array();
					// Перебираем массив
					foreach ($add_iblock_props as $prop_code){
						// Если это свойство
						if ( !substr_count($prop_code, 'CATALOG_GROUP_') ){
							// проверяем тип свойства
							$prop_res = \CIBlockProperty::GetByID( $prop_code, $iblock_id );
							if($prop = $prop_res->GetNext()){
								// если это мульти
								if ( $prop['MULTIPLE'] == 'Y' ){
									$multi_props[] = $prop_code;
								} else {
									$fields_array[] = 'PROPERTY_'.$prop_code;
								}
							}
						// Если это ценовая группа
						} else {
							$fields_array[] = $prop_code;
						}
					}
				}
				// Запрашиваем инфо по элементу
				$elements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock_id, "ID"=>$el_id, "ACTIVE" => "Y"), false, Array("nTopCount"=>1), $fields_array);
				while ($element = $elements->GetNext()){
					$el = tools\funcs::clean_array($element);
					// Получение мульти-свойств
					if ( count($multi_props) > 0 ){
						foreach ($multi_props as $prop_code){
							$db_props = \CIBlockElement::GetProperty($iblock_id, $el['ID'], array("sort" => "asc"), array("CODE" => $prop_code));
							while ($prop = $db_props->GetNext()){ 
								$el['PROPERTY_'.$prop_code.'_VALUE'][] = $prop['VALUE'];
								$el['PROPERTY_'.$prop_code.'_DESCRIPTION'][] = $prop['DESCRIPTION'];
							}
						}
					}
				}
				// Удалим пустые значения в массиве
				foreach ($el as $key => $value){
					if ( is_array($el[$key]) ){
						$el[$key] = array_diff($el[$key], array(''));
					}
				}
			$obCache->EndDataCache(array('el' => $el));
			}
		}
		return $el;
	}

	
	
	
		
	static function info_by_code($el_code, $iblock_id){
		\Bitrix\Main\Loader::includeModule('iblock');
		$el = false;
		if (  strlen($el_code) > 0  &&  intval($iblock_id) > 0  ){
			// Поля для запроса
			$fields_array = Array('ID', 'ACTIVE', 'NAME', 'CODE', 'XML_ID', 'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID');
			// поля для отдельных инфоблоков
			$iblock_fields = static::getFields()[$iblock_id]['EL_FIELDS'];
			// Свойства для разных инфоблоков
			$add_iblock_props = static::getFields()[$iblock_id]['EL_PROPS'];
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'EL_CACHE_TIME');
			if( strlen($cache_time) > 0 ){} else{
				$cache_time = static::EL_CACHE_TIME;
			}
			$cache_id = static::EL_CACHE_NAME.'_by_code_'.$iblock_id.'_'.$el_code;
			$cache_path = '/'.static::EL_CACHE_NAME.'_by_code/'.$iblock_id.'/'.$el_code.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				if( $iblock_fields ){  
					$fields_array = array_merge($fields_array, $iblock_fields);
					$fields_array = array_unique($fields_array);
				}
				$fields_array = array_unique($fields_array);
				// добавляем свойства
				if ( !empty($add_iblock_props) ){
					$multi_props = array();
					// Перебираем массив
					foreach ($add_iblock_props as $prop_code){
						// Если это свойство
						if ( !substr_count($prop_code, 'CATALOG_GROUP_') ){
							// проверяем тип свойства
							$prop_res = \CIBlockProperty::GetByID( $prop_code, $iblock_id );
							if($prop = $prop_res->GetNext()){
								// если это мульти-свойство
								if ( $prop['MULTIPLE'] == 'Y' ){
									$multi_props[] = $prop_code;
								} else {
									$fields_array[] = 'PROPERTY_'.$prop_code;
								}
							}
						// Если это ценовая группа
						} else {
							$fields_array[] = $prop_code;
						}
					}
				}
				// Запрашиваем инфо по элементу
				$elements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock_id, "=CODE"=>$el_code, "ACTIVE" => "Y"), false, Array("nTopCount"=>1), $fields_array);
				while ($element = $elements->GetNext()){
					$el = tools\funcs::clean_array($element);
					// Получение мульти-свойств
					if ( count($multi_props) > 0 ){
						foreach ($multi_props as $prop_code){
							$db_props = \CIBlockElement::GetProperty($iblock_id, $el['ID'], array("sort" => "asc"), array("CODE" => $prop_code));
							while ($prop = $db_props->GetNext()){ 
								$el['PROPERTY_'.$prop_code.'_VALUE'][] = $prop['VALUE'];
								$el['PROPERTY_'.$prop_code.'_DESCRIPTION'][] = $prop['DESCRIPTION'];
							}
						}
					}
				}
				// Удалим пустые значения в массиве
				foreach ($el as $key => $value){
					if ( is_array($el[$key]) ){
						$el[$key] = array_diff($el[$key], array(''));
					}
				}
			$obCache->EndDataCache(array('el' => $el));
			}
		}
		return $el;
	}
	
	
	
	
	
    static function info_by_xml_id($xml_id, $iblock_id){
        $el = false;
        if (  strlen($xml_id) > 0  &&  intval($iblock_id) > 0  ){
            // Поля для запроса
            $fields_array = Array('ID', 'ACTIVE', 'NAME', 'CODE', 'XML_ID', 'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID');
            // Каталожные инфоблоки
            $catalogIblocks = project\catalog::iblocks();
            // Если это каталожный инфоблок
            if( $catalogIblocks[$iblock_id] ){
                // поля
                $iblock_fields = static::getCatalogFields()['EL_FIELDS'];
                // Свойства
                $add_iblock_props = static::getCatalogFields()['EL_PROPS'];
            } else {
                // поля для отдельных инфоблоков
                $iblock_fields = static::getFields()[$iblock_id]['EL_FIELDS'];
                // Свойства для разных инфоблоков
                $add_iblock_props = static::getFields()[$iblock_id]['EL_PROPS'];
            }
            // Кешируем
            $obCache = new \CPHPCache();
            $cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'EL_CACHE_TIME');
            if( strlen($cache_time) > 0 ){} else{
                $cache_time = static::EL_CACHE_TIME;
            }
            $cache_id = static::EL_CACHE_NAME.'_by_xml_id_'.$iblock_id.'_'.$xml_id;
            $cache_path = '/'.static::EL_CACHE_NAME.'_by_xml_id/'.$iblock_id.'/'.$xml_id.'/';
            if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
                $vars = $obCache->GetVars();   extract($vars);
            } elseif($obCache->StartDataCache()){
                if( $iblock_fields ){
                    $fields_array = array_merge($fields_array, $iblock_fields);
                    $fields_array = array_unique($fields_array);
                }
                $fields_array = array_unique($fields_array);
                // добавляем свойства
                if ( !empty($add_iblock_props) ){
                    $multi_props = array();
                    // Перебираем массив
                    foreach ($add_iblock_props as $prop_code){
                        // Если это свойство
                        if ( !substr_count($prop_code, 'CATALOG_GROUP_') ){
                            // проверяем тип свойства
                            $prop_res = \CIBlockProperty::GetByID( $prop_code, $iblock_id );
                            if($prop = $prop_res->GetNext()){
                                // если это мульти-свойство
                                if ( $prop['MULTIPLE'] == 'Y' ){
                                    $multi_props[] = $prop_code;
                                } else {
                                    $fields_array[] = 'PROPERTY_'.$prop_code;
                                }
                            }
                            // Если это ценовая группа
                        } else {
                            $fields_array[] = $prop_code;
                        }
                    }
                }
                // Запрашиваем инфо по элементу
                \Bitrix\Main\Loader::includeModule('iblock');
                $elements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock_id, "=XML_ID"=>$xml_id, "ACTIVE" => "Y"), false, Array("nTopCount"=>1), $fields_array);
                while ($element = $elements->GetNext()){
                    $el = tools\funcs::clean_array($element);
                    // Получение мульти-свойств
                    if ( count($multi_props) > 0 ){
                        foreach ($multi_props as $prop_code){
                            $db_props = \CIBlockElement::GetProperty($iblock_id, $el['ID'], array("sort" => "asc"), array("CODE" => $prop_code));
                            while ($prop = $db_props->GetNext()){
                                $el['PROPERTY_'.$prop_code.'_VALUE'][] = $prop['VALUE'];
                                $el['PROPERTY_'.$prop_code.'_DESCRIPTION'][] = $prop['DESCRIPTION'];
                            }
                        }
                    }
                }
                // Удалим пустые значения в массиве
                foreach ($el as $key => $value){
                    if ( is_array($el[$key]) ){
                        $el[$key] = array_diff($el[$key], array(''));
                    }
                }
                $obCache->EndDataCache(array('el' => $el));
            }
        }
        return $el;
    }
	
	
	
	
	
    // Разделы элемента
    static function sections($el_id){
        // Кешируем
        $obCache = new \CPHPCache();
        $cache_time = \Bitrix\Main\Config\Option::get('aoptima.tools', 'EL_CACHE_TIME');
        if (strlen($cache_time) > 0) {
        } else {
            $cache_time = static::EL_CACHE_TIME;
        }
        $cache_id = static::EL_SECTIONS_CACHE_NAME . '_' . $el_id;
        $cache_path = '/' . static::EL_SECTIONS_CACHE_NAME . '/' . $el_id . '/';
        if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
            $vars = $obCache->GetVars();
            extract($vars);
        } elseif ($obCache->StartDataCache()) {
			\Bitrix\Main\Loader::includeModule('iblock');
            $el_sections = array();
            $el_iblock_id = static::getIblock($el_id);
            $db_sections = \CIBlockElement::GetElementGroups($el_id);
            while ($ar_sect = $db_sections->Fetch()) {
                if( $ar_sect['IBLOCK_ID'] == $el_iblock_id ){
                    $el_sections[] = $ar_sect;
                }
            }
            $obCache->EndDataCache(array('el_sections' => $el_sections));
        }
        return $el_sections;
    }
	
	
	
	// Вывод значения свойства типа HTML
    static function htmlPropText( $el, $propCode ){
        $text = $el['PROPERTY_'.$propCode.'_VALUE']['TYPE']=='HTML'?str_replace("\r\n", "", html_entity_decode($el['PROPERTY_'.$propCode.'_VALUE']['TEXT'])):str_replace("\r\n", "<br>", $el['PROPERTY_'.$propCode.'_VALUE']['TEXT']);
        return $text;
    }
	
	
	
	// Проставка свойства SORT_DATE
	static function setSortDate($arFields){
		\Bitrix\Main\Loader::includeModule('iblock');
		$prop = \CIBlockProperty::GetByID('SORT_DATE', $arFields['IBLOCK_ID']);
		if ($prop){
			$date = $arFields['ACTIVE_FROM'];
			if (!$date){
				$q = \CIBlockElement::GetList(Array("SORT"=>"DESC"), Array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']), false, false, Array("ID", "ACTIVE_FROM", "DATE_CREATE"));
				while ($a = $q->GetNext()){
					if ($a['ACTIVE_FROM']){
						$date = $a['ACTIVE_FROM'];
					} else {
						$date = $a['DATE_CREATE'];
					}
				}
			}
			$set_prop = array("SORT_DATE" => $date);
			\CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], $set_prop); 
		}
	}
	
	
	
	
	// Создание файла настроек
	static function createSettingsFile($file_path){
		$fields = array();
		// Перебираем все инфоблоки
		\Bitrix\Main\Loader::includeModule('iblock');
		$result = \CIBlock::GetList( Array(), Array(), true );
		while($iblock = $result->Fetch()){
			$fields[$iblock['ID']] = array(
				'EL_FIELDS' => array(
					'ID', 'ACTIVE', 'NAME', 'CODE', 'XML_ID',
					'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID'
				),
				'EL_PROPS' => array(),
				'SECT_FIELDS' => array()
			);
		}
		// сохраним файл настроек
		$f = fopen($file_path, "w"); 
		$result = fwrite( $f,  json_encode($fields) );
		fclose($f);
	}
	
	
	
	
}