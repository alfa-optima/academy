<? namespace AOptima\Tools;
use AOptima\Tools as tools;
use Bitrix\Main;
use Bitrix\Main\Entity;


class handlers {
	
	static function SEOHandler( Entity\Event $event ){
		BXClearCache(true, "/seoBlocks/");
	}

	static function OnAfterIBlockElementAdd( $arFields ){
		// Проставка свойства SORT_DATE
		tools\el::setSortDate($arFields);
		// Чистим кеши
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."/".$arFields['ID']."/");
		$el = tools\el::info($arFields['ID']);
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID']."/".$el['CODE']."/");
        BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_xml_id/".$arFields['IBLOCK_ID']."/".$el['XML_ID']."/");
        BXClearCache(true, "/".tools\el::EL_SECTIONS_CACHE_NAME."/".$arFields['ID']."/");
	}
	
	static function OnAfterIBlockElementUpdate( $arFields ){
		// Проставка свойства SORT_DATE
		tools\el::setSortDate($arFields);
		// Чистим кеши
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."/".$arFields['ID']."/");
		$el = tools\el::info($arFields['ID']);
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID']."/".$el['CODE']."/");
        BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_xml_id/".$arFields['IBLOCK_ID']."/".$el['XML_ID']."/");
        BXClearCache(true, "/".tools\el::EL_SECTIONS_CACHE_NAME."/".$arFields['ID']."/");
	}
	
	static function OnAfterIBlockElementDelete(){}
	
	
	
	static function OnBeforeIBlockElementAdd( &$arFields ){}
	
	static function OnBeforeIBlockElementUpdate( &$arFields ){}

	static function OnBeforeIBlockElementDelete( $ID ){
		$iblock_id = tools\el::getIblock($ID);
		$el = tools\el::info($ID);
		// Чистим кеши
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."/".$ID."/");
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_code/".$iblock_id."/".$el['CODE']."/");
		BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_xml_id/".$iblock_id."/".$el['XML_ID']."/");
        BXClearCache(true, "/".tools\el::EL_SECTIONS_CACHE_NAME."/".$el['ID']."/");
	}
	
	
	
	static function OnAfterIBlockSectionAdd( $arFields ){
		// Чистим кеши
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$arFields['ID']."/");
		$section = tools\section::info($arFields['ID']);
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID'].'/'.$section['CODE']."/");
		// подразделы
		$sub_sects = tools\section::sub_sects($arFields['ID']);
		if ( count($sub_sects) > 0 ){
			foreach ($sub_sects as $sub_sect){
                BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID']."/".$chainSection['CODE']."/");
                BXClearCache(true, "/".tools\section::SECTION_CHAIN_CACHE_NAME."/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."_cnt/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."/".$sub_sect['ID']."/");	
			}
		}
		//цепочка
		$dbChain = \CIBlockSection::GetNavChain($arFields['IBLOCK_ID'], $arFields['ID']);
		while ($chainSection = $dbChain->GetNext()){
			BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID']."/".$chainSection['CODE']."/");
			BXClearCache(true, "/".tools\section::SECTION_CHAIN_CACHE_NAME."/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."_cnt/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."/".$chainSection['ID']."/");
		}
		BXClearCache(true, "/".tools\el::EL_SECTIONS_CACHE_NAME."/");
	}
	
	static function OnAfterIBlockSectionUpdate( $arFields ){
		// Чистим кеши
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$arFields['ID']."/");
		$section = tools\section::info($arFields['ID']);
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID'].'/'.$section['CODE']."/");
		// подразделы
		$sub_sects = tools\section::sub_sects($arFields['ID']);
		if ( count($sub_sects) > 0 ){
			foreach ($sub_sects as $sub_sect){
                BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID']."/".$chainSection['CODE']."/");
                BXClearCache(true, "/".tools\section::SECTION_CHAIN_CACHE_NAME."/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."_cnt/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."/".$sub_sect['ID']."/");
			}
		}
		//цепочка
		$dbChain = \CIBlockSection::GetNavChain($arFields['IBLOCK_ID'], $arFields['ID']);
		while ($chainSection = $dbChain->GetNext()){
			BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$arFields['IBLOCK_ID']."/".$chainSection['CODE']."/");
			BXClearCache(true, "/".tools\section::SECTION_CHAIN_CACHE_NAME."/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."_cnt/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."/".$chainSection['ID']."/");
		}
		BXClearCache(true, "/".tools\el::EL_SECTIONS_CACHE_NAME."/");
	}
	
	static function OnAfterIBlockSectionDelete(){}
	
	
	
	static function OnBeforeIBlockSectionAdd( &$arFields ){}
	
	static function OnBeforeIBlockSectionUpdate( &$arFields ){}
	
	static function OnBeforeIBlockSectionDelete( $ID ){
		$iblock_id = tools\section::getIblock($ID);
		// Чистим кеши
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$ID."/");
		$section = tools\el::info($ID);
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$iblock_id.'/'.$section['CODE']."/");
		BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$ID."/");
		// подразделы
		$sub_sects = tools\section::sub_sects( $ID );
		if ( count($sub_sects) > 0 ){
			foreach ($sub_sects as $sub_sect){
                BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$iblock_id."/".$chainSection['CODE']."/");
                BXClearCache(true, "/".tools\section::SECTION_CHAIN_CACHE_NAME."/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."_cnt/".$sub_sect['ID']."/");
                BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."/".$sub_sect['ID']."/");	
			}
		}
		//цепочка
		$dbChain = \CIBlockSection::GetNavChain( $iblock_id, $ID );
		while ($chainSection = $dbChain->GetNext()){
			BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/".$iblock_id."/".$chainSection['CODE']."/");
			BXClearCache(true, "/".tools\section::SECTION_CHAIN_CACHE_NAME."/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."_cnt/".$chainSection['ID']."/");
			BXClearCache(true, "/".tools\section::SUBSECTIONS_CACHE_NAME."/".$chainSection['ID']."/");
		}
		BXClearCache(true, "/".tools\el::EL_SECTIONS_CACHE_NAME."/");
	}
	
	
	
	
	
	static function OnAfterIBlockAdd( $arFields ){
		$module_path = tools\module::getPath('aoptima.tools');
		if( $module_path ){
			$file_path = $module_path.'install/module_settings/iblock_fields.json';
			// Если файла нет
			if( !file_exists($file_path) ){
				tools\el::createSettingsFile($file_path);
			}
			if( file_exists($file_path) ){
				$json = file_get_contents($file_path);
				$fields = tools\funcs::json_to_array($json);
				// сохраним файл настроек
				$f = fopen($file_path, "w"); 
				$res = fwrite( $f,  json_encode($fields) );
				fclose($f);
			}
		}
        BXClearCache(true, "/iblockByID/".$arFields['ID']."/");
        BXClearCache(true, "/prop_enum_list/".$arFields['ID']."/");
		BXClearCache(true, "/prop_by_id/");
	}

    static function OnAfterIBlockUpdate( $arFields ){
        BXClearCache(true, "/iblockByID/".$arFields['ID']."/");
		BXClearCache(true, "/prop_enum_list/".$arFields['ID']."/");
		BXClearCache(true, "/prop_by_id/");
    }

	static function OnIBlockDelete( $ID ){
		$module_path = tools\module::getPath('aoptima.tools');
		if( $module_path ){
			$file_path = $module_path.'install/module_settings/iblock_fields.json';
			// Если файла нет
			if( !file_exists($file_path) ){
				tools\el::createSettingsFile($file_path);
			}
			if( file_exists($file_path) ){
				$json = file_get_contents($file_path);
				$fields = tools\funcs::json_to_array($json);
				unset($fields[$ID]);
				// сохраним файл настроек
				$f = fopen($file_path, "w"); 
				$res = fwrite( $f,  json_encode($fields) );
				fclose($f);
			}
		}
        BXClearCache(true, "/iblockByID/".$ID."/");
		BXClearCache(true, "/prop_enum_list/".$ID."/");
		BXClearCache(true, "/prop_by_id/");
	}



    static function OnBeforeIBlockAdd( $arFields ){}

    static function OnBeforeIBlockUpdate( $arFields ){
        $iblock = tools\iblock::getByID( $arFields['ID'] );
        BXClearCache(true, "/iblockByCode/".$iblock['CODE']."/");
        BXClearCache(true, "/iblockByCode/".$arFields['CODE']."/");
        BXClearCache(true, "/iblockByXMLID/".$iblock['XML_ID']."/");
        BXClearCache(true, "/iblockByXMLID/".$arFields['XML_ID']."/");
    }

    static function OnBeforeIBlockDelete( $ID ){
        $iblock = tools\iblock::getByID( $ID );
        BXClearCache(true, "/iblockByCode/".$iblock['CODE']."/");
        BXClearCache(true, "/iblockByXMLID/".$iblock['XML_ID']."/");
    }


	
	
	
	static function OnAfterUserAdd($arFields){
		BXClearCache(true, "/".tools\user::USER_CACHE_NAME."/".$arFields['ID']."/");
	}
	
	static function OnAfterUserUpdate($arFields){
		BXClearCache(true, "/".tools\user::USER_CACHE_NAME."/".$arFields['ID']."/");
	}
	
	static function OnBeforeUserDelete($user_id){
		BXClearCache(true, "/".tools\user::USER_CACHE_NAME."/".$user_id."/");
	}
	
	
	
	
	
}