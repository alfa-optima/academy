<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class user {
	

	
	const USER_CACHE_NAME = 'user';
	
	
	
	
	// Инфо о пользователе
	static function info($user_id){
		$arUser = false;
		if( intval($user_id) > 0 ){
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = 30*24*60*60;
			$cache_id = static::USER_CACHE_NAME.'_'.$user_id;
			$cache_path = '/'.static::USER_CACHE_NAME.'/'.$user_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				// Инфо о пользователе
				$dbUser = \CUser::GetByID($user_id);   $arUser = $dbUser->GetNext();
				if ( intval($arUser['ID']) > 0 ){    
					$arUser = tools\funcs::clean_array($arUser);
				}
			$obCache->EndDataCache(array('arUser' => $arUser));
			}
		}
		return $arUser;
	}
	

	
	static function getFullName( $user_id ){
	    $user = static::info($user_id);
        if( intval($user['ID']) > 0 ){
            $fullName = '';
            if( strlen($user['NAME']) > 0 ){
                $fullName .= $user['NAME'];
            }
            if( strlen($user['LAST_NAME']) > 0 ){
                if( strlen($user['LAST_NAME']) > 0 ){
                    $fullName .= ' ';
                }
                $fullName .= $user['LAST_NAME'];
            }
            if( strlen($fullName) == 0 ){
                $fullName = 'Пользователь [ID = '.$user['ID'].']';
            }
        }
        return false;
    }
	
	
	
	// Авторизация
	static function auth(){
		global $USER;
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);
		$result = $USER->Login(strip_tags($arFields['login']), strip_tags($arFields['password']), "Y");
		if (!is_array($result)){
			$USER->Authorize($USER->GetID());
//            if( !$USER->IsAdmin() ){
//                $user = tools\user::info( $USER->GetID() );
//                if( !$user['UF_REG_CONFIRMED'] ){
//                    $USER->Logout();
//                    // Ответ
//					echo json_encode([ 
//						"status" => "error", 
//						"text" => "Профиль ещё не подтверждён" 
//					]); return;
//                }
//            }
			// Ответ
			echo json_encode( Array( "status" => "ok" ) ); return;
		} else {
			// Ответ
			echo json_encode( Array( "status" => "error", "text" => "Неверный логин или пароль" ) ); return;
		}
	}
	
	
	
	
	// Регистрация
	/*static function register(){
		global $USER;
		if( !$USER->IsAuthorized() ){
			// данные в массив
			$arFields = Array();
			parse_str(param_post("form_data"), $arFields);
			// проверяем EMAIL на логин
			$filter = Array( "LOGIN" => strip_tags($arFields["EMAIL"]) );
			$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter); 
			while ($user = $rsUsers->GetNext()){
				// Если Email уже есть
				echo json_encode( Array( "status" => "error", "text" => "Данный логин уже зарегистрирован на&nbsp;сайте!" ) );  return;
			}
			// проверяем EMAIL на наличие у других пользователей
			$filter = Array( "EMAIL" => strip_tags($arFields["EMAIL"]) );
			$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
			while ($user = $rsUsers->GetNext()){
				// Если Email уже есть
				echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на&nbsp;сайте!" ) );  return;
			}
			// Регистрируем пользователя
			$user = new \CUser;
			$arUserFields = Array(
				"ACTIVE" => "Y",
				"LOGIN" => strip_tags($arFields["EMAIL"]),
				"EMAIL" => strip_tags($arFields["EMAIL"]),
				"NAME" => strip_tags($arFields["NAME"]),
				"PASSWORD" => strip_tags($arFields["PASSWORD"]),
				"CONFIRM_PASSWORD" => strip_tags($arFields["CONFIRM_PASSWORD"]),
				"GROUP_ID" => Array(6)
			);
			$userID = $user->Add($arUserFields);
			if ($userID){
				static::sendRegInfo($userID);
				// Ответ
				echo json_encode( array("status" => 'ok') ); return;
			} else {
				// Ответ
				echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) ); return;
			}
		} else {
			// Ответ
			echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) ); return;
		}
	}*/
	
	
	
	
	// Регистрационная информация на почту
	static function sendRegInfo($user_id, $password = false, $regCode = false){
		if( intval($user_id) > 0 ){
			$user = static::info($user_id);
			if( intval($user['ID']) > 0 ){

                $scheme = tools\config::getValue( 'SCHEME' );
                $serverName = tools\config::getValue( 'DOMAIN' );

			    //////////////////////
                if( strlen($regCode) > 0 ){
                    $title = 'Ссылка для подтверждения регистрации (сайт "'.$serverName.'")';
                } else {
                    $title = 'Регистрационная информация (сайт "'.$serverName.'")';
                }
			    /////////////////////
				$html .= '<p>Ваша регистрационная информация для сайта "'.$serverName.'":</p>';
                $html .= '<p>Ваш логин '.$user["LOGIN"].'</p>';
                if( $password ){
                    $html .= '<p>Ваш пароль: '.$password.'</p>';
                }
				$html .= '<p>Статус профиля: '.($user['ACTIVE']=='Y'?'активен':'не активен').'</p>';

				if( strlen($regCode) > 0 ){
				    $link = $scheme.'://'.$serverName.'/register_confirm/?code='.$regCode;
                    $html .= '<br><p>Ссылка для подтверждения регистрации:<br><a href="'.$link.'">'.$link.'</a></p>';
				}
				/////////////////////////////
				// отправка на почту
				tools\feedback::sendMainEvent(  $title,  $html,  $user["EMAIL"]  );
			}
		}
	}
	
	
	
	
	// Сверка пароля с пользовательским
	static function comparePassword($userId, $password){
        if( intval($userId) > 0 && strlen($password) > 0 ){
            $userData = static::info($userId, true);
            $hashLength = strlen($userData["PASSWORD"]);
            if ($hashLength > 100) {
                $salt = substr($userData["PASSWORD"], 3, 16);
                $hashPassword = crypt($password, "$6$" . $salt . "$");
            } else if ($hashLength > 32) {
                $salt = substr($userData["PASSWORD"], 0, $hashLength - 32);
                $hashPassword = $salt . md5($salt . $password);
            } else {
                $salt = "";
                $hashPassword = $userData["PASSWORD"];
            }
            return ($hashPassword == $userData["PASSWORD"]);
        }
		return false;
	}
	
	
	
    static function getByLogin ( $login, $not_user_id = false, $not_social = true ){
        $fields = Array( 'FIELDS' => array( 'ID', 'LOGIN', 'EXTERNAL_AUTH_ID' )  );
        $filter = Array( "LOGIN_EQUAL" => $login );
        if( $not_social ){    $filter["!EXTERNAL_AUTH_ID"] = "socservices";    }
        if( intval($not_user_id) > 0 ){    $filter["!ID"] = $not_user_id;    }
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        if ($user = $rsUsers->GetNext()){     return $user;     }
        return false;
    }



    static function getByEmail ( $email, $not_user_id = false, $not_social = true ){
        $fields = Array( 'FIELDS' => array( 'ID', 'EMAIL', 'EXTERNAL_AUTH_ID' )  );
        $filter = Array( "EMAIL" => $email );
        if( $not_social ){    $filter["!EXTERNAL_AUTH_ID"] = "socservices";    }
        if( intval($not_user_id) > 0 ){    $filter["!ID"] = $not_user_id;    }
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        if ($user = $rsUsers->GetNext()){     return $user;     }
        return false;
    }
	
	
	
}



