<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class logger {



    static function add( $message, $file_name = null ){

        if( !isset($file_name) ){    $file_name = 'log.log';    }

        $log_path = \Bitrix\Main\Config\Option::get('aoptima.tools', 'LOG_PATH');

        if( !file_exists($_SERVER['DOCUMENT_ROOT'].$log_path) ){
            mkdir($_SERVER['DOCUMENT_ROOT'].$log_path, 0700);
        }
        $message = '----------'."\n".'['.date("Y-m-d H:i:s").'] '.$message."\n";
        $file_path = $_SERVER["DOCUMENT_ROOT"].$log_path.$file_name;

        $f = fopen($file_path, "a");
        fwrite($f, $message);
        fclose($f);

        // Если превышает 4 МБ
        if( filesize($file_path)/1024/1024 > 4  ){

            // Архивируем файл
            $time = time();
            $tmpPath = $_SERVER["DOCUMENT_ROOT"].$log_path."other_logs_".$time."/";
            if( mkdir($tmpPath, 0700) ) {
                copy($file_path, $tmpPath.$file_name);
                $arPackFiles[] = $tmpPath.$file_name;
            }
            $packarc = \CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"].$log_path."/other_logs_".$time.".zip");
            $pRes = $packarc->Pack($arPackFiles);

            // Удаляем что уже не нужно
            unlink($tmpPath.$file_name);
            unlink($file_path);
            rmdir($tmpPath);
        }
    }


	
	static function addError( $message ){
		
		$log_path = \Bitrix\Main\Config\Option::get('aoptima.tools', 'LOG_PATH');
		$file_name = 'errors.log';
		
		if( !file_exists($_SERVER['DOCUMENT_ROOT'].$log_path) ){
			mkdir($_SERVER['DOCUMENT_ROOT'].$log_path, 0700);
		}
		$message = '----------'."\n".'['.date("Y-m-d H:i:s").'] '.$message."\n";
		$file_path = $_SERVER["DOCUMENT_ROOT"].$log_path.$file_name;

		$f = fopen($file_path, "a");
		fwrite($f, $message);
		fclose($f);
		
		// Если превышает 4 МБ
		if( filesize($file_path)/1024/1024 > 4  ){

			// Архивируем файл
			$time = time();
			$tmpPath = $_SERVER["DOCUMENT_ROOT"].$log_path."error_logs_".$time."/";
			if( mkdir($tmpPath, 0700) ) {
				copy($file_path, $tmpPath.$file_name);
				$arPackFiles[] = $tmpPath.$file_name;
			}
			$packarc = \CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"].$log_path."/error_logs_".$time.".zip");
			$pRes = $packarc->Pack($arPackFiles);
			
			// Удаляем что уже не нужно
			unlink($tmpPath.$file_name);
			unlink($file_path);
			rmdir($tmpPath);
		}
	}
	
	
	
}