<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class hiblock {
	

	const HEL_CACHE_NAME = 'hel_info';
	const HEL_CACHE_TIME = 2592000;
	
	
	
	
	
	function __construct($hlblock_id){
		if( intval($hlblock_id) > 0 ){
			\CModule::IncludeModule('highloadblock');
			$this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( $hlblock_id )->fetch();
			$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $this->hlblock );
			$this->entity = $entity;
			$this->entity_data_class = $entity->getDataClass();
			$this->entity_table_name = $this->hlblock['TABLE_NAME'];
			$this->sTableID = 'tbl_'.$this->entity_table_name;
		}
	}
	
	
	
	
	
	// Инфо по элементу
	public function info($item_id){
		$item = false;
		if ( intval($item_id) > 0 ){
			$arFilter = array("ID" => $item_id); 
			$arSelect = array( "ID" );
			$arOrder = array("ID" => "DESC");
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = static::HEL_CACHE_TIME;
			$cache_id = static::HEL_CACHE_NAME.'_'.$item_id;
			$cache_path = '/'.static::HEL_CACHE_NAME.'/'.$item_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				$entity_data_class = $this->entity_data_class;
				$rsData = $entity_data_class::getList(array(
					"select" => $arSelect,
					"filter" => $arFilter,
					"limit" => '1',
					"order" => $arOrder
				));
				$rsData = new \CDBResult($rsData, $this->sTableID); 
				if($el = $rsData->Fetch()){
					$item = tools\funcs::clean_array($el);
				}
			$obCache->EndDataCache(array('item' => $item));
			}
		}
		return $item;
	}
	
	

	
	
}