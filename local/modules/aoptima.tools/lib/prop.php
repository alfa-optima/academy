<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class prop {



    // Свойство по ID
    static function getByID( $prop_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $prop = null;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'prop_by_id_'.$prop_id;
        $cache_path = '/prop_by_id/'.$prop_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $props = \CIBlockProperty::GetByID( $prop_id );
            if( $arProp = $props->GetNext() ){
                $prop = $arProp;
            }
        $obCache->EndDataCache([ 'prop' => $prop ]);
        }
        return $prop;
    }



    // Свойство по коду
    static function getByCode( $prop_code, $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $props = \CIBlockProperty::GetList(
            [ "sort"=>"asc", "name"=>"asc" ],
            [ "IBLOCK_ID" => $iblock_id, "CODE" => $prop_code ]
        );
        if ($prop = $props->GetNext()){
            return $prop;
        }
        return false;
    }


	
	// Код свойства по ID
    static function getCode( $prop_id ){
		\Bitrix\Main\Loader::includeModule('iblock');
		$prop_res = \CIBlockProperty::GetByID( $prop_id );
		if($el_prop = $prop_res->GetNext()){
			$prop_code = $el_prop['CODE'];
			return $prop_code;
		}
		return false;
	}



    // Перечень свойств инфоблока
    static function getList( $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $properties = \CIBlockProperty::GetList(
            [ "sort"=>"asc", "name"=>"asc" ],
            [ "IBLOCK_ID" => $iblock_id ]
        );
        while ($prop = $properties->GetNext()){
            $list[$prop['ID']] = $prop;
        }
        return $list;
    }



    // Добавление свойства в умный фильтр
    static function addToSmartFilter( $prop_id, $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $fields = [ 'SMART_FILTER' => 'Y', 'IBLOCK_ID' => $iblock_id ];
        $obProp = new \CIBlockProperty();
        $obProp->Update( $prop_id, $fields );
    }


    // Удаление свойства из умного фильтра
    static function removeFromSmartFilter( $prop_id, $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $fields = [ 'SMART_FILTER' => 'N', 'IBLOCK_ID' => $iblock_id ];
        $obProp = new \CIBlockProperty();
        $obProp->Update( $prop_id, $fields );
    }


	
}