<? namespace AOptima\Tools;
use AOptima\Tools as tools;


/* Пример использования

$db_debug = new tools\db_debug();
$db_debug->start();

... тут идёт обращение к БД ...

echo "<pre>", print_r($db_debug->Output())."</pre>";

*/


class db_debug {

    private $inc_time;
    private $cnt_query;
    private $query_time;
    private $arQueryDebugSave;



    public function start(){
        global $DB;
        $this->inc_time = getmicrotime();
        if( $DB->ShowSqlStat ){
            $this->cnt_query = $DB->cntQuery;
            $this->query_time = $DB->timeQuery;
            $this->arQueryDebugSave = $DB->arQueryDebug;
            $DB->arQueryDebug = array();
        }
    }



    public function output($bTrac = false){
        global $DB, $APPLICATION, $USER;
        if ($USER->IsAdmin()){
            $this->inc_time = round(getmicrotime()-$this->inc_time, 4);
            if( ($DB->cntQuery - $this->cnt_query) > 0 ){
                return array(
                    "PATH" => $_SERVER["SCRIPT_NAME"],
                    "QUERY_COUNT" => $DB->cntQuery - $this->cnt_query,
                    "QUERY_TIME" => round($DB->timeQuery - $this->query_time, 4),
                    "ALL_TIME" => $this->inc_time,
                    "TRAC" => ($bTrac == true) ? $DB->arQueryDebug : '',
                );
            }
        }
    }

}