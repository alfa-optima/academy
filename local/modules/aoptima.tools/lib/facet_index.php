<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class facet_index {


    const TEMP_PATH = '/upload/facet_index/';
    const FILE_NAME = 'reindex_task.json';
    const LOG_FILE = 'log.txt';
    const MAX_EX_TIME = 55;



    // Запуск переиндексации фасетного индекса
    // (в него можно сразу передать параметр $iblocks - массив из ID инфоблоков
    // для добавления в задачу на переиндексацию)
    static function start_reindex( $iblocks = false ){
        if( is_array($iblocks) || ( !is_array($iblocks) && intval($iblocks) > 0) ){
            static::add_to_reindex( $iblocks );
        }
        // Путь к файлу задачи
        $file_path = $_SERVER['DOCUMENT_ROOT'].static::TEMP_PATH.static::FILE_NAME;
        if( file_exists( $file_path ) ){
            // Прочтём имеющиеся параметры
            $taskIblocksJSON = file_get_contents( $file_path );
            if( strlen($taskIblocksJSON) > 0 ){
                $taskIblocks = tools\funcs::json_to_array( $taskIblocksJSON );
                if( count($taskIblocks) > 0 ){
                    // Берём 1-ый попавшийся инфоблок
                    $iblock_id = array_keys($taskIblocks)[0];
                    $iblockParams = $taskIblocks[ $iblock_id ];
                    // Установим макс. время работы скрипта
                    $iblockParams['max_execution_time'] = static::MAX_EX_TIME;
                    // Запускаем его реиндексацию
                    $iblockParams = static::reindex_iblock($iblockParams);
                    // Если инфоблок уже не требует переиндексации
                    if( !static::needReindex($iblock_id) ){
                        unset($taskIblocks[$iblock_id]);
                    } else {
                        $taskIblocks[$iblock_id] = $iblockParams;
                    }
                    // Если ещё остались до конца не проиндексированные инфоблоки
                    if( count($taskIblocks) > 0 ){
                        // Запишем в файл параметры по инфоблокам
                        $file = fopen( $file_path, "w" );
                        $res = fwrite( $file,  json_encode($taskIblocks) );
                        fclose($file);
                    } else {
                        // Удалим файл задачи
                        unlink($file_path);
                    }
                } else {
                    // Удалим файл задачи
                    unlink($file_path);
                }
            } else {
                // Удалим файл задачи
                unlink($file_path);
            }
        }
        // Запишем в файл параметры по инфоблокам
        $log_text = 'Последнее обращение к методу "\AOptima\Tools\facet_index::start_reindex": '.date('d.m.Y H:i:s');
        $file_path = $_SERVER['DOCUMENT_ROOT'].static::TEMP_PATH.static::LOG_FILE;
        $file = fopen( $file_path, "w" );
        $res = fwrite( $file,  $log_text );
        fclose($file);
    }



    // Добавление инфоблоков в задачу на переиндексацию
    static function add_to_reindex( $iblocks ){
        if( !is_array($iblocks) && intval($iblocks) > 0 ){
            $iblocks = [ $iblocks ];
        }
        if( is_array($iblocks) && count($iblocks) > 0 ){
            // Временная папка для работы
            $path = $_SERVER['DOCUMENT_ROOT'].static::TEMP_PATH;
            if( !file_exists( $path ) ){     mkdir( $path, 0700 );     }
            $file_path = $_SERVER['DOCUMENT_ROOT'].static::TEMP_PATH.static::FILE_NAME;
            // Если файл активной задачи уже существует
            if( file_exists( $file_path ) ){
                // Прочтём имеющиеся параметры
                $taskIblocksJSON = file_get_contents($file_path);
                $taskIblocks = tools\funcs::json_to_array($taskIblocksJSON);
                // Переберём переданные инфоблоки
                foreach ( $iblocks as $key => $iblock_id ){
                    // Если в массиве $taskIblocks параметров по инфоблоку ещё нет
                    if(
                        is_int($iblock_id)
                        &&
                        intval($iblock_id) > 0
                        &&
                        !$taskIblocks[$iblock_id]
                    ){
                        // Добавим инфоблок в массив (со стартовыми параметрами)
                        $taskIblocks[$iblock_id] = [
                            'cnt' => 0, // Всего элементов проиндексированно
                            'last_id' => 0, // Последний элемент
                            'total' => 0, // Всего элементов
                            'iblock_id' => $iblock_id, // ИД инфоблока
                            'first_start' => true, // Первый запуск
                        ];
                    }
                }
            // Файл активной задачи отсутствует
            } else {
                // Зададим стартовые параметры по инфоблокам
                $taskIblocks = [];
                // Переберём переданные инфоблоки
                foreach ( $iblocks as $iblock_id ){
                    if( intval($iblock_id) > 0 ){
                        // Добавим инфоблок в массив (со стартовыми параметрами)
                        $taskIblocks[intval($iblock_id)] = [
                            'cnt' => 0, // Всего элементов проиндексированно
                            'last_id' => 0, // Последний элемент
                            'total' => 0, // Всего элементов
                            'iblock_id' => intval($iblock_id), // ИД инфоблока
                            'first_start' => true, // Первый запуск
                        ];
                    }
                }
            }
            // Переберём по факту все имеющиеся в задаче инфоблоки
            foreach ( $taskIblocks as $iblock_id => $params ){
                // Если инфоблок уже не требует переиндексации
                if( !static::needReindex($iblock_id) ){
                    // Удалим этот инфоблок из задачи
                    unset( $taskIblocks[$iblock_id] );
                }
            }
            // Если есть инфоблоки для реиндексации
            if( count( $taskIblocks ) > 0 ){
                // Запишем в файл параметры по инфоблокам
                $file = fopen( $file_path, "w" );
                $res = fwrite( $file,  json_encode($taskIblocks) );
                fclose($file);
            } else {
                // Удалим файл задачи
                unlink($file_path);
            }
        }
    }



    // Проверка необходимости переиндексации инфоблока
    static function needReindex( $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblockInfo = \Bitrix\Iblock\IblockTable::getList(array(
            'select' => array('ID', 'PROPERTY_INDEX'),
            'filter' => array('=ID' => $iblock_id)
        ))->fetch();
        return ($iblockInfo['PROPERTY_INDEX'] == 'I');
    }



    // Переиндексация инфоблока
    static function reindex_iblock( $params ){
        $index = \Bitrix\Iblock\PropertyIndex\Manager::createIndexer( $params['iblock_id'] );
        if ($params['first_start']) {
            $params['cnt'] = 0;
            $params['last_id'] = 0;
            $index->startIndex();
            $params['total'] = $index->estimateElementCount();
            $params['first_start'] = false;
        }
        $index->setLastElementId($params['last_id']);
        $res = $index->continueIndex($params['max_execution_time']);
        if ($res > 0) {
            $params['cnt'] += $res;
            $params['last_id'] = $index->getLastElementId();
        } else {
            $index->endIndex();
            \CBitrixComponent::clearComponentCache("bitrix:catalog.smart.filter");
            \CIBlock::clearIblockTagCache($params['iblock_id']);
        }
        return $params;
    }



    // Сброс фасетного индекса по инфоблоку
    static function delete( $iblock_id ){
        \Bitrix\Iblock\PropertyIndex\Manager::DeleteIndex( $iblock_id );
        \Bitrix\Iblock\PropertyIndex\Manager::markAsInvalid( $iblock_id );
    }



}