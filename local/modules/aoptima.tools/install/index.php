<?
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config as Conf;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);
Class aoptima_tools extends CModule {

    var $exclusionAdminFiles;

	
	function __construct(){
		
		$arModuleVersion = array();
		include(__DIR__."/version.php");
		
        $MODULE_EVENTS = array();
        include(__DIR__."/events.php");
		$this->events = $MODULE_EVENTS;

        $this->exclusionAdminFiles=array(
            '..', '.', 'menu.php',
			'operation_description.php', 'task_description.php'
        );

        $this->MODULE_ID = 'aoptima.tools';
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("AOPTIMA_TOOLS_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("AOPTIMA_TOOLS_MODULE_DESC");

		$this->PARTNER_NAME = Loc::getMessage("AOPTIMA_TOOLS_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("AOPTIMA_TOOLS_PARTNER_URI");

        $this->MODULE_SORT = 1;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS='Y';
        $this->MODULE_GROUP_RIGHTS = "Y";
	}

	
    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot=false){
        if($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(),'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

	
    //Проверяем что система поддерживает D7
    public function isVersionD7(){
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

	
    function InstallDB(){}

	
    function UnInstallDB(){}

	
	function InstallEvents(){
		$eventManager = \Bitrix\Main\EventManager::getInstance();
        foreach ($this->events as $event) {
            $eventManager->registerEventHandler(
				$event[0], $event[1], $event[2],
				$event[3], $event[4]
			);
        }
        return true;
	}

	
	function UnInstallEvents(){
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        foreach ($this->events as $event) {
            $eventManager->unRegisterEventHandler(
				$event[0], $event[1], $event[2],
				$event[3], $event[4]
			);
        }
        return true;
	}

	
	function InstallFiles( $arParams = array() ){
		
		$doc_root = $_SERVER['DOCUMENT_ROOT'];
		
		
		// проверяем наличие файла настроек
		$file_path = __DIR__.'/module_settings/iblock_fields.json';
		// Если файла нет
		if( !file_exists($file_path) ){
			$fields = array();
			// Перебираем все инфоблоки
			\Bitrix\Main\Loader::includeModule('iblock');
			$res = \CIBlock::GetList( Array(), Array(), true );
			while($iblock = $res->Fetch()){
				$fields[$iblock['ID']] = array(
					'EL_FIELDS' => array(
						'ID', 'ACTIVE', 'NAME', 'CODE', 'XML_ID',
						'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID'
					),
					'EL_PROPS' => array(),
					'SECT_FIELDS' => array()
				);
			}
			// сохраним файл настроек
			$f = fopen($file_path, "w"); 
			$res = fwrite( $f,  json_encode($fields) );
			fclose($f);
		}
		
		
		
        /*$path = $this->GetPath()."/install/components";
        if( \Bitrix\Main\IO\Directory::isDirectoryExists($path) )
            CopyDirFiles($path, $doc_root."/local/components", true, true);
        else
            throw new \Bitrix\Main\IO\InvalidPathException($path);*/

		/*CopyDirFiles(
			// from
			$this->GetPath()."/install/admin/",
			// to
			$doc_root."/bitrix/admin/"
		);*/

		
		
		$admin_path = $this->GetPath().'/install/admin/';
        if ( \Bitrix\Main\IO\Directory::isDirectoryExists( $admin_path ) ){
            if ( $dir = opendir( $admin_path ) ){
                while (false !== $item = readdir($dir)){
					if ( in_array($item, $this->exclusionAdminFiles) ){   continue;   }
                    file_put_contents(
						$doc_root.'/bitrix/admin/'.$this->MODULE_ID.'_'.$item,
                        '<'.'? require($_SERVER["DOCUMENT_ROOT"]."'.$this->GetPath(true).'/install/admin/'.$item.'");?'.'>'
					);
                }
                closedir($dir);
            }
        }


        $install_paths = [ 'js', 'themes' ];
        foreach ( $install_paths as $path ){
            $from = $this->GetPath().'/install/'.$path.'/';
            $to = $_SERVER['DOCUMENT_ROOT'].'/bitrix/'.$path.'/';
            CopyDirFiles($from, $to, true, true);
        }


        return true;
	}

	function UnInstallFiles(){
		
		$doc_root = $_SERVER['DOCUMENT_ROOT'];
		
        //\Bitrix\Main\IO\Directory::deleteDirectory($doc_root.'/local/components/aoptima/');
		
		
		/*DeleteDirFiles(
			$this->GetPath().'/install/admin/',
			$doc_root.'/bitrix/admin/'
		);*/
		
		
        if ( \Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/install/admin') ){
            if ($dir = opendir($path)) {
                while (false !== $item = readdir($dir)) {
                    if (in_array($item, $this->exclusionAdminFiles)){   continue;   }
                    \Bitrix\Main\IO\File::deleteFile( 
						$doc_root.'/bitrix/admin/'.$this->MODULE_ID.'_'.$item
					);
                }
                closedir($dir);
            }
        }


        DeleteDirFilesEx( '/bitrix/js/'.$this->MODULE_ID );

        $examplePath = $this->GetPath()."/install/themes/.default/";
        $toDeletePath = $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default";
        DeleteDirFiles($examplePath, $toDeletePath);

		
		return true;
	}

	
	function DoInstall(){
		global $APPLICATION;
        if($this->isVersionD7()){
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
			$this->InstallAgents();

            #работа с .settings.php
            $configuration = Conf\Configuration::getInstance();
            $aoptima_module_tools=$configuration->get('aoptima_module_tools');
            $aoptima_module_tools['install']=$aoptima_module_tools['install']+1;
            $configuration->add('aoptima_module_tools', $aoptima_module_tools);
            $configuration->saveConfiguration();
            #работа с .settings.php
        } else {
            $APPLICATION->ThrowException(Loc::getMessage("AOPTIMA_TOOLS_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("AOPTIMA_TOOLS_INSTALL_TITLE"), $this->GetPath()."/install/step.php");
	}

	
	function DoUninstall(){
        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if( $request["step"] < 2 ){
			
            $APPLICATION->IncludeAdminFile(Loc::getMessage("AOPTIMA_TOOLS_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep1.php");
			
        } elseif( $request["step"] == 2 ) {
			
            $this->UnInstallFiles();
			$this->UnInstallEvents();
			$this->UnInstallAgents();

            if($request["savedata"] != "Y")
                $this->UnInstallDB();

			Option::delete($this->MODULE_ID);
			
            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            #работа с .settings.php
            $configuration = Conf\Configuration::getInstance();
            $aoptima_module_tools=$configuration->get('aoptima_module_tools');
            $aoptima_module_tools['uninstall']=$aoptima_module_tools['uninstall']+1;
            $configuration->add('aoptima_module_tools', $aoptima_module_tools);
            $configuration->saveConfiguration();
            #работа с .settings.php

            $APPLICATION->IncludeAdminFile(Loc::getMessage("AOPTIMA_TOOLS_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep2.php");
        }
	}

	
    protected function InstallAgents() {
        return true;
    }

	
    protected function UnInstallAgents() {
        $agent = new CAgent();
        $agent->RemoveModuleAgents($this->MODULE_ID);
        return true;
    }
	
	
    function GetModuleRightList(){
        return array(
            "reference_id" => array("D","K","S","W"),
            "reference" => array(
                "[D] ".Loc::getMessage("AOPTIMA_TOOLS_DENIED"),
                "[K] ".Loc::getMessage("AOPTIMA_TOOLS_READ_COMPONENT"),
                "[S] ".Loc::getMessage("AOPTIMA_TOOLS_WRITE_SETTINGS"),
                "[W] ".Loc::getMessage("AOPTIMA_TOOLS_FULL"))
        );
    }
	
	
} 

?>