<?php

// Массив событий модуля.

 
$MODULE_EVENTS = array(



    array(
        'iblock',
        'OnAfterIBlockElementAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockElementDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockElementDelete',
        100,
    ),
	
	
    array(
        'iblock',
        'OnAfterIBlockSectionAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockSectionDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockSectionDelete',
        100,
    ),
	




    array(
        'iblock',
        'OnAfterIBlockAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnAfterIBlockUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnIBlockDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnIBlockDelete',
        100
    ),

    array(
        'iblock',
        'OnBeforeIBlockAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeIBlockDelete',
        100
    ),



	
	
    array(
        'main',
        'OnAfterUserAdd',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterUserAdd',
        100,
    ),
    array(
        'main',
        'OnAfterUserUpdate',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnAfterUserUpdate',
        100,
    ),
    array(
        'main',
        'OnBeforeUserDelete',
        'aoptima.tools',
        '\AOptima\Tools\handlers',
        'OnBeforeUserDelete',
        100,
    ),







    array(
        'main',
        'OnUserTypeBuildList',
        'aoptima.tools',
        'AOptimaToolsCustomUserFieldHtml',
        'GetUserTypeDescription',
        100,
    ),

    array(
        'iblock',
        'OnIBlockPropertyBuildList',
        'aoptima.tools',
        'AOptimaToolsCustomColorProp',
        'GetIBlockPropertyDescription',
        100,
    ),

    array(
        'iblock',
        'OnIBlockPropertyBuildList',
        'aoptima.tools',
        'AOptimaToolsCustomCheckboxNumProp',
        'GetUserTypeDescription',
        100,
    ),

	
	
);

