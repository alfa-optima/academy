<?

if (!function_exists('htmlspecialcharsbx')) {
    function htmlspecialcharsbx($string, $flags=ENT_COMPAT) {
        return htmlspecialchars($string, $flags, (defined('BX_UTF')? 'UTF-8' : 'ISO-8859-1'));
    }
}


\CModule::AddAutoloadClasses(
    'aoptima.tools', [

        'AOptimaToolsCustomUserFieldHtml' => 'classes/general/custom_user_field_html.php',

        'AOptimaToolsCustomColorProp' => 'classes/general/custom_color_prop.php',
        'AOptimaToolsCustomCheckboxNumProp' => 'classes/general/custom_checkbox_num_prop.php',

        'AOptimaToolsModuleVersion' => 'classes/general/module_version.php',
        'AOptimaToolsIblockVersion' => 'classes/general/iblock_version.php',
    ]
);


