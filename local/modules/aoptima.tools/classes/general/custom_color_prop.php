<?

IncludeModuleLangFile(__FILE__);

define ('PROP_TYPE_ID', 'AOptimaColor');



class AOptimaToolsCustomColorProp extends CUserTypeString {

    public static function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => PROP_TYPE_ID,
            "CLASS_NAME" => "AOptimaToolsCustomColorProp",
            "DESCRIPTION" => GetMessage('COLORPICKER_PROPERTY_NAME'),
            "BASE_TYPE" => "int",
        );
    }

    public function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => PROP_TYPE_ID,
            "DESCRIPTION" => GetMessage('COLORPICKER_PROPERTY_NAME'),
            'GetPropertyFieldHtml' => array('AOptimaToolsCustomColorProp', 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array('AOptimaToolsCustomColorProp', 'GetAdminListViewHTML'),
        );
    }

    public function getViewHTML($name, $value) {
        return '<div style="display: block; border: 1px solid; border-radius: 25px; display: block; height: 30px; width: 30px; background-color: #' . $value . '">&nbsp;</div>';
    }


    public static function getEditHTML($name, $value, $is_ajax = false) {
        $pathToLib = "/bitrix/js/aoptima.tools/";
        $uid = 'x' . uniqid();
        $dom_id = $uid;
        CJSCore::Init(array("jquery"));
        ob_start(); ?>
        <script type='text/javascript' src='<?= $pathToLib ?>jpicker-1.1.6.js'></script>
        <input type='text' id='element<?= $dom_id ?>' name='<?= $name ?>' value='<?= $value ?>' />
        <script>
            $(document).ready(function() {
                $('#element<?= $dom_id ?>').jPicker({
                    images: {
                        clientPath: '<?= $pathToLib ?>images/'
                    },
                    localization: {
                        text: {
                            title: 'Перетяните маркер для выбора цвета',
                            newColor: 'новый',
                            currentColor: 'текущий',
                            ok: 'принять',
                            cancel: 'отменить'
                        },
                        tooltips: {
                            colors: {
                                newColor: 'Новый цвет - нажмите "принять" для подтверждения',
                                currentColor: 'Нажмите для возврата первоначального цвета'
                            },
                            buttons: {
                                ok: 'Нажмите для выбора текущего цвета',
                                cancel: 'Нажмите для возврата первоначального цвета'
                            },
                            hue: {
                                radio: 'Set To "Hue" Color Mode',
                                textbox: 'Enter A "Hue" Value (0-360∞)'
                            },
                            saturation: {
                                radio: 'Set To "Saturation" Color Mode',
                                textbox: 'Enter A "Saturation" Value (0-100%)'
                            },
                            value: {
                                radio: 'Set To "Value" Color Mode',
                                textbox: 'Enter A "Value" Value (0-100%)'
                            },
                            red: {
                                radio: 'Set To "Red" Color Mode',
                                textbox: 'Enter A "Red" Value (0-255)'
                            },
                            green: {
                                radio: 'Set To "Green" Color Mode',
                                textbox: 'Enter A "Green" Value (0-255)'
                            },
                            blue: {
                                radio: 'Set To "Blue" Color Mode',
                                textbox: 'Enter A "Blue" Value (0-255)'
                            },
                            alpha: {
                                radio: 'Set To "Alpha" Color Mode',
                                textbox: 'Enter A "Alpha" Value (0-100)'
                            },
                            hex: {
                                textbox: 'Enter A "Hex" Color Value (#000000-#ffffff)',
                                alpha: 'Enter A "Alpha" Value (#00-#ff)'
                            }
                        }
                    }
                });
            });
        </script>
        <? $HTML = ob_get_clean();
        return $HTML;
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl) {
        return static::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    public function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        return static::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    public function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        return static::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        return $strHTMLControlName['MODE'] == 'FORM_FILL' ? static::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false) : static::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

};