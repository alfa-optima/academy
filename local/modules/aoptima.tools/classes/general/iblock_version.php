<?php

class AOptimaToolsIblockVersion {

	public static function isIblockNewGridv18() {
		return self::checkMinVersion('18.0.0');
	}

	public static function getIblockVersion() {
		return AOptimaToolsModuleVersion::getModuleVersion('iblock');
	}

	public static function checkMinVersion($checkVersion)
	{
		return AOptimaToolsModuleVersion::checkMinVersion('iblock', $checkVersion);
	}
}