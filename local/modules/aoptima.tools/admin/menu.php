<?
/** @global CMain$APPLICATION */
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


if ($APPLICATION->GetGroupRight('currency') == 'D')
	return false;



return array(
	"parent_menu" => "global_menu_content",
	"section" => "aoptima_tools",
	"sort" => 500,
	"text" => 'Aoptima tools',
	"title" => 'Aoptima tools',
	// url - при необходимости
	//"url" => "aoptima.tools_test.php",
	"items_id" => "menu_aoptima_tools",
	// items - при необходимости
	"items" => array(
		array(
			"text" => 'Поля для инфоблоков',
			"title" => 'Поля для инфоблоков',
			"url" => "aoptima.tools_iblock_fields.php?lang=".LANGUAGE_ID
		),
	)
);

