<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['IS_AUTH'] = 'N';

if( $USER->IsAuthorized() ){

    $arResult['IS_AUTH'] = 'Y';

    $arResult['USER'] = tools\user::info( $USER->GetID() );

    $arResult['COURSE'] = tools\el::info( intval( $_GET['course_id'] ) );

    if( intval( $arResult['COURSE']['ID'] ) > 0 ){

        $arResult['COURSE_TYPE'] = 'курс';
        $learn_type = tools\el::info( $arResult['COURSE']['PROPERTY_TYPE_VALUE'] );
        if( trim(mb_strtolower( $learn_type['NAME'] )) == 'вебинар' ){
            $arResult['COURSE_TYPE'] = 'вебинар';
        }
        $APPLICATION->SetPageProperty("title", 'Регистрация на '.$arResult['COURSE_TYPE']);

        $arResult['REGISTER_AVAILABLE'] = project\learning_course::registerAvailable( $arResult['COURSE']['ID'] );

        $arResult['FREE_PLACES_CNT'] = project\learning_course::freePlacesCnt( $arResult['COURSE']['ID'] );

        $arResult['PROGRAM'] = tools\el::info( $arResult['PROPERTY_PRORGAM_VALUE'] );

        $arResult['DATE_FROM'] = ConvertDateTime( $arResult['COURSE']['PROPERTY_DATE_FROM_VALUE'], "DD.MM.YYYY HH:MI", "ru" );
        $arResult['weekDay'] = tools\funcs::week_day_string( ConvertDateTime( $arResult['COURSE']['PROPERTY_DATE_FROM_VALUE'], "DD.MM.YYYY", "ru" ) );

        $arResult['PROGRAM'] = tools\el::info( $arResult['PROPERTY_PRORGAM_VALUE'] );

        $arResult['AREAS_OF_ACTIVITY'] = project\personal::$activities;

    }
}


$this->IncludeComponentTemplate();





