<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

if( $arResult['IS_AUTH'] == 'Y' ){ ?>


    <section class="section">
        <div class="container">


            <? if( $arResult['REGISTER_AVAILABLE']['result'] ){ ?>


                <h2 class="section__title text-uppercase">Регистрация на <?=$arResult['COURSE_TYPE']?></h2>

                <p><?=$arResult['COURSE']['NAME']?></p>

                <p><?=$arResult['weekDay']?> <?=$arResult['DATE_FROM']?></p>

                <p class="rw-info__desc">Количество свободных мест: <span><?=$arResult['FREE_PLACES_CNT']?></span></p>

                <div class="course bg-brown-soft">

                    <form id="contactform" action="" method="post">

                        <input type="hidden" name="course_id" value="<?=$arResult['COURSE']['ID']?>">

                        <div class="course__item">

                            <h4>Укажите Ваши данные</h4>

                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="phone" class="form-label">Телефон *</label>
                                        <input id="phone" type="text" name="phone" class="form-control" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="city" class="form-label">Город *</label>
                                        <input id="city" type="text" name="city" class="form-control" value="<?=$arResult['USER']['PERSONAL_CITY']?>">
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="f" class="form-label">Фамилия *</label>
                                        <input id="f" type="text" name="f" class="form-control" value="<?=$arResult['USER']['LAST_NAME']?>">
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="i" class="form-label">Имя *</label>
                                        <input id="i" type="text" name="i" class="form-control" value="<?=$arResult['USER']['NAME']?>">
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="o" class="form-label">Отчество *</label>
                                        <input id="o" type="text" name="o" class="form-control" value="<?=$arResult['USER']['SECOND_NAME']?>">
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-4">
                                    <p class="mb-1">Пол *</p>
                                    <div class="mb-3">
                                        <div class="mb-2">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender" id="male" <? if( $arResult['USER']['PERSONAL_GENDER'] == 'M' ){ echo 'checked'; } ?> value="Мужчина">
                                                <label class="form-check-label" for="male">Мужской</label>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender" id="female" <? if( $arResult['USER']['PERSONAL_GENDER'] == 'F' ){ echo 'checked'; } ?> value="Женщина">
                                                <label class="form-check-label" for="female">Женский</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="birthday" class="form-label">Дата рождения *</label>
                                        <input id="birthday" class="datepicker form-control" type="text" placeholder="Дата рождения" autocomplete="off" name="birthday" value="<?=$arResult['USER']['PERSONAL_BIRTHDAY']?>">
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label for="sfera" class="form-label">Сфера интересов *</label>
                                        <select name="sfera" id="sfera" class="form-select">
                                            <option value="empty">--- Не выбрано ---</option>
                                            <? foreach( $arResult['AREAS_OF_ACTIVITY'] as $key => $act ){ ?>
                                                <option value="<?=$act?>" <? if( $act == $arResult['USER']['UF_AREA_OF_ACTIVITY'] ){ echo 'selected'; } ?>><?=$act?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="course__item">
                            <div class="form-check mb-2">
                                <input id="agree_1" type="checkbox" name="agree_1" class="form-check-input" checked>
                                <label for="agree_1" class="form-check-label">Я прочитал и принял условия конфиденциальности данных и даю согласие на обработку своих персональных данных</label>
                            </div>

                            <p class="error___p" style="margin-top: 15px;"></p>

                            <button type="submit" class="btn btn-primary registerCourseButton to___process">Отправить заявку</button>

                        </div>

                    </form>

                </div>



            <? } else {



                if( $arResult['REGISTER_AVAILABLE']['result_code'] == 'no_places' ){ ?>


                    <h2 class="section__title text-uppercase">Регистрация на <?=$arResult['COURSE_TYPE']?></h2>

                    <p><?=$arResult['COURSE']['NAME']?></p>

                    <p><?=$arResult['weekDay']?> <?=$arResult['DATE_FROM']?></p>

                    <p class="rw-info__desc">Количество свободных мест: <span><?=$arResult['FREE_PLACES_CNT']?></span></p>

                    <div class="course bg-brown-soft">

                        <form id="contactform" action="" method="post">

                            <input type="hidden" name="course_id" value="<?=$arResult['COURSE']['ID']?>">

                            <div class="course__item">

                                <h4>Укажите Ваши данные</h4>

                                <div class="row">

                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                            <label for="email" class="form-label">Email *</label>
                                            <input id="email" type="text" name="email" class="form-control" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="course__item">

                                <p class="error___p" style="margin-top: 15px;"></p>

                                <button type="submit" class="btn btn-primary notyCourseFreePlacesButton to___process">Отправить заявку</button>

                            </div>

                        </form>

                    </div>


                <? } else { ?>


                    <h2 class="section__title text-uppercase">Регистрация невозможна - <?=$arResult['REGISTER_AVAILABLE']['description']?></h2>


                <? }

            } ?>


        </div>
    </section>


<? } else {


    // auth
    $APPLICATION->IncludeComponent(
        "aoptima:auth", "", array()
    );


} ?>