<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $USER->IsAuthorized() ){

    $arResult['AUTH'] = 'Y';
    $arResult['USER'] = tools\user::info( $USER->GetID() );
    $arResult['PERSONAL_PAGES'] = project\personal::pages();


    if( isset( $_POST['stop_ids'] ) ){

        $arResult['REGISTRATIONS_IDS'] = $_SESSION['REGISTRATIONS_IDS'];
        $arResult['REGISTRATION_COURSES_INFO'] = $_SESSION['REGISTRATION_COURSES_INFO'];
        $arResult['REGISTRATION_CERTIFICATES'] = $_SESSION['REGISTRATION_CERTIFICATES'];

        foreach ( $arResult['REGISTRATIONS_IDS'] as $k1 => $id_1 ){
            foreach ( $_POST['stop_ids'] as $k2 => $id_2 ){
                if( $id_1 == $id_2 ){
                    unset( $arResult['REGISTRATIONS_IDS'][ $k1 ] );
                }
            }
        }

    } else {

        $arResult['REGISTRATIONS_IDS'] = [];
        $arResult['REGISTRATION_COURSES_INFO'] = [];
        $arResult['REGISTRATION_CERTIFICATES'] = [];

        // Одобренный Enum
        $moderAcceptedEnum = \AOptima\Tools\prop_enum::getByXmlID('accepted');

        // Получим [ одобренные + прослушал курс ] регистрации пользователя
        $filter = [
            "IBLOCK_ID" => project\learning_course::REG_IBLOCK_ID,
            "PROPERTY_USER" => $USER->GetID(),
            'PROPERTY_MODERATION' => $moderAcceptedEnum['ID'],
            'PROPERTY_STATUS_1' => 1,
        ];
        $fields = [
            "ID", "PROPERTY_USER", "PROPERTY_MODERATION", "PROPERTY_COURSE",
            "PROPERTY_STATUS_1",
            //"PROPERTY_STATUS_2"
        ];
        $sort = [ "SORT" => "ASC" ];
        $registers = \CIBlockElement::GetList(
            $sort, $filter, false, false, $fields
        );
        while ( $register = $registers->GetNext() ){

            $course = tools\el::info( $register['PROPERTY_COURSE_VALUE'] );
            
            $toStamp = MakeTimeStamp($course['PROPERTY_'.project\learning_course::DATE_TO_PROP_CODE.'_VALUE'], "DD.MM.YYYY HH:MI:SS");

            if( $toStamp < time() ){
                
                $arResult['REGISTRATIONS_IDS'][] = $register['ID'];

                $course['PROGRAM'] = tools\el::info( $course['PROPERTY_PROGRAM_VALUE'] );
                $course['CENTER'] = tools\el::info( $course['PROPERTY_CENTER_VALUE'] );
                $course['CENTER']['COUNTRY'] = tools\el::info( $course['CENTER']['PROPERTY_COUNTRY_VALUE'] );

                $arResult['REGISTRATION_COURSES_INFO'][ $register['ID'] ][ $course['ID'] ] = $course;

                // Получим сертификат
                $certificate = project\certificate::getByRegisterID( $register['ID'] );
                if( isset( $certificate ) ){

                    $certificate['NUMBER'] = project\certificate::generateNumber(
                        $register['ID'],
                        $certificate['UF_NUM_PP'],
                        $certificate['UF_YEAR']
                    );

                    $arResult['REGISTRATION_CERTIFICATES'][ $register['ID'] ] = $certificate;
                }
            }
        }

        $_SESSION['REGISTRATIONS_IDS'] = $arResult['REGISTRATIONS_IDS'];
        $_SESSION['REGISTRATION_COURSES_INFO'] = $arResult['REGISTRATION_COURSES_INFO'];
        $_SESSION['REGISTRATION_CERTIFICATES'] = $arResult['REGISTRATION_CERTIFICATES'];
    }

    if( count( $arResult['REGISTRATIONS_IDS'] ) == 0 ){
        $arResult['REGISTRATIONS_IDS'] = [ 0 ];
    }



    

}



$this->IncludeComponentTemplate();



