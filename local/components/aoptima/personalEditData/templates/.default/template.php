<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

if( $arResult['AUTH'] == 'Y' ){ ?>


    <section class="section">
        <div class="container">

            <h2 class="section__title text-uppercase">Личный кабинет студента</h2>

            <div class="btn-group">
                <? foreach( $arResult['PERSONAL_PAGES'] as $page ){ ?>
                    <a <? if( $page['active'] != 'Y' ){ ?>href="<?=$page['url']?>"<? } ?> class="btn <? if( $page['active'] == 'Y' ){
                        echo 'btn-secondary';
                    } else {
                        echo 'btn-primary';
                    } ?>"><?=$page['name']?></a>
                <? } ?>
            </div>

            <div class="form bg-brown-soft px-4 py-5">
                <div class="row">

                    <div class="col-lg-6">

                        <h3>Личные данные</h3>

                        <form onsubmit="return false;">
                            
                            <div class="mb-3">
                                <label for="LAST_NAME" class="form-label">Фамилия</label>
                                <input id="LAST_NAME" type="text" name="LAST_NAME" class="form-control" value="<?=$arResult['USER']['LAST_NAME']?>">
                            </div>
                            
                            <div class="mb-3">
                                <label for="NAME" class="form-label">Имя</label>
                                <input id="NAME" type="text" name="NAME" class="form-control" value="<?=$arResult['USER']['NAME']?>">
                            </div>
                            
                            <div class="mb-3">
                                <label for="SECOND_NAME" class="form-label">Отчество</label>
                                <input id="SECOND_NAME" type="text" name="SECOND_NAME" class="form-control" value="<?=$arResult['USER']['SECOND_NAME']?>">
                            </div>
                            
                            <div class="mb-3">
                                <label for="PERSONAL_BIRTHDAY" class="form-label">Дата рождения</label>
                                <input id="PERSONAL_BIRTHDAY" type="text" name="PERSONAL_BIRTHDAY" class="datepicker form-control" value="<?=$arResult['USER']['PERSONAL_BIRTHDAY']?>" readonly>
                            </div>

                            <div class="mb-3">
                                <label for="PERSONAL_GENDER" class="form-label">Пол</label>
                                <select name="PERSONAL_GENDER" id="PERSONAL_GENDER" class="form-select">
                                    <option value="empty">--- Не выбрано ---</option>
                                    <option value="M" <? if( $arResult['USER']['PERSONAL_GENDER'] == 'M' ){ echo 'selected'; } ?>>Мужской</option>
                                    <option value="F" <? if( $arResult['USER']['PERSONAL_GENDER'] == 'F' ){ echo 'selected'; } ?>>Женский</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="PERSONAL_PHONE" class="form-label">Телефон</label>
                                <input id="PERSONAL_PHONE" type="text" name="PERSONAL_PHONE" class="form-control" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
                            </div>

                            <div class="mb-3">
                                <label for="PERSONAL_CITY" class="form-label">Город</label>
                                <input id="PERSONAL_CITY" type="text" name="PERSONAL_CITY" class="form-control" value="<?=$arResult['USER']['PERSONAL_CITY']?>">
                            </div>

                            <div class="mb-3">
                                <label for="UF_AREA_OF_ACTIVITY" class="form-label">Сфера деятельности</label>
                                <select name="UF_AREA_OF_ACTIVITY" id="UF_AREA_OF_ACTIVITY" class="form-select">
                                    <option value="empty">--- Не выбрано ---</option>
                                    <? foreach( $arResult['AREAS_OF_ACTIVITY'] as $item ){ ?>
                                        <option value="<?=$item?>" <? if( $arResult['USER']['UF_AREA_OF_ACTIVITY'] == $item ){ echo 'selected'; } ?>><?=$item?></option>
                                    <? } ?>
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="WORK_COMPANY" class="form-label">Место работы</label>
                                <input id="WORK_COMPANY" type="text" name="WORK_COMPANY" class="form-control" value="<?=$arResult['USER']['WORK_COMPANY']?>">
                            </div>

                            <div class="mb-3">
                                <label for="<?=$arResult['USER']['WORK_POSITION']?>" class="form-label">Должность</label>
                                <input id="<?=$arResult['USER']['WORK_POSITION']?>" type="text" name="WORK_POSITION" value="<?=$arResult['USER']['WORK_POSITION']?>" class="form-control">
                            </div>

                            <div class="mb-3">
                                <label for="UF_HOW_DID_YOU_KNOW" class="form-label">Откуда узнали об обучении</label>
                                <select name="UF_HOW_DID_YOU_KNOW" id="UF_HOW_DID_YOU_KNOW" class="form-select">
                                    <option value="empty">--- Не выбрано ---</option>
                                    <? foreach( $arResult['HOW_DID_YOU_KNOW_LIST'] as $item ){ ?>
                                        <option value="<?=$item?>" <? if( $arResult['USER']['UF_HOW_DID_YOU_KNOW'] == $item ){ echo 'selected'; } ?>><?=$item?></option>
                                    <? } ?>
                                </select>
                            </div>

                            <p class="error___p"></p>

                            <div class="mt-4">
                                <button type="button" class="btn btn-primary personalEditDataSave to___process">Сохранить</button>
                            </div>

                        </form>

                        <hr>

                        <h3>Смена пароля</h3>

                        <form onsubmit="return false;">

                            <div class="mb-3">
                                <label for="OLD_PASSWORD" class="form-label">Текущий пароль</label>
                                <input id="OLD_PASSWORD" type="password" name="OLD_PASSWORD" class="form-control">
                            </div>

                            <div class="mb-3">
                                <label for="PASSWORD" class="form-label">Новый пароль</label>
                                <input id="PASSWORD" type="password" name="PASSWORD" class="form-control">
                            </div>

                            <div class="mb-3">
                                <label for="CONFIRM_PASSWORD" class="form-label">Повторить пароль</label>
                                <input id="CONFIRM_PASSWORD" type="password" name="CONFIRM_PASSWORD" class="form-control">
                            </div>

                            <p class="error___p"></p>

                            <div class="mt-4">
                                <button type="button" class="btn btn-primary personalEditPasswordSave to___process">Сохранить</button>
                            </div>

                        </form>

                    </div>

                    <div class="col-lg-6">

                        <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/includes/personalContacts.php'; ?>

                    </div>

                </div>
            </div>

        </div>
    </section>


<? } else {


    // auth
    $APPLICATION->IncludeComponent(
        "aoptima:auth", "", array()
    );


} ?>