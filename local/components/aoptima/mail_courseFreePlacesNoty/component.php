<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['COURSE_ID'] = $arParams['course_id'];

$arResult['COURSE'] = tools\el::info( $arResult['COURSE_ID'] );


if( intval( $arResult['COURSE']['ID'] ) > 0 ){



    $this->IncludeComponentTemplate();
}




