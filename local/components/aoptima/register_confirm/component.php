<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){
    LocalRedirect( '/' );
}

$arResult['CONFIRM_CODE'] = trim(strip_tags($_GET['code']));

if( strlen( $arResult['CONFIRM_CODE'] ) > 0 ){

    $arResult['CONFIRM_ERROR'] = null;
    $arResult['CONFIRM_SUCCESS'] = false;

    $filter = [ "UF_REGISTER_CONFIRM_CODE" => $arResult['CONFIRM_CODE'] ];
    $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
    if( $user = $rsUsers->GetNext() ){

        $obUser = new \CUser;
        $result = $obUser->Update( $user['ID'], [
            'ACTIVE' => 'Y',
            'UF_REGISTER_CONFIRM_CODE' => null
        ]);

        if( $result ){

            // Регистрационная информация на Email
            \AOptima\Tools\user::sendRegInfo( $userID );

            $arResult['CONFIRM_SUCCESS'] = true;

        } else {

            $arResult['CONFIRM_ERROR'] = 'Ошибка подтверждения регистрации';
        }

    } else {

        $arResult['CONFIRM_ERROR'] = 'Ссылка ошибочна либо устарела';
    }

} else {

    LocalRedirect( '/' );
}



$this->IncludeComponentTemplate();