<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<section class="section">
    <div class="container">

        <h2 class="section__title text-uppercase">Регистрация</h2>

        <? if( isset( $arResult['CONFIRM_ERROR'] ) ){ ?>

            <p class="error___p"><?=$arResult['CONFIRM_ERROR']?></p>

        <? } else { ?>

            <p class="success___p">Регистрация успешно подтверждена!</p>
            <p class="success___p">Теперь вы можете <a href="/auth/">Авторизоваться</a></p>

        <? } ?>

    </div>
</section>

