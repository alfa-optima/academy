<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
/** @var array $arResult */ ?>

<section class="section coursesFilterBlock">

    <h2 class="section__title text-uppercase text-center">Поиск курсов обучения</h2>

    <div class="form bg-brown-soft px-4">

        <form onsubmit="return false;" class="coursesFilterForm">

            <div class="row">

                <div class="form__item col-md-6 col-lg-3">
                    <div class="py-5">

                        <? if( count( $arResult['learn_types'] ) > 0 ){ ?>

                            <div class="mb-3">
                                <label for="learn_type" class="form-label">Форма обучения</label>
                                <select name="learn_type" id="learn_type" class="form-select">
                                    <option value="empty">--- Не выбрано ---</option>
                                    <? foreach( $arResult['learn_types'] as $learn_type ){ ?>
                                        <option value="<?=$learn_type['ID']?>" <? if( $learn_type['ID'] == $_GET['learn_type'] ){ echo 'selected'; } ?>><?=$learn_type['NAME']?></option>
                                    <? } ?>
                                </select>
                            </div>

                        <? } ?>

                        <? if( count( $arResult['all_programs'] ) > 0 ){ ?>
                            <div class="mb-3">
                                <label for="program" class="form-label">Программа</label>
                                <select name="program" id="program" class="form-select">
                                    <option value="empty">--- Не выбрано ---</option>
                                    <? foreach( $arResult['all_programs'] as $program ){ ?>
                                        <option value="<?=$program['ID']?>" <? if( $_GET['program'] == $program['ID'] ){ echo 'selected'; } ?>><?=$program['NAME']?></option>
                                    <? } ?>
                                </select>
                            </div>
                        <? } ?>

                    </div>
                </div>

                <div class="form__item col-md-6 col-lg-3">
                    <div class="py-5">
                        <div class="mb-3">
                            <label for="program" class="form-label">Дата начала</label>
                            <input class="datepicker form-control" type="text" id="date_from" name="min_date" value="<?=strip_tags(trim($_GET['min_date']))?>" placeholder="Дата начала" readonly="readonly">
                        </div>
                        <div class="mb-3">
                            <label for="program" class="form-label">Дата окончания</label>
                            <input class="datepicker form-control" type="text" id="date_to" name="max_date" value="<?=strip_tags(trim($_GET['max_date']))?>" placeholder="Дата окончания" readonly="readonly">
                        </div>
                    </div>
                </div>

                <? if( count( $arResult['countries'] ) > 0 || count( $arResult['cities'] ) > 0 ){ ?>
                    <div class="form__item col-md-6 col-lg-3">
                        <div class="py-5">
                            <? if( count( $arResult['countries'] ) > 0 ){ ?>
                                <div class="mb-3">
                                    <label for="country" class="form-label">Страна</label>
                                    <select name="country" id="country" class="form-select">
                                        <option value="empty">--- Не выбрано ---</option>
                                        <? foreach( $arResult['countries'] as $country ){ ?>
                                            <option value="<?=$country['CODE']?>" <? if( $arResult['active_country']['CODE'] == $country['CODE'] ){ echo 'selected'; } ?>><?=$country['NAME']?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            <? } ?>
                            <? if( count( $arResult['cities'] ) > 0 ){ ?>
                                <div class="mb-3">
                                    <label for="city" class="form-label">Город</label>
                                    <select name="city" id="city" class="form-select">
                                        <option value="empty">--- Не выбрано ---</option>
                                        <? foreach( $arResult['cities'] as $city ){ ?>
                                            <option value="<?=trim(strip_tags($city))?>" <? if( mb_strtolower($arResult['active_city']) == trim(strip_tags(mb_strtolower($city))) ){ echo 'selected'; } ?>><?=trim(strip_tags($city))?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                <? } ?>

                <div class="form__item col-md-6 col-lg-3">
                    <div class="py-5">
                        <div class="mb-3">
                            <label for="search" class="form-label">Искать курсы обучения</label>
                            <div class="input-group">
                                <input id="search" type="text" name="q" class="form-control" value="<?=trim(strip_tags($_GET['q']))?>" placeholder="Поиск">
                                <button class="btn btn-primary btn-sm" type="submit">Найти</button>
                            </div>
                        </div>
                        <a style="cursor: pointer" class="arrow-link resetCoursesFilterButton to___process">Сбросить фильтр</a>
                    </div>
                </div>

            </div>

        </form>

    </div>

</section>
