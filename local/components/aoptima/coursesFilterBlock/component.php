<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['all_programs'] = project\program::all_items();
$arResult['all_countries'] = project\country::all_items();
$arResult['centers'] = project\center::all_items();
$arResult['learn_types'] = project\learning_type::all_items();

$arResult['countries'] = project\center::countries();
$arResult['active_country'] = null;
if( isset( $arParams['arFields'] ) ){   $_GET = $arParams['arFields'];   }
if( isset($_GET['country']) && strlen($_GET['country']) > 0 ){
    $country_code = trim(strip_tags( $_GET['country'] ));
    if( strlen($country_code) > 0 && $country_code != 'empty' ){
        $country = \AOptima\Tools\el::info_by_code( $country_code, project\country::IBLOCK_ID );
        if( intval( $country['ID'] ) > 0 ){
            $arResult['active_country'] = $country;
        }
    }
}

if( isset( $arResult['active_country'] ) ){
    $arResult['cities'] = project\center::cities( $arResult['active_country']['ID'] );
} else {
    $arResult['cities'] = project\center::cities();
}
$arResult['active_city'] = null;
if( isset($_GET['city']) && strlen($_GET['city']) > 0 ){
    $city = trim(strip_tags( $_GET['city'] ));
    if( strlen($city) > 0 && $city != 'empty' ){
        $arResult['active_city'] = $city;
    }
}







$this->IncludeComponentTemplate();