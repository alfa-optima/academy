<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<html>
	<head>

        <style>

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0px;
        }
        .certificate {
            font-family: 'DejaVu Sans';
            font-size: 85%;
        }
        .certificate__wrapper {
            padding: 0;
        }
        .certificate__logo {
            width: 54%;
        }
        .certificate__field-text {
            margin-bottom: 5px;
        }
        .certificate__field-name {
            padding-right: 5px;
            color: #7f7f7f;
        }
        .certificate__field-input {
            width: auto;
            max-width: 100%;
            border: 0;
            border-bottom: 1px solid #7f7f7f;
        }

        .certificate__field-p {
            width: auto;
            max-width: 100%;
            border: 0;
            border-bottom: 1px solid #7f7f7f;
            display: inline-block;
            margin:0;
        }

        .certificate__field-textarea {
            font-family: 'DejaVu Sans';
            width: 100%;
            max-width: 100%;
            margin-right: 0;
            margin-left: 0;
            padding-top: 5px;
            padding-right: 0;
            padding-bottom: 12px;
            padding-left: 0;
            resize: none;
            color: #7f7f7f;
            border: none;
            background-image: url(<?=$arResult['BG']?>);
        }
        .certificate__form {
            padding: 10px;
        }
        .certificate__form table {
            width: 100%;
        }
        .certificate__form .certificate__field-text {
            display: inline-block;
            width: 100%;
            margin-bottom: 5px;
        }
        .certificate__form .certificate__field-name {
            width: 14%;
        }
        .certificate__form .certificate__field-input, .certificate__form .certificate__field-p {
            width: 82%;
        }

        .certificate__footer-td .certificate__field-col {
            text-align: center;
        }

        </style>

    </head>

	<body>


        <div class="certificate">
            <div class="certificate__wrapper">

                <div></div>

                <table>
                    <tbody>
                        <tr>
                            <td style="width: 50%">
                                <img class="certificate__logo" src="<?=$arResult['LOGO_1_SRC']?>">
                            </td>
                            <td style="width: 50%; text-align: right;">
                                <img class="certificate__logo" src="<?=$arResult['LOGO_2_SRC']?>">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table>
                    <tbody>

                        <? if( count( $arResult['HEAD_IMAGES'] ) > 0 ){ ?>

                            <tr>

                                <? $cnt = 0;
                                foreach( $arResult['HEAD_IMAGES'] as $src ){ $cnt++;
                                    $colspan = '';
                                    if(
                                        count( $arResult['HEAD_IMAGES'] ) < 3
                                        &&
                                        $cnt == count( $arResult['HEAD_IMAGES'] )
                                    ){
                                        if( count( $arResult['HEAD_IMAGES'] ) == 2 ){
                                            $colspan = 'colspan="2"';
                                        } else if( count( $arResult['HEAD_IMAGES'] ) == 1 ){
                                            $colspan = 'colspan="3"';
                                        }
                                    } ?>
                                    <td <?=$colspan?> style="width: <?=floor( 1 / count($arResult['HEAD_IMAGES']) * 100 )?>%">
                                        <img width="100%" class="certificate__img" src="<?=$src?>">
                                    </td>
                                <? } ?>

                            </tr>

                        <? } ?>

                        <tr>

                            <td style="width: 67%;" colspan="2">
                                <div style="width: 100%; max-width: 100%;">
                                    <img style="width: 100%;" class="certificate__title" src="<?=$arResult['IMG_4_SRC']?>">
                                </div>
                            </td>

                            <td style="background-color: #e2eff5; width: 33%;">
                                <div style="padding-left: 10px; padding-right: 10px;">

                                    <label class="certificate__field-text">

                                        <span class="certificate__field-name">№</span>

                                        <p class="certificate__field-p" style="background-color: #e2eff5;width: 100px;"><?=$arParams['certificateNumber']?></p>

                                    </label>

                                    <br><br>

                                    <label class="certificate__field-text">

                                        <span class="certificate__field-name">Выдан</span>

                                        <p class="certificate__field-p" style="background-color: #e2eff5; width: 100px;"><?=$arParams['certificateDate']?></p>

                                    </label>

                                </div>
                            </td>

                        </tr>

                    </tbody>
                </table>

                <div class="certificate__form">
                    <table>
                        <tbody>

                            <tr>
                                <td>
                                    <label class="certificate__field-text  certificate__field-text--col">

                                        <span class="certificate__field-name" style="width: 100%; color: #2ebbf0; display: block;">Учебный центр КНАУФ</span>

                                        <textarea rows="2" class="certificate__field-textarea"><?=html_entity_decode($arResult['CENTER']['NAME'])?></textarea>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text">

                                        <span style=" display: inline-block;" class="certificate__field-name">Город</span>

                                        <p class="certificate__field-p"><?=html_entity_decode($arResult['CENTER']['PROPERTY_CITY_VALUE'])?></p>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text">
                                        <span style="width: 100%; display: inline-block;" class="certificate__field-name">Настоящий сертификат КНАУФ подтверждает, что</span>
                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text">

                                        <span style=" display: inline-block;" class="certificate__field-name">Фамилия</span>

                                        <p class="certificate__field-p"><?=html_entity_decode($arResult['REGISTER']['PROPERTY_F_VALUE'])?></p>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text">

                                        <span style=" display: inline-block;" class="certificate__field-name">Имя</span>

                                        <p class="certificate__field-p"><?=html_entity_decode($arResult['REGISTER']['PROPERTY_I_VALUE'])?></p>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text">

                                        <span style=" display: inline-block;" class="certificate__field-name">Отчество</span>

                                        <p class="certificate__field-p"><?=html_entity_decode($arResult['REGISTER']['PROPERTY_O_VALUE'])?></p>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text certificate__field-text--start">

                                        <span style=" display: inline-block;" class="certificate__field-name">В период с</span>

                                        <input style="width: 100px; margin-right: 10px;" class="certificate__field-input" type="text" value="<?=ConvertDateTime($arResult['COURSE']['PROPERTY_DATE_FROM_VALUE'], "DD.MM.YYYY", "ru")?>">

                                        <span style="width: auto; display: inline-block;" class="certificate__field-name">По</span>

                                        <input style="width: 100px" class="certificate__field-input" type="text" value="<?=ConvertDateTime($arResult['COURSE']['PROPERTY_DATE_TO_VALUE'], "DD.MM.YYYY", "ru")?>">

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text certificate__field-text--start">

                                        <input type="checkbox" name="name" value="value" <? if( $arResult['REGISTER']['PROPERTY_STATUS_1_VALUE'] || $arResult['REGISTER']['PROPERTY_STATUS_2_VALUE'] ){ echo 'checked'; } ?>>

                                        <? if(
                                            $arResult['REGISTER']['PROPERTY_STATUS_1_VALUE']
                                            &&
                                            $arResult['REGISTER']['PROPERTY_STATUS_2_VALUE']
                                        ){
                                            $checkbox_text = 'Прослушал(а) программу и успешно сдал(а) тестирование';
                                        } else if( $arResult['REGISTER']['PROPERTY_STATUS_1_VALUE'] ){
                                            $checkbox_text = 'Прослушал(а) программу';
                                        } else if( $arResult['REGISTER']['PROPERTY_STATUS_2_VALUE'] ){
                                            $checkbox_text = 'Успешно сдал(а) тестирование';
                                        } ?>

                                        <span style=" display: inline-block; width: auto; margin-left: 10px; padding-bottom: 4px;" class="certificate__field-name"><?=$checkbox_text?></span>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text  certificate__field-text--col">

                                        <span class="certificate__field-name" style="width: 100%; display: block;">Программа</span>

                                        <textarea rows="2" class="certificate__field-textarea"><?=html_entity_decode($arResult['PROGRAM']['NAME'])?></textarea>

                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="certificate__field-text certificate__field-text--start">

                                        <span style=" display: inline-block; width: auto;" class="certificate__field-name">Теоретических часов</span>

                                        <p class="certificate__field-p" style="width: 100px;"><?
                                        if( isset($arResult['COURSE']['PROPERTY_TEOR_HOURS_VALUE']) ){
                                            echo $arResult['COURSE']['PROPERTY_TEOR_HOURS_VALUE'];
                                        } else {
                                            echo $arResult['PROGRAM']['PROPERTY_TEOR_HOURS_VALUE'];
                                        }
                                        ?></p>

                                        <span style="width: auto; display: inline-block;" class="certificate__field-name">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Практических часов</span>

                                        <p class="certificate__field-p" style="width: 100px;"><?
                                        if( isset($arResult['COURSE']['PROPERTY_PRACT_HOURS_VALUE']) ){
                                            echo $arResult['COURSE']['PROPERTY_PRACT_HOURS_VALUE'];
                                        } else {
                                            echo $arResult['PROGRAM']['PROPERTY_PRACT_HOURS_VALUE'];
                                        }
                                        ?></p>

                                    </label>
                                </td>
                            </tr>

                        </tbody>
                    </table>

                    <table>
                        <tbody>
                            <tr style='vertical-align: bottom;'>

                                <td class="certificate__footer-td" style="width: 33%; padding-top: 40px;">
                                    <label class="certificate__field-text  certificate__field-text--col">

                                        <p class="certificate__field-p">Директор</p>

                                        <span style=" display: block; width: 100%; text-align: center;" class="certificate__field-name">Должность</span>

                                    </label>
                                </td>

                                <td class="certificate__footer-td" style="width: 33%; padding-top: 40px;">
                                    <label class="certificate__field-text  certificate__field-text--col">

                                        <p class="certificate__field-p"><?=html_entity_decode($arResult['CENTER']['PROPERTY_RUKOVODITEL_VALUE'])?></p>

                                        <span style=" display: block; width: 100%; text-align: center;" class="certificate__field-name">Ф.И.О</span>

                                    </label>
                                </td>

                                <td class="certificate__footer-td" style="width: 33%; padding-top: 40px;">
                                    <label class="certificate__field-text  certificate__field-text--col">

                                        <input class="certificate__field-input" type="text">

                                        <span style=" display: block; width: 100%; text-align: center;" class="certificate__field-name">Подпись</span>

                                    </label>
                                </td>

                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </body>
</html>