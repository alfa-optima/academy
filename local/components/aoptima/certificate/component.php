<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$s = tools\config::getValue('SCHEME');
$d = tools\config::getValue('DOMAIN');
$img_path = $s.'://'.$d.'/local/components/aoptima/certificate/templates/.default/images';

$arResult['BG'] = $img_path.'/textarea-bcg6.png';
$arResult['LOGO_1_SRC'] = $img_path.'/logo-academy.png';
$arResult['LOGO_2_SRC'] = $img_path.'/knauf-logo.png';
$arResult['IMG_4_SRC'] = $img_path.'/cert-title.png';

$arResult['REGISTER_ID'] = intval( $arParams['REGISTER_ID'] );
if( intval( $arResult['REGISTER_ID'] ) > 0 ){
    $arResult['REGISTER'] = tools\el::info( intval( $arResult['REGISTER_ID'] ) );
    if( intval($arResult['REGISTER']['ID']) > 0 ){
        $arResult['COURSE'] = tools\el::info( $arResult['REGISTER']['PROPERTY_COURSE_VALUE'] );
        if( intval($arResult['COURSE']['ID']) > 0 ){

            $arResult['PROGRAM'] = tools\el::info( $arResult['COURSE']['PROPERTY_PROGRAM_VALUE'] );
            $arResult['CENTER'] = tools\el::info( $arResult['COURSE']['PROPERTY_CENTER_VALUE'] );

            if(
                intval( $arResult['PROGRAM']['ID'] ) > 0
                &&
                intval( $arResult['CENTER']['ID'] ) > 0
            ){

                $arResult['HEAD_IMAGES'] = [];
                if( is_array( $arResult['PROGRAM']['PROPERTY_CERT_IMAGES_VALUE'] ) ){
                    foreach ( $arResult['PROGRAM']['PROPERTY_CERT_IMAGES_VALUE'] as $key => $pic_id ){
                        $src = tools\funcs::rIMG( $pic_id, 5, (460 * 3 / count( $arResult['PROGRAM']['PROPERTY_CERT_IMAGES_VALUE'] )), 329 );
                        $arResult['HEAD_IMAGES'][] = $s.'://'.$d.$src;
                    }
                }
                $arResult['HEAD_IMAGES'] = array_slice( $arResult['HEAD_IMAGES'], 0, 3 );

                $this->IncludeComponentTemplate();
            }
        }
    }
}




