<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<section class="section recoveryArea">
    <div class="container">

        <h2 class="section__title text-uppercase">Восстановление пароля</h2>

        <div class="form bg-brown-soft px-4 py-5">
            <div class="row">

                <div class="col-lg-6">

                    <? if( $arParams['SUCCESS_SENT'] == 'Y' ){ ?>

                        <p class="success___p">Ссылка для восстановления пароля отправлена на указанный вами Email<br>Перейдите по ней для установки нового пароля</p>

                    <? } else if( $arParams['SUCCESS_SAVED'] == 'Y' ){ ?>

                        <p class="success___p">Пароль успешно изменён!</p>
                        <p class="success___p">Теперь вы можете <a  href="/auth/">Авторизоваться</a></p>

                    <? } else { ?>

                        <? if( $arResult['IS_AUTH'] == 'N' ){ ?>

                            <? if( strlen( $arResult['RECOVERY_CODE'] ) > 0 ){ ?>

                                <? if( isset( $arResult['RECOVERY_ERROR'] ) ){ ?>

                                    <p class="error___p"><?=$arResult['RECOVERY_ERROR']?></p>

                                <? } else { ?>

                                    <h4>Теперь вы можете ввести новый пароль</h4>

                                    <form onsubmit="return false;">

                                        <input type="hidden" name="recovery_code" value="<?=$arResult['RECOVERY_CODE']?>">

                                        <div class="mb-3">
                                            <label class="form-label" for="password">Новый пароль</label>
                                            <input class="form-control" id="password" type="password" name="password" placeholder="Новый пароль">
                                        </div>

                                        <div class="mb-3">
                                            <label for="password_confirm" class="form-label">Повтор пароля</label>
                                            <input class="form-control" id="password_confirm" type="password" name="password_confirm" placeholder="Повтор пароля">
                                        </div>

                                        <p class="error___p"></p>

                                        <div class="mt-4">
                                            <button type="button" class="btn btn-primary passwordRecoverySaveButton to___process">Сохранить</button>
                                        </div>

                                    </form>

                                <? } ?>

                            <? } else { ?>

                                <p>Введите ваш Email и мы отправим вам ссылку для восстановления пароля</p>

                                <form onsubmit="return false;">

                                    <div class="mb-3">
                                        <label for="email" class="form-label">Имя</label>
                                        <input id="email" class="form-control" type="email" name="email" placeholder="Email">
                                    </div>

                                    <p class="error___p"></p>

                                    <div class="mt-4">
                                        <button type="button" class="btn btn-primary passwordRecoveryRequestButton to___process">Сохранить</button>
                                    </div>

                                </form>

                            <? } ?>

                        <? }

                    } ?>

                </div>

                <div class="col-lg-6">

                    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/includes/personalContacts.php'; ?>

                </div>

            </div>

        </div>

    </div>
</section>

