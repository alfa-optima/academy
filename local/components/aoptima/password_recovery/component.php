<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

global $USER;
$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){
    LocalRedirect( '/personal/' );
}

$arResult['RECOVERY_CODE'] = trim(strip_tags($_GET['code']));

if( strlen( $arResult['RECOVERY_CODE'] ) > 0 ){

    $filter = [ "UF_PASSWORD_RECOVERY_CODE" => $arResult['RECOVERY_CODE'] ];
    $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
    if( $user = $rsUsers->GetNext() ){

        $arResult['RECOVERY_ERROR'] = null;

    } else {

        $arResult['RECOVERY_ERROR'] = 'Ссылка ошибочна либо устарела';
    }
}




$this->IncludeComponentTemplate();


