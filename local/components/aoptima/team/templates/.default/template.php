<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


if( $arParams['IS_AJAX'] != 'Y' ){ ?>


    <section class="section">
        <div class="container">

            <? if( count( $arResult['COUNTRIES'] ) > 0 ){ ?>

                <div class="btn-group">

                    <? foreach( $arResult['COUNTRIES'] as $key => $country ){ ?>

                        <a style="cursor: pointer" class="btn btn-primary team_country_button to___process <? if( $country['ID'] == $arResult['ACTIVE_COUNTRY']['ID'] ){ echo 'active'; } ?>" item_id="<?=$country['ID']?>"><?=$country['NAME']?></a>

                    <? } ?>

                </div>

            <? } ?>

            <div class="team bg-brown-soft teamLoadArea">

                <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

                    <div class="team__item px-3 py-4">

                        <? if( intval( $arItem['CENTER']['ID'] ) > 0 ){ ?>
                            <h4 class="mb-4"><?=$arItem['CENTER']['PROPERTY_COMPANY_VALUE']?$arItem['CENTER']['PROPERTY_COMPANY_VALUE']:$arItem['CENTER']['NAME']?></h4>
                        <? } ?>

                        <div class="row g-4">

                            <div class="col-lg-6">
                                <img class="img-fluid" src="<?=tools\funcs::rIMGG( $arItem['PREVIEW_PICTURE'], 4, 418, 313 )?>">
                            </div>

                            <div class="col-lg-6">
                                <div class="section__text">

                                    <h5 class="mb-3"><?=$arItem['NAME']?></h5>

                                    <?=$arItem['PREVIEW_TEXT']?>

                                </div>
                            </div>
                        </div>
                    </div>

                <? } ?>

            </div>
        </div>
    </section>


<? } else {


    foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

        <div class="team__item px-3 py-4">

            <? if( intval( $arItem['CENTER']['ID'] ) > 0 ){ ?>
                <h4 class="mb-4"><?=$arItem['CENTER']['NAME']?></h4>
            <? } ?>

            <div class="row g-4">

                <div class="col-lg-6">
                    <img class="img-fluid" src="<?=tools\funcs::rIMGG( $arItem['PREVIEW_PICTURE'], 4, 418, 313 )?>">
                </div>

                <div class="col-lg-6">
                    <div class="section__text">

                        <h5 class="mb-3"><?=$arItem['NAME']?></h5>

                        <?=$arItem['PREVIEW_TEXT']?>

                    </div>
                </div>
            </div>
        </div>

    <? }


} ?>
