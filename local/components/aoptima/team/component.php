<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['COUNTRIES'] = [];
$arResult['COUNTRIES_IDS'] = [];

$arResult['ITEMS'] = project\team_member::all_items();

foreach ( $arResult['ITEMS'] as $key => $arItem ){

    $arItem['CENTER'] = null;
    if( intval( $arItem['PROPERTY_CENTER_VALUE'] ) > 0 ){
        $arItem['CENTER'] = tools\el::info( $arItem['PROPERTY_CENTER_VALUE'] );
        if(
            intval( $arItem['CENTER']['ID'] ) > 0
            &&
            intval( $arItem['CENTER']['PROPERTY_COUNTRY_VALUE'] ) > 0
        ){
            $arResult['COUNTRIES_IDS'][] = $arItem['CENTER']['PROPERTY_COUNTRY_VALUE'];
        }
    }

    if( intval( $_REQUEST['country'] ) > 0 ){
        if( $_REQUEST['country'] == $arItem['CENTER']['PROPERTY_COUNTRY_VALUE'] ){
            $arResult['ITEMS'][ $key ] = $arItem;
        } else {
            unset( $arResult['ITEMS'][ $key ] );
        }
    } else {
        $arResult['ITEMS'][ $key ] = $arItem;
    }
}

$arResult['COUNTRIES_IDS'] = array_unique( $arResult['COUNTRIES_IDS'] );
foreach ( $arResult['COUNTRIES_IDS'] as $key => $id ){
    $country = tools\el::info( $id );
    if( intval( $country['ID'] ) > 0 && $country['NAME'] == 'Россия' ){
        $arResult['COUNTRIES'][] = $country;
    }
}
$otherCountries = [];
foreach ( $arResult['COUNTRIES_IDS'] as $key => $id ){
    $country = tools\el::info( $id );
    if( intval( $country['ID'] ) > 0 && $country['NAME'] != 'Россия' ){
        $otherCountries[ $country['NAME'] ] = $country;
    }
}
ksort($otherCountries);
$arResult['COUNTRIES'] = array_merge( $arResult['COUNTRIES'], $otherCountries );


$arResult['ACTIVE_COUNTRY'] = null;
if( count( $arResult['COUNTRIES'] ) > 0 ){
    $arResult['ACTIVE_COUNTRY'] = $arResult['COUNTRIES'][ array_keys($arResult['COUNTRIES'])[0] ];
}
if( intval($_REQUEST['country']) > 0 ){
    foreach ( $arResult['COUNTRIES'] as $key => $country ){
        if( $country['ID'] == intval($_REQUEST['country']) ){
            $arResult['ACTIVE_COUNTRY'] = $country;
        }
    }
}





$this->IncludeComponentTemplate();