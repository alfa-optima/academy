<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arResult['IS_AUTH'] = $USER->IsAuthorized()?"Y":"N";

if( $arResult['IS_AUTH'] == 'Y' ){
    LocalRedirect( '/personal/' );
}





$this->IncludeComponentTemplate();