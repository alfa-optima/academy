<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<section class="section">
    <div class="container">

        <h2 class="section__title text-uppercase">Регистрация</h2>

        <div class="btn-group">
            <a href="/auth/" class="btn btn-secondary">Вход</a>
            <a class="btn btn-primary">Регистрация</a>
        </div>

        <div class="form bg-brown-soft px-4 py-5">
            <div class="row">

                <div class="col-lg-6">

                    <form onsubmit="return false;">

                        <div class="mb-3">
                            <label for="email" class="form-label">E-mail</label>
                            <input id="email" type="email" name="email" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="f" class="form-label">Фамилия</label>
                            <input id="f" type="text" name="f" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="i" class="form-label">Имя</label>
                            <input id="i" type="text" name="i" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="o" class="form-label">Отчество</label>
                            <input id="o" type="text" name="o" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="password" class="form-label">Пароль</label>
                            <input id="password" type="password" name="password" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="confirm_password" class="form-label">Повторить пароль</label>
                            <input id="confirm_password" type="password" name="confirm_password" class="form-control">
                        </div>

                        <div class="form-check mb-3">
                            <input id="userAgreement" type="checkbox" name="userAgreement" class="form-check-input" value="Y">
                            <label for="userAgreement" class="form-check-label">Я принимаю условия <a href="/user_agreement/" target="_blank">Пользовательского соглашения</a> и даю своё согласие на обработку моей персональной информации на условиях, определенных Политикой конфиденциальности</label>
                        </div>

                        <? if( 0 ){ ?>
                            <div class="form-check mb-4">
                                <input id="info" type="checkbox" name="info" class="form-check-input">
                                <label for="info" class="form-check-label">Да, я хотел бы получать информационные материалы</label>
                            </div>
                        <? } ?>

                        <p class="error___p"></p>

                        <button type="button" class="btn btn-primary registerButton to___process">Зарегистироваться</button>

                    </form>

                </div>

                <div class="col-lg-6">

                    <? //include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/includes/personalContacts.php'; ?>

                </div>

            </div>
        </div>
    </div>
</section>
