<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<section class="section">
    <div class="container">

        <h2 class="section__title text-uppercase">Авторизация</h2>

        <div class="btn-group">
            <a class="btn btn-secondary">Вход</a>
            <a href="/registration/" class="btn btn-primary">Регистрация</a>
        </div>

        <div class="form bg-brown-soft px-4 py-5">
            <div class="row">

                <div class="col-lg-6">

                    <form onsubmit="return false;">

                        <div class="mb-3">
                            <label for="login" class="form-label">E-mail / Логин</label>
                            <input id="login" type="text" name="login" class="form-control">
                        </div>

                        <div class="mb-3">
                            <label for="pasw" class="form-label d-flex justify-content-between">Пароль <a href="/password_recovery/">Забыли пароль?</a></label>
                            <input id="pasw" type="password" name="password" class="form-control">
                        </div>

                        <? if( 0 ){ ?>
                            <div class="form-check mb-4">
                                <input id="info" type="checkbox" name="info" class="form-check-input">
                                <label for="info" class="form-check-label">Запомнить меня</label>
                            </div>
                        <? } ?>

                        <p class="error___p"></p>

                        <button type="button" class="btn btn-primary authButton to___process">Войти</button>

                    </form>

                </div>

                <div class="col-lg-6">

                    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/includes/personalContacts.php'; ?>

                </div>

            </div>
        </div>
    </div>
</section>
