<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $USER->IsAuthorized() ){

    $arResult['AUTH'] = 'Y';
    $arResult['USER'] = tools\user::info( $USER->GetID() );
    $arResult['PERSONAL_PAGES'] = project\personal::pages();







}



$this->IncludeComponentTemplate();



