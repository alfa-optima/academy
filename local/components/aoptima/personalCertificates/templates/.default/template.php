<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arResult['AUTH'] == 'Y' ){ ?>


    <section class="section">
        <div class="container">

            <h2 class="section__title text-uppercase">Личный кабинет студента</h2>

            <div class="btn-group">
                <? foreach( $arResult['PERSONAL_PAGES'] as $page ){ ?>
                    <a <? if( $page['active'] != 'Y' ){ ?>href="<?=$page['url']?>"<? } ?> class="btn <? if( $page['active'] == 'Y' ){
                        echo 'btn-secondary';
                    } else {
                        echo 'btn-primary';
                    } ?>"><?=$page['name']?></a>
                <? } ?>
            </div>

            <div class="bg-brown-soft">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th colspan="6">
                                <h4 class="mb-2">Пройденные курсы</h4>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form action="/">
                                            <div class="mb-3">
                                                <label for="number" class="form-label">Введите номер</label>
                                                <div class="input-group">
                                                    <input id="number" type="text" name="number" class="form-control">
                                                    <button class="btn btn-primary" type="submit">Найти</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </th>
                        </tr>
                        <tr class="text-nowrap">
                            <th>№</th>
                            <th>Дата выдачи</th>
                            <th>Программа</th>
                            <th>Дата обучения</th>
                            <th>Кол-во балов</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">12345</a></td>
                            <td>02.02.2021</td>
                            <td><a href="#">Название программы</a></td>
                            <td>02.02.2021</td>
                            <td>10</td>
                            <td width=1%>
                                <a href="#" class="btn btn-primary btn-sm">
                                    <svg class="icon">
                                        <use xlink:href="#print"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="text-center text-center pt-2 pb-4">
                    <a href="#" class="btn btn-primary px-4">Показать еще (10)</a>
                </div>
            </div>
        </div>
    </section>



<? } else {


    // auth
    $APPLICATION->IncludeComponent(
        "aoptima:auth", "", array()
    );


} ?>