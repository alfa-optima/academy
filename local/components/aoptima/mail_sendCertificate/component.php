<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['REGISTER_ID'] = $arParams['register_id'];

$arResult['REGISTER'] = tools\el::info( $arResult['REGISTER_ID'] );

if( intval( $arResult['REGISTER']['ID'] ) > 0 ){

    $arResult['COURSE'] = tools\el::info( $arResult['REGISTER']['PROPERTY_COURSE_VALUE'] );
    if( intval($arResult['COURSE']['ID']) > 0 ){


        $this->IncludeComponentTemplate();
    }
}




