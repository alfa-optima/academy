<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if( $USER->IsAuthorized() ){

    $arResult['AUTH'] = 'Y';
    $arResult['USER'] = tools\user::info( $USER->GetID() );
    $arResult['PERSONAL_PAGES'] = project\personal::pages();

    if( isset( $_POST['stop_ids'] ) ){

        $arResult['USER_REG_COURSES'] = $_SESSION['USER_REG_COURSES'];

        foreach ( $arResult['USER_REG_COURSES'] as $k1 => $id_1 ){
            foreach ( $_POST['stop_ids'] as $k2 => $id_2 ){
                if( $id_1 == $id_2 ){
                    unset( $arResult['USER_REG_COURSES'][ $k1 ] );
                }
            }
        }

    } else {

        // Отклонённый Enum
        $moderRejectedEnum = \AOptima\Tools\prop_enum::getByXmlID('rejected');

        // Соберём ID курсов (все), к которым привязаны НЕотклоненные регистрации пользователя
        $arResult['USER_REG_COURSES'] = [];

        // Получим НЕотклонённые регистрации пользователя
        $filter = [
            "IBLOCK_ID" => project\learning_course::REG_IBLOCK_ID,
            "PROPERTY_USER" => $USER->GetID(),
            '!PROPERTY_MODERATION' => $moderRejectedEnum['ID']
        ];
        $fields = [ "ID", "PROPERTY_USER", "PROPERTY_MODERATION", "PROPERTY_COURSE" ];
        $sort = [ "SORT" => "ASC" ];
        $registers = \CIBlockElement::GetList(
            $sort, $filter, false, false, $fields
        );
        while ( $register = $registers->GetNext() ){
            $arResult['USER_REG_COURSES'][] = $register["PROPERTY_COURSE_VALUE"];
        }
        $arResult['USER_REG_COURSES'] = array_unique( $arResult['USER_REG_COURSES'] );
        sort( $arResult['USER_REG_COURSES'] );

        $_SESSION['USER_REG_COURSES'] = $arResult['USER_REG_COURSES'];
    }

    if( count( $arResult['USER_REG_COURSES'] ) == 0 ){
        $arResult['USER_REG_COURSES'] = [ 0 ];
    }
}





$this->IncludeComponentTemplate();



