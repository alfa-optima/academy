<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['IS_ROOT'] = false;
$arResult['IS_LIST'] = false;
$arResult['IS_DETAIL'] = false;
$arResult['IS_404'] = false;

$arURI = tools\funcs::arURI( tools\funcs::pureURL() );

$countryCode = trim(strip_tags($arURI[2]));

if( strlen( $arURI[3] ) > 0 && !( strlen($arURI[4]) > 0 ) ){

    $arResult['IS_DETAIL'] = true;

    $arResult['COUNTRY'] = tools\el::info_by_code( $countryCode, project\country::IBLOCK_ID );

    $arResult['CENTER'] = tools\el::info_by_code( $arURI[3], project\center::IBLOCK_ID );

    if(
        strlen($countryCode) > 0
        &&
        intval( $arResult['COUNTRY']['ID'] ) > 0
        &&
        intval( $arResult['CENTER']['ID'] ) > 0
    ){

        $arResult['CENTERS'] = project\center::all_items( $arResult['COUNTRY']['ID'] );
        $center_ids = [];
        foreach ( $arResult['CENTERS'] as $key => $center ){
            $center_ids[] = $center['ID'];
        }
        
        if( !in_array( $arResult['CENTER']['ID'], $center_ids ) ){
            $arResult['IS_404'] = true;
        }

    } else {

        $arResult['IS_404'] = true;
    }

} else if( strlen( $arURI[2] ) > 0 && !( strlen($arURI[3]) > 0 ) ){

    $arResult['IS_LIST'] = true;

    $arResult['COUNTRY'] = tools\el::info_by_code( $countryCode, project\country::IBLOCK_ID );

    if(
        strlen($countryCode) > 0
        &&
        intval( $arResult['COUNTRY']['ID'] ) > 0
    ){

        $arResult['CENTERS'] = project\center::all_items( $arResult['COUNTRY']['ID'] );
        foreach ( $arResult['CENTERS'] as $key => $center ){
            $center['COUNTRY'] = tools\el::info( $center['PROPERTY_COUNTRY_VALUE'] );
            $arResult['CENTERS'][ $key ] = $center;
        }

    } else {

        $arResult['IS_404'] = true;
    }

} else if( strlen( $arURI[1] ) > 0 && !( strlen($arURI[2]) > 0 ) ){

    $arResult['IS_ROOT'] = true;

    $arResult['COUNTRIES'] = project\country::all_items();

}

if( $arResult['IS_ROOT'] || $arResult['IS_LIST'] || $arResult['IS_DETAIL'] ){

    $this->IncludeComponentTemplate();

}



