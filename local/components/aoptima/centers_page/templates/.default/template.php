<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arResult['IS_404'] ){

    include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/includes/404.php';

} else {


    if( $arResult['IS_ROOT'] ){ ?>


        <section class="intro">
            <picture class="intro-bg">
                <source srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/center/center-banner.webp" type="image/webp">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/center/center-banner.jpg">
            </picture>
            <div class="intro-caption">
                <h1 class="intro-title text-uppercase mb-0">Центры академии кнауф</h1>
            </div>
            <div class="intro-btn">
                <a href="#about" class="arrow scrollTo">
                    <svg class="icon">
                        <use xlink:href="#arrow-down"></use>
                    </svg>
                </a>
            </div>
        </section>


        <section id="about" class="section">
            <div class="container">

                <h2 class="section__title text-uppercase">Выбор страны</h2>

                <div class="lessons">
                    <div class="row g-4">

                        <? foreach( $arResult['COUNTRIES'] as $key => $country ){ ?>

                            <div class="col-md-6 col-lg-3">
                                <div class="lessons__item">
                                    <h5 class="mb-3"><?=$country['NAME']?></h5>
                                    <a href="/centers/<?=$country['CODE']?>/">
                                        <img class="img-fluid mb-3" src="<?=\CFile::GetPath($country['PROPERTY_FLAG_VALUE'])?>">
                                    </a>
                                    <a class="lessons__link" href="/centers/<?=$country['CODE']?>/">Подробнее</a>
                                </div>
                            </div>

                        <? } ?>

                    </div>
                </div>

            </div>
        </section>



    <? } else if( $arResult['IS_LIST'] ){ ?>



        <section class="section">
            <div class="container">

                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d17939.271622558812!2d37.348176965413465!3d55.80350038224347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5466f27b62c47%3A0xff21eeef674937fa!2z0YPQuy4g0KbQtdC90YLRgNCw0LvRjNC90LDRjywgMTM5LCDQmtGA0LDRgdC90L7Qs9C-0YDRgdC6LCDQnNC-0YHQutC-0LLRgdC60LDRjyDQvtCx0LsuLCAxNDM0MDA!5e0!3m2!1sru!2sru!4v1623785731514!5m2!1sru!2sru" allowfullscreen="" loading="lazy"></iframe>
                </div>

                <div class="table-responsive">
                    <table class="table table-numbered">

                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Контактная информация</th>
                            </tr>
                        </thead>

                        <tbody>

                            <? $cnt = 0;
                            foreach( $arResult['CENTERS'] as $center ){ $cnt++; ?>

                                <tr>
                                    <td class="align-top">
                                        <div class="blob">
                                            <div class="blob__inner"><?=$cnt?></div>
                                        </div>
                                    </td>
                                    <td>
                                        <h4>
                                            <a href="/centers/<?=$center['COUNTRY']['CODE']?>/<?=$center['CODE']?>/" style="color:#484846;"><?=$center['NAME']?></a>
                                        </h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><?=$center['COUNTRY']['NAME']?>, <?=$center['PROPERTY_CITY_VALUE']?></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><?=$center['PROPERTY_ADDRESS_VALUE']?></p>
                                            </div>
                                            <div class="col-md-4">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a><?=$center['PROPERTY_PHONES_VALUE']?></a>
                                                    </li>
                                                    <li>
                                                        <a href="mailto:<?=$center['PROPERTY_EMAIL_VALUE']?>"><?=$center['PROPERTY_EMAIL_VALUE']?></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            <? } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>



    <? } else if( $arResult['IS_DETAIL'] ){


        if( intval($arResult['CENTER']['ID']) > 0 ){ ?>


            <section class="section">
                <div class="container">

                    <h2 class="section__title text-uppercase"><?=$arResult['CENTER']['NAME']?></h2>

                    <div class="row g-4">

                        <? if(
                            is_array( $arResult['CENTER']['PROPERTY_PHOTOS_VALUE'] )
                            &&
                            count($arResult['CENTER']['PROPERTY_PHOTOS_VALUE']) > 0
                        ){ ?>

                            <div class="col-lg-6">
                                <div class="product">

                                    <div class="product__preview mb-4">
                                        <a href="<?=tools\funcs::rIMGG( $arResult['CENTER']['PROPERTY_PHOTOS_VALUE'][0], 4, 800, 600 )?>" data-fancybox="gallery">
                                            <img class="img-fluid" src="<?=tools\funcs::rIMGG( $arResult['CENTER']['PROPERTY_PHOTOS_VALUE'][0], 5, 433, 324 )?>">
                                        </a>
                                    </div>

                                    <? if( count($arResult['CENTER']['PROPERTY_PHOTOS_VALUE']) > 1 ){
                                        unset( $arResult['CENTER']['PROPERTY_PHOTOS_VALUE'][0] ); ?>

                                        <div class="swiper images">
                                            <div class="swiper-wrapper align-center mb-5">

                                                <? foreach(
                                                    $arResult['CENTER']['PROPERTY_PHOTOS_VALUE'] as $photo_id
                                                ){ ?>

                                                    <div class="swiper-slide">
                                                        <div class="images__item">
                                                            <div class="product__preview">
                                                                <a href="<?=tools\funcs::rIMGG( $photo_id, 4, 800, 600 )?>" data-fancybox="gallery">
                                                                    <img class="img-fluid" src="<?=tools\funcs::rIMGG( $photo_id, 5, 89, 67 )?>">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <? } ?>

                                            </div>
                                            <div class="swiper-pagination"></div>
                                        </div>

                                    <? } ?>

                                </div>
                            </div>

                        <? } ?>

                        <div class="col-lg-6">
                            <div class="section__text">

                                <? if( strlen( $arResult['CENTER']['PROPERTY_GOD_OSN_VALUE'] ) > 0 ){ ?>
                                    <p>Год основания: <?=$arResult['CENTER']['PROPERTY_GOD_OSN_VALUE']?></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_RUKOVODITEL_VALUE'] ) > 0 ){ ?>
                                    <p>Руководитель: <?=$arResult['CENTER']['PROPERTY_RUKOVODITEL_VALUE']?></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_KOLICH_SOTRUD_VALUE'] ) > 0 ){ ?>
                                    <p>Количество сотрудников: <?=$arResult['CENTER']['PROPERTY_KOLICH_SOTRUD_VALUE']?></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_MAX_CNT_VALUE'] ) > 0 ){ ?>
                                    <p>Максимальное количество курсантов в группе: <?=$arResult['CENTER']['PROPERTY_MAX_CNT_VALUE']?></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_ADDRESS_VALUE'] ) > 0 ){ ?>
                                    <p><?=$arResult['CENTER']['PROPERTY_ADDRESS_VALUE']?></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_PHONES_VALUE'] ) > 0 ){ ?>
                                    <p>Телефоны: <?=$arResult['CENTER']['PROPERTY_PHONES_VALUE']?></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_EMAIL_VALUE'] ) > 0 ){ ?>
                                    <p>Телефоны: <a href="mailto:<?=$arResult['CENTER']['PROPERTY_EMAIL_VALUE']?>" class="link"><?=$arResult['CENTER']['PROPERTY_EMAIL_VALUE']?></a></p>
                                <? } ?>

                                <? if( strlen( $arResult['CENTER']['PROPERTY_SITE_LINK_VALUE'] ) > 0 ){ ?>
                                    <p><a href="<?=$arResult['CENTER']['PROPERTY_SITE_LINK_VALUE']?>" class="link" target="_blank">Ссылка на сайт</a></p>
                                <? } ?>

                            </div>
                        </div>

                    </div>
                </div>
            </section>


        <? }

    }

} ?>
