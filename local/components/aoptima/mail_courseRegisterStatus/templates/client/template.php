<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<p>Здравствуйте!</p>


<? if( $arResult['REG_ITEM']['MODER_ENUM']['XML_ID'] == 'accepted' ){ ?>


    <p>Ваша заявка на курс "<?=html_entity_decode($arResult['COURSE']['NAME'])?>" <u>одобрена!</u></p>


<? } else if( $arResult['REG_ITEM']['MODER_ENUM']['XML_ID'] == 'rejected' ){ ?>


    <p>Ваша заявка на курс "<?=html_entity_decode($arResult['COURSE']['NAME'])?>" <u>отклонена</u>!</p>

    <? if( strlen( $arResult['REG_ITEM']['PROPERTY_REJECT_REASON_VALUE']['TEXT'] ) > 0 ){
        if( $arResult['REG_ITEM']['PROPERTY_REJECT_REASON_VALUE']['TYPE'] == 'TEXT' ){ ?>
            <p><?=str_replace("\n", "<br>", $arResult['REG_ITEM']['PROPERTY_REJECT_REASON_VALUE']['TEXT'])?></p>
        <? } else if( $arResult['REG_ITEM']['PROPERTY_REJECT_REASON_VALUE']['TYPE'] == 'HTML' ){ ?>
            <?=html_entity_decode($arResult['REG_ITEM']['PROPERTY_REJECT_REASON_VALUE']['TEXT'])?>
        <? }
    } ?>


<? } else { ?>


    <p>Вы успешно зарегистрировались на курс "<?=html_entity_decode($arResult['COURSE']['NAME'])?>" в Академии Кнауф</p>

    <p>В данный момент Ваша заявка находится <u>на рассмотрении</u></p>
    <p>Мы сообщим вам, когда заявку будет одобрена</p>

    <br>

    <p>Email: <?=$arResult['USER']['EMAIL']?></p>
    <p>Телефон: <?=$arResult['REG_ITEM']['PROPERTY_PHONE_VALUE']?></p>

    <p>Фамилия: <?=$arResult['REG_ITEM']['PROPERTY_F_VALUE']?></p>
    <p>Имя: <?=$arResult['REG_ITEM']['PROPERTY_I_VALUE']?></p>
    <p>Отчество: <?=$arResult['REG_ITEM']['PROPERTY_O_VALUE']?></p>
    <p>Город: <?=$arResult['REG_ITEM']['PROPERTY_CITY_VALUE']?></p>


<? } ?>




