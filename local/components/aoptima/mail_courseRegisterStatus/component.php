<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['COURSE_ID'] = $arParams['course_id'];
$arResult['REG_ITEM_ID'] = $arParams['reg_item_id'];

BXClearCache(true, '/'.tools\el::EL_CACHE_NAME.'/'.$arResult['COURSE_ID'].'/');
BXClearCache(true, '/'.tools\el::EL_CACHE_NAME.'/'.$arResult['REG_ITEM_ID'].'/');

$arResult['COURSE'] = tools\el::info( $arResult['COURSE_ID'] );
$arResult['REG_ITEM'] = tools\el::info( $arResult['REG_ITEM_ID'] );

if(
    intval( $arResult['COURSE']['ID'] ) > 0
    &&
    intval( $arResult['REG_ITEM']['ID'] ) > 0
){

    $arResult['USER'] = tools\user::info( $arResult['REG_ITEM']['PROPERTY_USER_VALUE'] );

    if( intval( $arResult['REG_ITEM']['PROPERTY_MODERATION_ENUM_ID'] ) > 0 ){

        $arResult['REG_ITEM']['MODER_ENUM'] = tools\prop_enum::getByID( $arResult['REG_ITEM']['PROPERTY_MODERATION_ENUM_ID'] );
    }

    $arResult['EDIT_LINK'] = 'http://31.177.78.149/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.project\learning_course::REG_IBLOCK_ID.'&type=feedback&lang=ru&ID='.$arResult['REG_ITEM']['ID'].'&find_section_section=-1&WF=Y';



    $this->IncludeComponentTemplate();
}




