<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

global $APPLICATION;
$APPLICATION->RestartBuffer();

if( $USER->IsAuthorized() ){

    $reg_id = intval($_GET['reg_id']);

    $reg = tools\el::info( $reg_id );

    if(
        intval( $reg['ID'] ) > 0
        &&
        $reg['PROPERTY_USER_VALUE'] == $USER->GetID()
    ){
        
        // путь к файлу сертификата
        $cert_file_path = project\certificate::filePath( $reg['ID'] );
        
        if( file_exists( $cert_file_path ) ){

            $file = \CFile::MakeFileArray( $cert_file_path );

            if ( ob_get_level() ){    ob_end_clean();    }

            $fp = fopen( $cert_file_path, 'rb' );

            header('Content-Description: File Transfer');
            header('Content-Disposition: filename='.$file['name']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');

            header("Content-Type: ".$file['type'] );
            header("Content-Length: " . $file['size']);

            fpassthru($fp);

            fclose($fp);

            exit;

        }
    }
}





