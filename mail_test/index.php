<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if( $USER->IsAdmin() ){

    $APPLICATION->IncludeComponent(
        "aoptima:mail_courseRegisterStatus", strip_tags($_GET['template']),
        [
            "course_id" => intval($_GET['course_id']),
            "reg_item_id" => intval($_GET['reg_id']),
        ]
    );

}

